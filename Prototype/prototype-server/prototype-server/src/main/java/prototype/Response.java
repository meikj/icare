package prototype;

public class Response {
	
	private final long id;
	private final String name;
	
	public Response(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

}
