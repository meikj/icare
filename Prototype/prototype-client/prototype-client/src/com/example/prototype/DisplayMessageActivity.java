package com.example.prototype;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class DisplayMessageActivity extends ActionBarActivity {
	
	public static final String URL = "http://meikj.com:8080/hello?name={query}";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_message);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		Intent intent = getIntent();
		String name = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		new HttpResponseTask().execute(name);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_message, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class HttpResponseTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			
			return restTemplate.getForObject(URL, String.class, params[0]);
		}
		
		@Override
		protected void onPostExecute(String response) {
			TextView textView = (TextView) findViewById(R.id.text_response);
			textView.setText(response);
		}
		
	}
	
}
