\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objectives}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Brief Overview of Outcome}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Report Structure}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Related Work}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Jointly}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Evaluation}{5}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}App4Care}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Evaluation}{5}{subsection.2.2.1}
\contentsline {chapter}{\numberline {3}Problem Description and Specification}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Description}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Specification}{7}{section.3.2}
\contentsline {chapter}{\numberline {4}System Design}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}High-level Overview}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Data Management}{11}{section.4.2}
\contentsline {section}{\numberline {4.3}User Interface}{13}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Web Service}{13}{subsection.4.3.1}
\contentsline {subsubsection}{Login}{13}{section*.6}
\contentsline {subsubsection}{Home}{14}{section*.8}
\contentsline {subsubsection}{Users}{14}{section*.11}
\contentsline {subsubsection}{Clients}{16}{section*.16}
\contentsline {subsubsection}{Tasks}{18}{section*.22}
\contentsline {subsubsection}{Reminders}{20}{section*.28}
\contentsline {subsubsection}{Tracking}{22}{section*.34}
\contentsline {subsubsection}{Contacts}{23}{section*.36}
\contentsline {subsubsection}{Alerts}{24}{section*.41}
\contentsline {subsubsection}{Client Selection}{25}{section*.43}
\contentsline {subsubsection}{Visual Feedback}{26}{section*.46}
\contentsline {subsection}{\numberline {4.3.2}Mobile Application}{26}{subsection.4.3.2}
\contentsline {subsubsection}{Authenticating}{26}{section*.49}
\contentsline {subsubsection}{Reminders}{27}{section*.51}
\contentsline {subsubsection}{Tasks}{28}{section*.54}
\contentsline {subsubsection}{Contacts}{29}{section*.57}
\contentsline {subsubsection}{Tracks}{30}{section*.60}
\contentsline {subsubsection}{Settings}{30}{section*.61}
\contentsline {subsubsection}{Visual Feedback}{31}{section*.63}
\contentsline {chapter}{\numberline {5}Detailed Design and Implementation}{32}{chapter.5}
\contentsline {section}{\numberline {5.1}Web Service}{34}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Security and Authentication}{34}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Controllers}{35}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Template Engine and Web Layout Framework}{36}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}QR Code Image Service}{37}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}REST Service}{38}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Security and Authentication}{38}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Controllers}{40}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Shared Data Library}{41}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Data Source Configuration}{41}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Resource Models}{42}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Resource Repositories}{43}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Authority Filtering}{44}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Query Constructor and Filtering}{44}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}Authentication Token Provider}{45}{subsection.5.3.6}
\contentsline {subsection}{\numberline {5.3.7}Exceptions}{45}{subsection.5.3.7}
\contentsline {section}{\numberline {5.4}Mobile Application}{45}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Configuration}{46}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}REST API}{47}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Tracking}{47}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Tasks}{48}{subsection.5.4.4}
\contentsline {subsection}{\numberline {5.4.5}Resource Loading}{49}{subsection.5.4.5}
\contentsline {subsection}{\numberline {5.4.6}Generalising Resources}{49}{subsection.5.4.6}
\contentsline {subsection}{\numberline {5.4.7}Location Aware Activity}{51}{subsection.5.4.7}
\contentsline {chapter}{\numberline {6}Verification and Validation}{52}{chapter.6}
\contentsline {section}{\numberline {6.1}Similar Software Systems}{52}{section.6.1}
\contentsline {section}{\numberline {6.2}Logging}{52}{section.6.2}
\contentsline {section}{\numberline {6.3}JUnit Test Cases}{53}{section.6.3}
\contentsline {chapter}{\numberline {7}Results and Evaluation}{54}{chapter.7}
\contentsline {section}{\numberline {7.1}Existing Software}{54}{section.7.1}
\contentsline {section}{\numberline {7.2}Test Results}{55}{section.7.2}
\contentsline {chapter}{\numberline {8}Summary and Conclusions}{56}{chapter.8}
\contentsline {section}{\numberline {8.1}Future Work}{56}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Notifications}{56}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}AJAX}{57}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Alternative Client Frontends}{57}{subsection.8.1.3}
\contentsline {section}{\numberline {8.2}Conclusion}{58}{section.8.2}
\contentsline {chapter}{A\hspace {6pt}Bibliography}{59}{section.8.2}
\contentsline {chapter}{B\hspace {6pt}Detailed Specification and Design}{61}{appendix*.70}
\contentsline {section}{\numberline {B.1}Database Schema}{61}{section.B.1}
\contentsline {subsection}{\numberline {B.1.1}users}{61}{subsection.B.1.1}
\contentsline {subsection}{\numberline {B.1.2}clients}{61}{subsection.B.1.2}
\contentsline {subsection}{\numberline {B.1.3}reminders}{62}{subsection.B.1.3}
\contentsline {subsection}{\numberline {B.1.4}tasks}{62}{subsection.B.1.4}
\contentsline {subsection}{\numberline {B.1.5}tracking}{63}{subsection.B.1.5}
\contentsline {subsection}{\numberline {B.1.6}contacts}{63}{subsection.B.1.6}
\contentsline {subsection}{\numberline {B.1.7}alerts}{63}{subsection.B.1.7}
\contentsline {section}{\numberline {B.2}REST API}{64}{section.B.2}
\contentsline {subsection}{\numberline {B.2.1}Authentication}{64}{subsection.B.2.1}
\contentsline {subsection}{\numberline {B.2.2}Output}{64}{subsection.B.2.2}
\contentsline {subsection}{\numberline {B.2.3}Client}{64}{subsection.B.2.3}
\contentsline {subsubsection}{The client object}{64}{section*.71}
\contentsline {subsubsection}{Update client information}{65}{section*.72}
\contentsline {subsubsection}{Get client information}{65}{section*.73}
\contentsline {subsection}{\numberline {B.2.4}Reminders}{65}{subsection.B.2.4}
\contentsline {subsubsection}{The reminder object}{66}{section*.74}
\contentsline {subsubsection}{Create a new reminder}{66}{section*.75}
\contentsline {subsubsection}{Update a reminder}{67}{section*.76}
\contentsline {subsubsection}{Delete a reminder}{67}{section*.77}
\contentsline {subsubsection}{Get a reminder}{67}{section*.78}
\contentsline {subsubsection}{Get all reminders}{68}{section*.79}
\contentsline {subsection}{\numberline {B.2.5}Tasks}{68}{subsection.B.2.5}
\contentsline {subsubsection}{The task object}{68}{section*.80}
\contentsline {subsubsection}{Create a new task}{68}{section*.81}
\contentsline {subsubsection}{Update a task}{69}{section*.82}
\contentsline {subsubsection}{Delete a task}{69}{section*.83}
\contentsline {subsubsection}{Get a task}{70}{section*.84}
\contentsline {subsubsection}{Get all tasks}{70}{section*.85}
\contentsline {subsection}{\numberline {B.2.6}Tracking}{70}{subsection.B.2.6}
\contentsline {subsubsection}{The track object}{70}{section*.86}
\contentsline {subsubsection}{Create new track}{70}{section*.87}
\contentsline {subsubsection}{Get a track}{71}{section*.88}
\contentsline {subsubsection}{Get all tracks}{71}{section*.89}
\contentsline {subsection}{\numberline {B.2.7}Contacts}{71}{subsection.B.2.7}
\contentsline {subsubsection}{The contact object}{71}{section*.90}
\contentsline {subsubsection}{Create a new contact}{72}{section*.91}
\contentsline {subsubsection}{Update a contact}{72}{section*.92}
\contentsline {subsubsection}{Delete a contact}{73}{section*.93}
\contentsline {subsubsection}{Get a contact}{73}{section*.94}
\contentsline {subsubsection}{Get all contacts}{73}{section*.95}
\contentsline {subsection}{\numberline {B.2.8}Alerts}{74}{subsection.B.2.8}
\contentsline {subsubsection}{The alert object}{74}{section*.96}
\contentsline {subsubsection}{Create a new alert}{74}{section*.97}
\contentsline {subsubsection}{Get an alert}{74}{section*.98}
\contentsline {subsubsection}{Get all alerts}{75}{section*.99}
\contentsline {section}{\numberline {B.3}Original Project Plan}{75}{section.B.3}
\contentsline {chapter}{C\hspace {6pt}Detailed Test Strategy}{77}{appendix*.100}
\contentsline {section}{\numberline {C.1}Use Cases}{77}{section.C.1}
\contentsline {subsection}{\numberline {C.1.1}Web Service}{77}{subsection.C.1.1}
\contentsline {subsubsection}{Create a new resource}{77}{section*.101}
\contentsline {subsubsection}{Delete an existing resource}{78}{section*.102}
\contentsline {subsubsection}{Edit an existing resource}{79}{section*.103}
\contentsline {subsubsection}{Selecting a client}{80}{section*.104}
\contentsline {subsubsection}{Login}{80}{section*.105}
\contentsline {subsubsection}{Logout}{81}{section*.106}
\contentsline {subsubsection}{Acknowledging/unacknowledging an alert}{81}{section*.107}
\contentsline {subsubsection}{Marking a task as complete/incomplete}{82}{section*.108}
\contentsline {subsubsection}{Looking up a coordinate}{82}{section*.109}
\contentsline {subsubsection}{Searching an address}{83}{section*.110}
\contentsline {subsubsection}{Regenerating a client authentication token}{83}{section*.111}
\contentsline {subsection}{\numberline {C.1.2}Mobile Application}{84}{subsection.C.1.2}
\contentsline {subsubsection}{Create a new resource}{84}{section*.112}
\contentsline {subsubsection}{Delete an existing resource}{85}{section*.113}
\contentsline {subsubsection}{Edit an existing resource}{85}{section*.114}
\contentsline {subsubsection}{View tracks}{86}{section*.115}
\contentsline {subsubsection}{Toggle tracking service}{86}{section*.116}
\contentsline {subsubsection}{Send an alert}{87}{section*.117}
\contentsline {subsubsection}{Scan an authentication token}{87}{section*.118}
\contentsline {subsubsection}{Clear or regenerate authentication token}{88}{section*.119}
\contentsline {section}{\numberline {C.2}Test Cases}{88}{section.C.2}
\contentsline {subsection}{\numberline {C.2.1}QueryConstructorTest}{88}{subsection.C.2.1}
\contentsline {subsection}{\numberline {C.2.2}JdbcAlertsRepositoryTest}{89}{subsection.C.2.2}
\contentsline {subsection}{\numberline {C.2.3}JdbcClientRepositoryTest}{89}{subsection.C.2.3}
\contentsline {subsection}{\numberline {C.2.4}JdbcContactsRepositoryTest}{89}{subsection.C.2.4}
\contentsline {subsection}{\numberline {C.2.5}JdbcRemindersRepositoryTest}{90}{subsection.C.2.5}
\contentsline {subsection}{\numberline {C.2.6}JdbcTasksRepositoryTest}{90}{subsection.C.2.6}
\contentsline {subsection}{\numberline {C.2.7}JdbcTrackingRepositoryTest}{90}{subsection.C.2.7}
\contentsline {subsection}{\numberline {C.2.8}JdbcUserRepositoryTest}{90}{subsection.C.2.8}
\contentsline {chapter}{D\hspace {6pt}User Guide}{92}{appendix*.120}
\contentsline {section}{\numberline {D.1}Backend Setup}{92}{section.D.1}
\contentsline {section}{\numberline {D.2}Web Service}{94}{section.D.2}
\contentsline {subsection}{\numberline {D.2.1}Selecting a Client}{96}{subsection.D.2.1}
\contentsline {subsection}{\numberline {D.2.2}Client Management}{96}{subsection.D.2.2}
\contentsline {subsection}{\numberline {D.2.3}User Management}{98}{subsection.D.2.3}
\contentsline {subsection}{\numberline {D.2.4}User Page}{99}{subsection.D.2.4}
\contentsline {subsection}{\numberline {D.2.5}Resource Management}{99}{subsection.D.2.5}
\contentsline {section}{\numberline {D.3}Mobile Application}{100}{section.D.3}
\contentsline {subsection}{\numberline {D.3.1}Sending an Alert}{102}{subsection.D.3.1}
\contentsline {subsection}{\numberline {D.3.2}Viewing Your Tracks}{102}{subsection.D.3.2}
\contentsline {subsection}{\numberline {D.3.3}Tasks, Reminders and Contacts}{103}{subsection.D.3.3}
\contentsline {subsection}{\numberline {D.3.4}Settings}{104}{subsection.D.3.4}
\contentsline {chapter}{E\hspace {6pt}Program Listing}{105}{appendix*.139}
