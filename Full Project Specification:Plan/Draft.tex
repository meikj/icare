\documentclass[11pt, oneside]{article}  % use "amsart" instead of "article" for AMSLaTeX format
\usepackage[margin=0.7in]{geometry}       % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   	% ... or a4paper or a5paper or ... 
\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}					% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
										% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{framed}

\title{
	\includegraphics[width=6cm]{strath}\\
	\vspace{5cm}
	\textbf{CS408 \\ Project Specification \& Plan \\ \ \\ iCare}
}
\author{
	\href{mailto:john.meikle.2013@uni.strath.ac.uk}{John James Meikle} \\
	Supervised by \href{mailto:clemens.kupke@strath.ac.uk}{Dr Clemens Kupke} \\
	University of Strathclyde
}
\date{Wednesday, 10th of December 2014}

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage
%\begin{center}
%\line(1,0){500}
%\end{center}

\section{Overview of Project}
The project \emph{iCare} is a mobile and tablet application designed to support and improve the lives of people with potentially special needs (e.g. elderly, mentally ill or impaired, sick) who require contact with a care provider. This project focuses on implementing a client-server architecture providing a platform which enables care providers a way for managing and connecting with their clients, as well as a way for clients to connect with their carers. This brings carers and their clients closer together.

In terms of functionality, the application provides a way for clients to seek help and assistance from their carers in times of need. There is further functionality for reminders, tasks, tracking, statistics, notifications, and contact -- all of which can be handled remotely by the carer through an online web-based interface.

More specifically, both the care worker and client can issue reminders for medication, food and water intake, or anything else deemed appropriate, as well as tasks. The carer can track the location of the client, so whereabouts are known at all times. The carer can request that the client fill out wellbeing figures or other specifiable data, which can provide both the carer and the client useful statistics. Not only are appropriate and relevant notifications produced by reminders and statistical data requests, the carer can issue custom notifications to the client, such as questions or messages. The client can themselves issue messages and emergency alerts to their carer through the application. The client can also be given access to contact details such as phone numbers, as well as addresses, which can range from just their doctor, all the way to commonly visited or important locations. There will also be experimental functionality for detecting falls or trips, which will issue an emergency alert to the carer, who can then act on it.

Architecturally, the mobile application will connect to a remote server hosted and managed by the care provider organisation. The server will interface with a database storing all user information. There will be an authentication procedure when connecting to the server, designed to make it easy and secure for the client mobile application to identify itself. This authentication procedure will entail a unique token, rather than conventional (often forgettable) username and password credentials. As far as management goes, a web-based administration interface will be provided, accessible through a web browser by an authorised care worker. This interface will provide a way for registering new clients and managing existing ones.

\section{List of Objectives}
\begin{enumerate}
\item Continue to investigate, explore and gain confidence with Android SDK in preparation for developing mobile and tablet application, including networking, notifications and reminders, user interface, device sensors, persistent storage, and accessibility.

\item Continue to investigate, explore and gain confidence with the Spring IO platform, including Spring IO Platform/Spring Framework, Spring Boot, Spring for Android, and Spring Data.

\item Develop a custom server backend comprised of a web service and REST service using Spring to support a client-server architecture.

\item Interface with a MySQL database server backend using Spring's wrapper of JDBC.

\item Appreciate the need for good user interface design, particularly if elderly and impaired individuals are concerned. This will entail careful attention to human-computer interaction and accessibility.

\item Demonstrate practical application and use of standard design patterns, such as MVC. Both the backend and frontend architecture must be (or at least somewhat) sanely designed.

\item Provide substantial documentation covering both the internal architecture of the system and the REST service API. The latter of which will particularly allow for other applications (e.g. alternative frontend) to be developed that can interact with the server backend.

\item Provide substantial automated test cases and units covering the entire backend. Sensibly employ iterative test-driven development.

\item Provide substantial manual use case tests covering the entire frontend.

\item Overcome challenges in security and authentication.
\end{enumerate}

\section{Survey of Related Work}
\subsection{Jointly}
\begin{framed}
``\textbf{Jointly} is an app that makes caring for someone a little easier, less stressful and a lot more organised by making communication and coordination between those who share the care as easy as a text message. [...] This app is brought to you by Carers UK." -- \href{https://www.jointlyapp.com/}{Jointly}
\end{framed}

This app appears to combine group messaging with other useful features including to-do and medication lists, calendar and more. More specific features include:

\begin{itemize}
\item \textbf{Home} -- where you can invite people, see who is in your circle, and view a log of recent activity. 

\item \textbf{Profile} -- where you can store useful information about the person you are looking after and access it any time at a click of a button.

\item \textbf{Messaging} -- where you can communicate with everyone in your circle at a touch of a button.

\item \textbf{Tasks} -- where you can keep organised and on top of things by using tasks and task lists.

\item \textbf{Calendar} -- where you can create date/time specific events and invite anyone in your circle.

\item \textbf{Medications} -- where you can keep track of current and past medication of the person you are caring for. You can also upload an image to quickly recognise a medicine.

\item \textbf{Contacts} -- where you can store useful contacts and access their details anytime, anywhere!
\end{itemize}

\subsubsection{Evaluation}
In contrast to iCare, this application seems to lean on the more social side, providing features such as profile management, circles, messaging, calendar and contacts. However, it does provide a feature for medication tracking and reminding, which iCare specifies.

\subsection{App4Care}
\begin{framed}
``\textbf{App4Care} is an app which provides a suite of seven [contact, location, info, panic alert, tasks, trip/fall, wellbeing] easy to use functions, which help carers at work, living away or on holiday, stay connected with those they care for. [...] connects across two apps -- the carer app downloads at store price and the cared-for app downloads free of charge." -- \href{http://www.app4care.com/}{App4Care}
\end{framed}

This app appears to make use of two mobile apps: one for the carer, and the other for the cared-for. There is support for multiple cared-for to connect with a single carer, allowing a single carer to care for multiple people.

There is functionality for scheduling important tasks and reminders, monitoring wellbeing, panic alarm, trip/fall alert, GPS tracker and zone alert, essential health and contact information, and contact.

\subsubsection{Evaluation}
This application seems to fit the idea of iCare more in terms of functionality, but is architecturally much different. Both the carer and client utilise dedicated mobile applications, rather than a backend web service supplying a web-based administration interface for carer. I think pursuing the web service route for carers is better because it allows them to manage and connect with their clients anywhere they have access to a web browser, which includes desktop and mobile, both optimised for. The limitations I see coming from this is lack of mobile notifications to carer (i.e. there's an emergency!). This can be solved by sending an email or text to the carers specified email address or mobile number. This will act as a notification, providing more information in the content body.

\section{Brief Overview of Methodology}
In terms of project specification, the original (KUPK01) specification was taken as inspiration, fleshed out and extended further with more useful features. This document acts as project specification.

In terms of project design, a client-server architecture will be pursued. The backend will comprise of a web service and REST service, both will interact with a MySQL database server. The web service will supply a web-based administration interface for use by carers. The frontend will comprise of a mobile Android application which connects to the server and consumes its REST service.

In terms of project implementation, the approach will mainly consist of Java. This is because the frontend mobile application will be programmed in Java using the Android SDK in conjunction with Spring for Android. And as far as the backend is concerned, Java will be used to develop the web service and REST service using the Spring IO framework.

In terms of project verification, the project could potentially be verified against real people within the actual relevant demographic (i.e. elderly, mentally ill or impaired, sick) through contacting a care provider organisation willing to help. Not only this, but the iteratively developed automated test cases and units, as well as use cases, will enable verification of the features, robustness and workability of the system.

\section{Brief Overview of End Project Evaluation}
The end project will be evaluated by ensuring it meets the project specification and requirements. I will do this by testing the project against predefined manual use cases and automated test cases. Since iterative test driven development will be used, this should be fairly straightforward.

It is important during the evaluation period we test the project in the context for which it is designed for (i.e. remote server maintained by care organisation of which client mobile applications connect to over the Internet) so we can be certain of its usability. This is why I have a DreamCloud VPS for hosting the backend, so as to replicate this context. In the case of the client, the feasibility of offline scenarios must be evaluated.

Not only this, but I will consider getting in touch with relevant care organisations and asking for their thoughts on the project and application, as well as possibly getting relevant people within the demographic to try out the application, thus gaining real world feedback on usability, features, viability, etc.

\newpage
\section{Final Project Plan}
\begin{center}
\begin{tabular}{|l||l|}
\hline
\textbf{Semester 1} & \textbf{Semester 2} \\
\hline

\begin{tabular}[t]{c|p{5cm}}
\textbf{Week} & \textbf{Milestone} \\
6 & Finalise Project Scope \& Outline Plan. \\
7 & Further investigate and explore Android SDK and Spring IO. \\
8 & Settle on backend database solution and ensure familiarity with integration and usage. \\
9 & Initial client-server prototype (basic mobile app connecting to basic backend server). \\
10 & Ensure preparation for Full Project Specification/Plan and Project Poster is fully under way. \\
11 & Finalise Full Project Specification/Plan and Project Poster. \\
12 & End of semester, reflect on previous weeks work. \\
\ \\
\end{tabular}

&

\begin{tabular}[t]{c|p{8cm}}
\textbf{Week} & \textbf{Milestone} \\
1 & Ensure database backend is up and running with an initial skeletal schema. \\
2 & Backend web service for web-based administration interface. \\
3 & Finalise Project Progress Report. \\
4 & Backend REST service, including design and finalisation of REST API. \\
5 & Consume REST service with mobile Android application. \\
6 & Finalise Project Report Outline and (some) Draft Chapters. \\
7 & Deal with potential authentication and security concerns. \\
8+9 & Finalise implementation, testing and documentation of backend. \\
8+9 & Finalise implementation, testing and documentation of frontend. \\
10 & Finalise Project Report for electronic submission. \\
11 & Submission of two bound copies of Project Report. Demo project. \\
\ \\
\end{tabular}\\

\hline
\end{tabular}
\end{center}

\section{Overview of Technologies and Frameworks Involved}
\subsection{Spring IO}
\begin{framed}
"Let's build a better Enterprise. Spring helps development teams everywhere build simple, portable, fast and flexible JVM-based systems and applications." -- \href{http://spring.io/}{Spring IO}
\end{framed}

Since this project relies heavily on a client-server architecture, whereby some frontend mobile application is consuming some backend service, the Spring IO framework is an excellent choice. Not only is this an enterprise-grade framework, it has libraries which support the development of backend REST and web services, simplified database access, and support for Android. The Spring IO framework is composed of several different core components, not all of which are required for this project. The major components identified for this project are the Spring IO Platform/Spring Framework, Spring Boot, Spring for Android, and Spring Data. Further components can be identified and used at a later stage if more advanced functionality is required that is not presently covered within the scope of the currently identified framework components.

In terms of the backend, this framework will be used to develop a REST and web service. Both services will communicate with a MySQL database server through JDBC. The Spring IO framework provides a wrapper for JDBC which simplifies database communication immensely. This is supplied through the aforementioned Spring Data framework component. Specifically, this handles: opening the connection; preparing and executing the SQL statement; setting up the loop to iterate through the results (if any); processing any exception; handling transactions; and closing the connection, statement, and result set. This means that we only have to define the connection parameters, specify the SQL statement, declare parameters and provide parameter values, and do the work for each iteration.

As far as the REST service is concerned, this allows us to provide an API for (authorised) consumption of data. REST stands for Representational State Transfer. This service relies on the HTTP protocol for transfer, which is the preferred method of communication from a frontend Android mobile application standpoint. The REST architectural style describes six constraints:

\begin{enumerate}
\item
\textbf{Client-server} -- The uniform interface separates clients from servers. This separation means that, for example, clients are not concerned with data storage, instead the server handles the retrieval and processing of data. This improves the portability of client code. Servers are not concerned with the user interface or user state, this makes servers more simpler and scalable. Both clients and servers can be developed independently, as long as the interface is not altered. This means that any client frontend can communicate with the backend server so long as it conforms to the interface (or API).
\item
\textbf{Stateless} -- As REST stands for Representational State Transfer, statelessness is key. This means that the necessary state to handle the request is contained within the request itself (e.g. unique client authentication token for authenticated requests), which can come in the form of part of URI, query-string parameters, request body or headers. The URI identifies the resource and the body contains the state of that resource. After the server completes its processing, the appropriate response (or state) is communicated back to the client via headers, status and a response body. The response body in our case consists of JSON (JavaScript Object Notation), which is a lightweight data-interchange format that uses human-readable text to transmit data objects consisting of key-value pairs. It is easy for computers to parse and generate (see Jackson).
\item
\textbf{Cacheable} -- Clients should be able to cache responses. Responses must therefore, implicitly or explicitly, define themselves as cacheable, or not, to prevent clients reusing stale or inappropriate data in response to further requests. This can improve scalability and performance if utilised correctly.
\item
\textbf{Layered system} -- A client cannot ordinarily tell whether it is connected directly to the end server, or to an intermediary along the way. Intermediary servers may improve system scalability by enabling load-balancing and providing shared caches. Layers may also enforce security policies.
\item
\textbf{Code on demand (optional)} -- Servers are able to temporarily extend or customise the functionality of a client by transferring logic to it that it can execute. Example of this may include compiled components such as Java applets and client-side scripts such as JavaScript. Complying with these constraints, and thus conforming to the REST architectural style, will enable any kind of distributed hypermedia system to have desirable emergent properties, such as performance, scalability, simplicity, modifiability, portability and reliability.
\item
\textbf{Uniform interface} -- The uniform interface constraint defines the interface between clients and servers (i.e. the API). It simplifies and decouples the architecture, which enables each part to evolve independently. The four guiding principles of the uniform interface are:
\begin{itemize}
\item
\textbf{Resource-based} -- Individual resources are identified in requests using URIs as resource identifiers. The resources themselves are conceptually separate from the representations that are returned to the client. For example, the server does not send its database, instead some HTML, XML, or JSON (in our case!) that represents some database records expressed.
\item
\textbf{Manipulation of resources through representations} -- When a client holds a representation of a resource, including any metadata attached, it has enough information to modify or delete the resource on the server, provided it has the permission to do so. The permission aspect is very important!
\item
\textbf{Self-descriptive messages} -- Each message includes enough information to describe how to process the message. For example, which parser to invoke may be specified by an Internet media type (or MIME type), for example \emph{text/json}. Responses also explicitly indicate their cache-ability (see Cacheable).
\item
\textbf{Hypermedia as the engine of application state} -- Client deliver state via body contents, query-string parameters, request headers, and the requested URI (i.e. the resource name). Services deliver state to clients via body content, response codes, and response headers. This is technically referred to as hypermedia.
\end{itemize}
\end{enumerate}

As far as the web service is concerned, this will be used for the web-based administration interface that is accessed by authorised care workers. This runs independently to the REST service. It works similar to the REST service in that it utilises HTTP, handles requests, and constructs responses. The difference is that we return views -- which are templates with appropriate response data -- comprised entirely of HTML. For the web-based user interface, I will utilise the Bootstrap framework as this will allow me to build an extremely rich and dynamic web-based user interface. To quote: "Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web". This means that I can easily ensure a workable interface for not only desktop web browsers, but mobile web browsers too, essentially allowing any authorised care worker to login on the go.

In both cases, we can use Spring Boot to generate executable JAR files. The reason why we would want to use this is because it makes it easy to create stand-alone, production-grade Spring based applications that you can "just run". It packages all dependencies and third-party libraries with minimal fuss; automatically configures Spring whenever possible; provide production-ready features such as metrics, health checks and externalised configuration; and absolutely no code generation and no requirement for XML configuration. Also, most importantly, for both services it embeds Tomcat or Jetty directly. The choice of servlet software is Apache Tomcat, purely because of maturity and stability. It is an open source software implementation of the Java Servlet and JavaServer Pages technologies. It powers numerous large-scale, mission-critical web applications across a diverse range of industries and organisations.

On a more personal note, this is the perfect project for getting to grips with such a framework. This is very valuable as Spring is enterprise-grade and heavily used in industry where Java is employed. This is why I did not decide to design and implement a proprietary custom-built backend. There is no sense reinventing the wheel.

\subsection{Android SDK}
\begin{framed}
"The Android SDK provides the API libraries and developer tools necessary to build, test, and debug apps for Android." -- \href{http://developer.android.com/sdk/index.html}{Android SDK}
\end{framed}

The Android SDK will be utilised for the frontend mobile/tablet application. In terms of the development environment, Eclipse ADT (Eclipse + ADT plugin) with the appropriate Android SDK and Platform tools will be used. There is the option of Android Studio, but is presently in beta testing stages, whereas Eclipse ADT is stable and still the de facto standard. I'm also familiar with the Eclipse environment, so that helps.

There are several important aspects to the Android SDK that must be explored. These are:

\begin{enumerate}
\item
\textbf{Notifications and reminders} -- Android provides functionality for displaying notifications, as well as setting reminders. This is important for medication, food and water intake reminders.
\item
\textbf{User interface} -- Android provides a very rich set of user interface components and layout options, as well as concrete design guidelines which this project will (mostly) adhere to.
\item
\textbf{Persistent storage} -- Android provides several options to save persistent application data. This ranges from shared preferences (store \emph{private} primitive data in key-value pairs), internal storage (store \emph{private} data on the device memory), external storage (store \emph{public} data on the shared external storage), SQLite databases (store structured data in a \emph{private} database), and network connection (store data on the web with your own network server). It is likely application settings (non-sensitive) will be stored using shared preferences, whereas sensitive information (unique authentication token, medication reminders, client information, etc.) will be stored privately in internal storage.
\item
\textbf{Permissions} -- Android ensures that any application using special operations must have the correct corresponding permissions set in the manifest file. There are many permissions that Android enforce, which can range from accessing the Internet, saving local data, requesting current location, and so on.
\item
\textbf{Sensors} -- Android provides ways of accessing mobile sensor information through an API. As we would like to experiment with detecting falls or accidents, exploring this area will be very useful. The Android platform supports three broad categories of sensors:
\begin{itemize}
\item
\textbf{Motion sensors} -- These sensors measure acceleration forces and rotational forces along three axes. This category includes accelerometers, gravity sensors, gyroscopes, and rotational vector sensors.
\item
\textbf{Environmental sensors} -- These sensors measure various environmental parameters, such as ambient air temperature and pressure, illumination, and humidity. This category includes barometers, photometers, and thermometers.
\item
\textbf{Position sensors} -- These sensors measure the physical position of a device. This category includes orientation sensors and magnetometers.
\end{itemize}
\item
\textbf{Messaging and calls} -- Android provides application developers ways of using messaging and calls (assuming appropriate permissions are requested). This may be useful when implementing emergency features so as to issue contact to a care worker in the event of a suspected fall or accident.
\item
\textbf{Accessibility} -- Android provides accessibility features and services for helping visually and physically impaired individuals navigate their devices more easily, including text-to-speech, haptic feedback, gesture navigation, trackball and directional-pad navigation. Further accessibility features can be built by developers if desired, which may be explored if more advanced accessibility features are required, or can be thought of. This should be important to explore so the application can be used easily by visually and physically impaired individuals, who are likely the main demographic to be using the application.
\end{enumerate}

The frontend application ties in with the Spring for Android component. This particular component allows us to easily consume the backend REST service, including parsing. As far as authentication and encryption is concerned, there is support for OAuth, which is an open standard to authorisation. It is designed specifically to work with HTTP, and essentially allows access tokens to be issued to clients. The client then uses their access token to access protected resources on the server. This relieves the need for a username and password. We can store this token locally (but privately) and use it in requests to the REST service. It is also very convenient because if a token is compromised, we can simply revoke it and issue the client a new one. We still maintain the concept of identity because we know who we issue tokens to. This will simplify the authorisation process, particularly as the demographic may not be the best in remembering credentials. All requests could theoretically be done over HTTPS, so long as the server is set up for SSL, thereby ensuring encrypted communication.

\subsection{Gradle}
\begin{framed}
"Gradle is build automation evolved. Gradle can automate the building, testing, publishing, deployment and more of software packages or other types of projects such as generated static websites, generated documentation or indeed anything else." -- \href{http://www.gradle.org/}{Gradle}
\end{framed}

As a result of exploring both Spring IO and Android SDK, it seems that Gradle is the preferred build system of choice. This is not necessarily a requirement, but helps a great deal when building Spring applications as it handles and fetches all of the dependencies for you (no manually importing JARs) and integrates well with Spring Boot. There are plugins for Eclipse too, which means that theoretically it should work fine with my current development environment. It is also an enterprise-grade build system, which would be very useful to learn as it is extremely relevant in industry.

Gradle was used to build the prototype backend REST service JAR using the Spring Boot and Eclipse plugins. This seemed to work well enough to strongly consider using it for at least the backend services.

\section{Prototype}
A simple (emphasis on simple) prototype was developed to demonstrate a basic client-server architecture. This prototype utilised the Spring IO framework and Android SDK. It did not demonstrate any particular feature of the iCare application, but more of a proof of concept or demonstration of the technologies involved.

The prototype entailed a backend REST service containing one resource mapping called \emph{hello}. This accepted an optional parameter called \emph{name}. When a GET request was made on this resource, we returned a customised response using the supplied \emph{name} parameter. If no parameter was supplied, we merely issued a canned response. The response was returned as a JSON string, ready for parsing. The Android application consisted of two views: one for inputting name and submitting; and the other for displaying the response string.

Overall, although this was an extremely basic prototype, it provided an opportunity to explore all technologies and frameworks involved, thus wetting my feet enough to get me started on real solid work come Semester 2. It is now a case of exploring further and utilising more advanced potential.

\subsection{Problems Overcome}
A number of problems were overcome, which made this prototype stage a worthwhile endeavour. I believe these are worth mentioning, so as to get an idea of how my approach to the prototype went. These were:

\begin{itemize}
\item
Initially placed REST service request code in \emph{onCreate()} method in \emph{DisplayMessageActivity} class. This meant that upon creation of the activity, there was code executing which took too long, causing Android to terminate the app on the ground it was "unresponsive". In hindsight, this made sense. The solution was to investigate the \emph{AsyncTask} abstract class, provided by the Android SDK. This abstract class enables proper and easy use of the UI thread, providing a way for us to perform a computational task in the background that publishes results on the UI thread. There are overridable methods for processing input and computing output, reporting progress (optional), and performing some operation on the output (optional).
\item
Android requires permissions to be set in the manifest file if we are performing controlled operations, such as accessing the Internet. The solution was to request use of the \emph{INTERNET} permission.
\item
When running the prototype REST service JAR on my DigitalOcean VPS it would stall (or block) before running main class, even though it worked perfectly fine on home computer. Upon further digging, this appears to be a problem with the generation of the \emph{SecureRandom} instance and is stuck trying to read from an entropy resource, namely \emph{/dev/random}. The solution was to use \emph{/dev/urandom} instead, which does not block. This was done by setting a command-line parameter.
\end{itemize}

\newpage
\section{Choice of Marking Scheme}
Taking into consideration the predominantly software-oriented project objectives and the substantial practical element involved (i.e. client-server architecture, mobile app, etc.), it is therefore agreed upon that this project is a \emph{Software Development-based Project}. The following marking scheme will therefore be used:

\begin{itemize}
\item Student Performance - 10\%
\item Project Product - 35\%
	\begin{itemize}
		\item Software - 25\%
		\item Documentation - 10\%
	\end{itemize}
\item Project Process - 30\%
	\begin{itemize}
		\item Background study, requirements analysis, and design - 20\%
		\item Testing - 10\%
	\end{itemize}
\item Project Results and Evaluation - 15\%
\item Report Presentation - 10\%
\end{itemize}

\end{document}  
