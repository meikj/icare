\contentsline {section}{\numberline {1}Overview of Project}{3}{section.1}
\contentsline {section}{\numberline {2}List of Objectives}{3}{section.2}
\contentsline {section}{\numberline {3}Survey of Related Work}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Jointly}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Evaluation}{4}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}App4Care}{4}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Evaluation}{5}{subsubsection.3.2.1}
\contentsline {section}{\numberline {4}Brief Overview of Methodology}{5}{section.4}
\contentsline {section}{\numberline {5}Brief Overview of End Project Evaluation}{5}{section.5}
\contentsline {section}{\numberline {6}Final Project Plan}{6}{section.6}
\contentsline {section}{\numberline {7}Overview of Technologies and Frameworks Involved}{6}{section.7}
\contentsline {subsection}{\numberline {7.1}Spring IO}{6}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Android SDK}{8}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Gradle}{9}{subsection.7.3}
\contentsline {section}{\numberline {8}Prototype}{10}{section.8}
\contentsline {subsection}{\numberline {8.1}Problems Overcome}{10}{subsection.8.1}
\contentsline {section}{\numberline {9}Choice of Marking Scheme}{11}{section.9}
