# iCare #
***
This repository is for my 4th year Honours project entitled iCare (KUPK01) and is being supervised by the project proposer, [Dr Clemens Kupke](https://personal.cis.strath.ac.uk/clemens.kupke/). For the project log book, please consult the wiki.

The project iCare is primarily a mobile and tablet application designed to support and improve the everyday life of elderly who require or benefit from a care provider. The system delivers an enterprise ready care platform for use within a care organisation. The project focuses on implementing a client-server architecture, with a backend consisting of a REST and web service, and a frontend consisting of a mobile Android application. This particular implementation uses MySQL as the backend data source (uses the MySQL JDBC connector), but any JDBC compatible data source can be used.

## System Features ##

* Backend REST service providing an API for consuming and manipulating system resources. The frontend mobile Android application will make use of this service. This will also provide the potential for further frontend developments, including by third parties. This is because the API will be fully and thoroughly documented, including the requirements for authentication, so that anyone can make use of it.
* Backend web service providing an administrative web-based interface for carers and administrators. This provides a service accessible through any web browser, with an interface that is optimised for both mobile and desktop. The service provides management facilities, allowing care providers a way for managing their clients and resources. There is also special administrative features for managing carers, only accessible by privileged users.
* Backend shared data layer providing a consistent way of data access to both the REST and web services. This provides models for all resources, repository interfaces, authority filtering, and a data source configuration with a connection to the database through JDBC.
* Frontend mobile Android application will consume the backend REST service for the resources it requires. This application will be optimised for accessibility and good HCI through adherence to Android design guidelines.
* The following represents the main features (and resources) of the entire system (both backend and frontend):
    * **Reminders** -- Reminders can be set to remind the client of important events, such as to take their medication, have a bite to eat or drink, etc. The client can set these reminders themselves, or they can be set remotely by the care provider.
    * **Tasks** -- Tasks can be set to encourage clear goals to tackle throughout the day. The client can set these tasks themselves, or they can be set remotely by the care provider.
    * **Alerts** -- Provides the client the ability to issue emergency alerts, which in turn alerts the care provider.
    * **Tracking** -- Location of the client can be known at all times to the care provider. This is helpful in times of emergency, which could be particularly useful in especially vulnerable clients.
    * **Contacts** -- Important and frequent contacts can be stored, which could range from the clients doctor/GP, home, etc. If addresses are provided, maps can be fetched for route planning purposes. The client can set these contacts themselves, or they can be set remotely by the care provider.

## Technologies Involved ##
* [Spring IO](http://spring.io) platform and framework, including [Spring Boot](http://projects.spring.io/spring-boot/), [Spring for Android](http://projects.spring.io/spring-android/), [Spring Data](http://projects.spring.io/spring-data/), and [Spring Security](http://projects.spring.io/spring-security/). This will be used in all aspects of the system, including the REST service, web service, and frontend mobile Android application.
* [Android SDK](http://developer.android.com/sdk/index.html) will be used for the frontend mobile Android application used by the elderly clients.
* [Android Studio](http://developer.android.com/tools/studio/index.html) will used as the IDE for Android application development given that it is now stable and the recommended Android development IDE. Gradle is the standard build system for this IDE, so integration with existing work flow will be smooth.
* [Spring Tool Suite](https://spring.io/tools/sts) will be used as the IDE for backend development given that the entire backend utilises Spring heavily. This is just a custom Eclipse tailored for Spring development. Gradle build support is built in, as well as Spring Boot.
* [MySQL](http://www.mysql.com/) will be used as the backend database.
* [Gradle](https://gradle.org) will be the build system for the entire project (both Android Studio and STS support this).
* [Bootstrap](http://getbootstrap.com/) will be used as the web user interface framework for the web service templates.
* [Google ZXing](https://github.com/zxing/zxing/) will be used for QR code image generation and scanning.
* [HikariCP](https://github.com/brettwooldridge/HikariCP) will be used for database connection pooling.
* [Tomcat](https://tomcat.apache.org/) will be used for the web service and REST service server software. This is automatically embedded into a runnable JAR by Spring Boot.

The project is now complete. It has been submitted to MyPlace. The report can be viewed [here](https://drive.google.com/file/d/0ByEyAoaihZW-cXUwWHJMLWFZdzA/view?usp=sharing).

Peace!
