\documentclass[12pt, oneside]{article}  % use "amsart" instead of "article" for AMSLaTeX format
\usepackage[margin=1in]{geometry}       % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   	% ... or a4paper or a5paper or ... 
\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}					% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
										% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{framed}
\usepackage{listings}

\title{
	\vspace{3.5in}
	\textbf{CS408 \\ Project Progress Report \\ \ \\ iCare}
}
\author{
	\href{mailto:john.meikle.2013@uni.strath.ac.uk}{John James Meikle} \\
	Supervised by \href{mailto:clemens.kupke@strath.ac.uk}{Dr Clemens Kupke} \\
	University of Strathclyde
}
\date{Friday, 13th of February 2015}

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage

\section{Introduction}
\subsection{Problem Statement}
The aim of this project is to provide an enterprise ready care platform for use in the context of a care organisation. The project is designed to support and improve the lives of elderly who require or benefit from contact with a care provider, primarily through a frontend mobile and tablet application which connects with an external backend service. The application has features for tasks, reminders, alerts, notifications, tracking and statistics, with the expectation that these will help elderly under care better manage and support their lives. In the interest of further support, care providers are able to remotely manage all features. There is focus on implementing a client-server architecture providing a platform which enables care providers a way for supporting, managing and connecting with their elderly clients through a web-based interface. The aim is to bring carers and their elderly clients closer together.

\subsection{Objectives}
\begin{enumerate}
\item Develop a frontend mobile Android application which demonstrates good HCI and accessibility through adherence to Android design and accessibility guidelines.
\item Develop a REST API service for frontend client consumption which follows RESTful API design.
\item Thoroughly document the REST API service to allow the development of other frontends, thus encouraging extensibility.
\item Develop a way for API consumers to easily authenticate with the service through techniques such as unique authorisation tokens.
\item Develop a web service providing an administration interface for both carers and administrators allowing management of the system through any device with a web browser (mobile, tablet, desktop).
\item Demonstrate practical use of the enterprise grade framework Spring to produce an enterprise ready care platform.
\item Understand the accessibility needs and requirements for elderly.
\item Requirements elicitation from both carers and elderly to figure out what features would benefit the most in such a system.
\item Explore and evaluate the feasibility and usefulness of such a system, both from the client (elderly) and server (care organisation) perspective.
\end{enumerate}

\subsection{Brief Overview of Outcome}
Lorem ipsum...

\subsection{Report Structure}
This report is subsequently structured into several chapters:

\begin{itemize}
\item
\textbf{Chapter 2 -- Survey of Related Work}\\
This chapter of the report entails background research into related work. In particular, two projects have been identified which are employed in a similar context to this project with similar aims.
\item
\textbf{Chapter 3 -- Problem Description and Specification}\\
This chapter of the report describes in detail the problem this project is trying to solve along with a complete specification of the project. The approach taken to solve the problem is also discussed.
\item
\textbf{Chapter 4 -- Description of Curent Implementations}\\
This chapter of the report describes in some detail the current implementations of the project. This includes the prototype applications and the web service.
\item
\textbf{Chapter 5 -- Verification and Validation}\\
This chapter of the report presents the methods used to verify and validate the project.
\item
\textbf{Chapter 6 -- Evaluation}\\
This chapter of the report discusses what ways will be pursued to evaluate this project.
\item
\textbf{Chapter 7 -- Progress Report}\\
This chapter of the report outlines the project development progress to date, as required by the submission.
\end{itemize}

\newpage
\section{Related Work}
This chapter serves as background research for the project. In particular, two projects have been identified as related work because they serve to solve similar problems within the same problem domain. These projects are discussed and evaluated accordingly.

\subsection{Jointly}
\begin{framed}
``\textbf{Jointly} is an app that makes caring for someone a little easier, less stressful and a lot more organised by making communication and coordination between those who share the care as easy as a text message." -- \href{https://www.jointlyapp.com/}{Jointly}
\end{framed}

This app appears to combine group messaging with other useful features including to-do and medication lists, calendar and more. More specific features include:

\begin{itemize}
\item \textbf{Home} -- where you can invite people, see who is in your circle, and view a log of recent activity. 

\item \textbf{Profile} -- where you can store useful information about the person you are looking after and access it any time at a click of a button.

\item \textbf{Messaging} -- where you can communicate with everyone in your circle at a touch of a button.

\item \textbf{Tasks} -- where you can keep organised and on top of things by using tasks and task lists.

\item \textbf{Calendar} -- where you can create date/time specific events and invite anyone in your circle.

\item \textbf{Medications} -- where you can keep track of current and past medication of the person you are caring for. You can also upload an image to quickly recognise a medicine.

\item \textbf{Contacts} -- where you can store useful contacts and access their details anytime, anywhere!
\end{itemize}

\subsubsection{Evaluation}
In contrast to iCare, this application seems to lean on the more social side, providing features such as profile management, circles, messaging, calendar and contacts. However, it does provide a feature for medication tracking and reminding, which iCare specifies.

\subsection{App4Care}
\begin{framed}
``\textbf{App4Care} is an app which provides a suite of seven [contact, location, info, panic alert, tasks, trip/fall, wellbeing] easy to use functions, which help carers at work, living away or on holiday, stay connected with those they care for. [...] connects across two apps -- the carer app downloads at store price and the cared-for app downloads free of charge." -- \href{http://www.app4care.com/}{App4Care}
\end{framed}

This app appears to make use of two mobile apps: one for the carer, and the other for the cared-for. There is support for multiple cared-for to connect with a single carer, allowing a single carer to care for multiple people.

There is functionality for scheduling important tasks and reminders, monitoring wellbeing, panic alarm, trip/fall alert, GPS tracker and zone alert, essential health and contact information, and contact.

\subsubsection{Evaluation}
This application seems to fit the idea of iCare more in terms of functionality, but is architecturally much different. Both the carer and client utilise dedicated mobile applications, rather than a backend web service supplying a web-based administration interface for carer. I think pursuing the web service route for carers is better because it allows them to manage and connect with their clients anywhere they have access to a web browser, which includes desktop and mobile, both optimised for. The limitations I see coming from this is lack of mobile notifications to carer (i.e. there's an emergency!). This can be solved by sending an email or text to the carers specified email address or mobile number. This will act as a notification, providing more information in the content body.

\newpage
\section{Problem Description and Specification}
This chapter provides a description of the problem this project is trying to solve, as well as a full specification of the project.

\subsection{Description}
Providing care, particularly in elderly, is a very important aspect in society. For these people, they need an extra helping hand, and as much support as possible. Everyday life can become harder, more challenging, and perhaps even impossible for some. Such people can benefit greatly from a caregiver, thereby making their lives much easier and overall generally better.

Today, we see greater advent of mobile devices and much greater adoption, including within the elderly. These devices are becoming easier to use, more accessible and user friendly. This therefore provides an excellent platform and opportunity from which to develop a technological approach to caring.

\subsection{Specification}
This project is a care platform designed to support and improve the lives of elderly who require or benefit from contact with a care provider. This project focuses on implementing a client-server architecture providing a platform enabling care providers a way for managing, supporting and connecting with their clients.

In terms of functionality, the application provides a way for clients to seek help and assistance from their carers in times of need. There is further functionality for reminders, tasks, tracking, statistics, notifications, and contact -- all of which can be handled remotely by the carer through an online web-based interface.

More specifically, both the caregiver and client can issue tasks and reminders. The carer can track the location of the client, so whereabouts are known at all times. The carer can request that the client fill out wellbeing figures or other specifiable data, which can provide both the carer and the client useful statistics. Not only are appropriate and relevant notifications produced by reminders and statistical data requests, the carer can issue custom notifications to the client, such as questions or messages. The client can themselves issue messages and emergency alerts to their carer through the application. The client can also be given access to contact details such as phone numbers, as well as addresses, which could range from just their doctor, all the way to commonly visited or important locations.

Architecturally, the mobile application will connect to a remote server backend hosted and managed by the care provider organisation. This server backend will comprise of a web and REST service. The backend will interface with a database storing all user information. There will be a special authentication procedure when making requests to the REST server, specifically designed to make it easy and secure for the client mobile application to identify itself. This authentication procedure will entail a unique token, rather than conventional (often forgettable) username and password credentials. As far as management goes, a web-based administration interface will be provided, accessible through a web browser by an authorised care worker or administrator. This interface will provide a way for carers to register new clients and managing existing ones. The interface will also provide a way for administrators to manage all aspects of the system, including the management of carers, as well as all clients.

\newpage
\section{Description of Current Implementations}
\subsection{Prototype}
A simple (emphasis on simple) prototype was developed to demonstrate a basic client-server architecture. This prototype utilised the Spring IO framework and Android SDK. It did not demonstrate any particular feature of the iCare application, but more of a proof of concept or demonstration of the technologies involved.

The prototype entailed a backend REST service containing one resource mapping called \emph{hello}. This accepted an optional parameter called \emph{name}. When a GET request was made on this resource, we returned a customised response using the supplied \emph{name} parameter. If no parameter was supplied, we merely issued a canned response. The response was returned as a JSON string, ready for parsing. The Android application consisted of two views: one for inputting name and submitting; and the other for displaying the response string.

Overall, although this was an extremely basic prototype, it provided an opportunity to explore all technologies and frameworks involved, thus wetting my feet enough to get me started on real solid work come Semester 2. It is now a case of exploring further and utilising more advanced potential.

\subsubsection{Problems Overcome}
A number of problems were overcome, which made this prototype stage a worthwhile endeavour. I believe these are worth mentioning, so as to get an idea of how my approach to the prototype went. These were:

\begin{itemize}
\item
Initially placed REST service request code in \emph{onCreate()} method in \emph{DisplayMessageActivity} class. This meant that upon creation of the activity, there was code executing which took too long, causing Android to terminate the app on the ground it was "unresponsive". In hindsight, this made sense. The solution was to investigate the \emph{AsyncTask} abstract class, provided by the Android SDK. This abstract class enables proper and easy use of the UI thread, providing a way for us to perform a computational task in the background that publishes results on the UI thread. There are overridable methods for processing input and computing output, reporting progress (optional), and performing some operation on the output (optional).
\item
Android requires permissions to be set in the manifest file if we are performing controlled operations, such as accessing the Internet. The solution was to request use of the \emph{INTERNET} permission.
\item
When running the prototype REST service JAR on my DigitalOcean VPS it would stall (or block) before running main class, even though it worked perfectly fine on home computer. Upon further digging, this appears to be a problem with the generation of the \emph{SecureRandom} instance and is stuck trying to read from an entropy resource, namely \emph{/dev/random}. The solution was to use \emph{/dev/urandom} instead, which does not block. This was done by setting a command-line parameter.
\end{itemize}

\subsection{Web Service}
The web service has been implemented to a functional state providing a way for carers to login and manage their clients. Presently, the administrator aspect of the web service has not yet been implemented, so carers have to be added to the database manually. However, there is support for carer authentication through a login form resulting in a persistent session, from which they can logout from. Functionally, carers can add new clients, as well as view existing clients under their control.

In terms of implementation and technical details, the Spring framework has been utilised considerably. Specifically, a number of key Spring framework components have been identified and used:

\begin{enumerate}
\item \textbf{Spring MVC} -- provides web application support.
\item \textbf{Spring Data} -- provides a way for querying backend JDBC data source in a clean and succinct manner, compared to the conventional way of Java JDBC.
\item \textbf{Spring Security} -- provides authentication and authorisation support.
\item \textbf{Spring Boot} -- provides a way for auto configuring the web application where possible, as well embedding Tomcat and packaging into a standalone runnable JAR.
\end{enumerate}

The web service connects to a MySQL database server serving as its data source. The database presently has tables for carers and clients maintaining the following schema:

\begin{lstlisting}[basicstyle=\footnotesize, frame=single, language=sql]
CREATE TABLE carers(
    username VARCHAR(40) NOT NULL,
    password VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(40),
    first_name VARCHAR(40),
    last_name VARCHAR(40),
    enabled BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY(username));
    
CREATE TABLE clients(
    id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(40),
    last_name VARCHAR(40),
    email VARCHAR(100),
    address VARCHAR(100),
    phone VARCHAR(40),
    auth_token VARCHAR(100),
    carer VARCHAR(40),
    PRIMARY KEY(id),
    FOREIGN KEY(carer) REFERENCES carers(username));
\end{lstlisting}

There will of course be further tables for all resources in the system.

Of particular note, the web service maintains a repository filter. This means that if a carer is logged in, any query made to the backend data source is filtered such that only clients under their control are ever returned. This ensures a level of protection and privacy. This is accomplished by checking the authority role of the currently authorised user. In the system, a carer has the authority role \emph{ROLE\_CARER}, which allows special security policies to be devised. However, an administrator will have the authority role \emph{ROLE\_ADMIN}, which will exempt them from the filter, thus allowing them to access information about any client, as well as of course any carer.

The web service is able to generate an authorisation token (at the moment it's merely a stub function producing a random integer), which is expected to be used by frontend clients included in each request they make to the backend REST service.

As initially planned, Bootstrap has been utilised as the web interface framework. This has worked out very well, providing a mobile (and tablet) friendly interface, as well as of course a spacious desktop version.

\newpage
\section{Verficiation and Validation}
Verification and validation of the software system will be achieved through employing manual use test cases and automated test units.

\newpage
\section{Evaluation}
This project will be evaluated several ways:

\begin{enumerate}
\item Contact with care organisations, especially local, to gauge feasibility of the software system in such a cotext. Also, asking for feedback about this software systems features and how useful it could be.
\item Feedback from someone within the demographic to gauge usability and ease of use with the frontend mobile application. Also, their view(s) on how such a software system would perform in a care context.
\item Evaluation against related work. The software systems identified in related work maintain a standard and serve as worthwhile solutions within the problem domain. The features, design and architecture of all software systems against this software system will be considered.
\end{enumerate}

\newpage
\section{Progress Report}
This section outlines the progress of project development to date:

\subsection{Web Service}
\begin{itemize}
\item
Foundation for web service is built. At the moment, functionality is limited to login (which utilises Spring Security for authentication and authorisation in a session based manner) and the home section. The home section makes available Bulletins and Manage Clients. The latter of which is reasonably functional, allowing the addition of clients to be managed by the authorised carer.
\item
Constructed templates: header (contains header navigation, parameterised alerts for success and error, and header title), footer (contains footer section with copyright), login (contains panel with login form), home (contains main body section of home consisting of tabbed navigation with persistence functionality attained through tab parameter ?- there are two tabs: one for bulletin, and another for manage clients ?- the bulletin tab makes available bulletins posted by an administrator (not implemented yet) and manage clients giving access to an add client form and a table containing the list of clients managed by the authorised carer).
\item
Organised web-service project up into meaningful package structure:
	\begin{itemize}
	\item
	\textbf{org.icare.web} --- contains main entry point class Application which starts the web application.
	\item
	\textbf{org.icare.web.config} --- contains the configuration classes: SecurityConfig which configures the security of the application, ensuring every section of web app requires authorisation other than the login page, and provides authentication with backend JDBC data source; and DataSourceConfig which configures the data source of the application, which in this case is JDBC using MySQL driver connecting to meikj.com MySQL server.
	\item
	\textbf{org.icare.web.controller} --- contains the controllers for the web app: HomeController is used to provide the home section, as well as some experimental client management functionality which will later be refactored into own ClientController; and LoginController which merely provides the string to the login template.
	\item
	\textbf{org.icare.web.data} --- contains all the data repository classes. This includes experimental functionality with a CarerFilter. This interface is implemented by the CarerFilteredJdbcRepository, which checks with the SecurityContext whether or not the queries to data source should be filtered by carer (this protects clients who are not under the care of the authorised carer to be accessible).
	\item
	\textbf{org.icare.web.model} --- contains all the model classes.
	\item
	\textbf{org.icare.web.service} --- contains all the service classes. There is a ClientService class which accesses a ClientRepository. The justification for this class is that it generates the authentication token for the client.
	\end{itemize}
\end{itemize}

\subsection{REST Service}
\begin{itemize}
\item
Some progress with the REST API design. This includes assessing what resources the API should make available. The main resources made available are: tasks, reminders, tracking, contacts, alerts, and statistics.
\item
There has been some effort to push this further by specifying the mapping structures, including the methods available, on some resources (namely reminders, tracking and alerts).
\item
The REST service will not be tied to the web service, even though in some cases they access the same content and use very similar mapping structures. This is because REST is stateless, where as the web service maintains session.
\end{itemize}

\subsection{Database}
\begin{itemize}
\item
Initial SQL schema has been produced for the carer and client tables.
\item
Ideas on how the rest of the tables should be structured are in log book.
\item
MySQL server set up securely on DigitalOcean VPN @ meikj.com
\end{itemize}

\end{document}  
