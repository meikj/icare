\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Problem Statement}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Objectives}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Brief Overview of Outcome}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Report Structure}{4}{subsection.1.4}
\contentsline {section}{\numberline {2}Related Work}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Jointly}{5}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Evaluation}{5}{subsubsection.2.1.1}
\contentsline {subsection}{\numberline {2.2}App4Care}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Evaluation}{6}{subsubsection.2.2.1}
\contentsline {section}{\numberline {3}Problem Description and Specification}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Description}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Specification}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Description of Current Implementations}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Prototype}{9}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Problems Overcome}{9}{subsubsection.4.1.1}
\contentsline {subsection}{\numberline {4.2}Web Service}{10}{subsection.4.2}
\contentsline {section}{\numberline {5}Verficiation and Validation}{12}{section.5}
\contentsline {section}{\numberline {6}Evaluation}{13}{section.6}
\contentsline {section}{\numberline {7}Progress Report}{14}{section.7}
\contentsline {subsection}{\numberline {7.1}Web Service}{14}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}REST Service}{15}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Database}{15}{subsection.7.3}
