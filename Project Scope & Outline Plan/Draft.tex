\documentclass[10pt, oneside]{article}  % use "amsart" instead of "article" for AMSLaTeX format
\usepackage[margin=0.7in]{geometry}       % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   	% ... or a4paper or a5paper or ... 
\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}					% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
										% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{framed}

\title{\textbf{\vspace{-2.5cm} \\ CS408 \\ Project Scope \& Outline Plan \\ \ \\ iCare}}
\author{
	\href{mailto:john.meikle.2013@uni.strath.ac.uk}{John James Meikle} \\
	Supervised by \href{mailto:clemens.kupke@strath.ac.uk}{Dr Clemens Kupke} \\
	University of Strathclyde
}
\date{Wednesday, 5th of November 2014}

\begin{document}
\maketitle
\begin{center}
\line(1,0){500}
\end{center}

\section{Overview of Project}
The project \emph{iCare} is a mobile and tablet application designed to support and improve the lives of people with potentially special needs (e.g. elderly, mentally ill or impaired, sick) who require contact with a care provider. This project focuses on implementing a client-server architecture providing a platform which enables care providers a way of managing and connecting with their patients. This brings carers and their patients closer together, providing a way for patients to seek help and assistance in times of need -- all at the tap of a button -- as well as kept up to date with their medication, and possibly food or water intake, through reminders and tracking.

More specifically, the mobile application will connect to a remote server hosted and managed by the care provider organisation. The server will interface with a database storing all user information. There will be an authentication procedure after connecting to the server, designed to make it easy and secure for the client mobile application to identify itself. As far as management goes, a web based administration interface will be provided, accessible through a web browser by an authorised care worker. This interface will provide a way for registering new clients and managing existing ones.

As far as functionality goes, this system will allow care providers through the web-based interface to remotely set and manage reminders for their patients, view their location, see how they're doing in terms of what wellbeing figures are being provided, provide them a way of requesting assistance or emergency, and potentially detect falling accidents that would trigger an emergency alert.

\section{Preliminary List of Objectives}
\begin{enumerate}
\item Investigate, explore and gain familiarity with \href{https://developer.android.com/sdk/index.html?hl=i}{Android SDK} in preparation for developing mobile and tablet application, including networking, notifications, user interface, device sensors and persistent storage.

\item Investigate, explore and gain familiarity with the \href{http://spring.io/}{Spring IO} platform for developing a \href{http://spring.io/guides/gs/serving-web-content/}{web service} and \href{http://spring.io/guides/gs/rest-service/}{RESTful API}. The former will be used for the web based administration interface, the latter will be \href{http://spring.io/guides/gs/consuming-rest-android/}{consumed by the mobile application}.

\item Develop a custom server backend to support a client-server architecture.

\item Interface with a database server backend, most likely \href{http://www.mysql.com/}{MySQL} or \href{http://www.postgresql.org/}{PostgreSQL}.

\item Appreciate the need for good user interface design, particularly if elderly and impaired individuals are concerned. This will entail careful attention to human-computer interaction and accessibility.

\item Demonstrate practical application and use of standard design patterns, such as MVC.

\item Provide substantial documentation covering both the internal architecture of the system and the RESTful API. The latter of which will particularly allow for other applications (or extensions) to be developed that can interact with the server backend.

\item Provide substantial test cases and units covering the entire backend.

\item Overcome challenges in security and authentication.
\end{enumerate}

\section{Preliminary Survey of Related Work}
\subsection{Jointly}
\begin{framed}
``\textbf{\href{https://www.jointlyapp.com/}{Jointly}} is an app that makes caring for someone a little easier, less stressful and a lot more organised by making communication and coordination between those who share the care as easy as a text message. [...] This app is brought to you by \href{http://www.carersuk.org/}{Carers UK}."
\end{framed}

This app appears to combine group messaging with other useful features including to-do and medication lists, calendar and more. More specific features include:

\begin{itemize}
\item \textbf{Home} -- where you can invite people, see who is in your circle, and view a log of recent activity. 

\item \textbf{Profile} -- where you can store useful information about the person you are looking after and access it any time at a click of a button.

\item \textbf{Messaging} -- where you can communicate with everyone in your circle at a touch of a button.

\item \textbf{Tasks} -- where you can keep organised and on top of things by using tasks and task lists.

\item \textbf{Calendar} -- where you can create date/time specific events and invite anyone in your circle.

\item \textbf{Medications} -- where you can keep track of current and past medication of the person you are caring for. You can also upload an image to quickly recognise a medicine.

\item \textbf{Contacts} -- where you can store useful contacts and access their details anytime, anywhere!
\end{itemize}

\subsection{App4Care}
\begin{framed}
``\href{http://www.app4care.com/}{\textbf{App4Care}} is an app which provides a suite of seven [contact, location, info, panic alert, tasks, trip/fall, wellbeing] easy to use functions, which help carers at work, living away or on holiday, stay connected with those they care for. [...] connects across two apps -- the carer app downloads at store price and the cared-for app downloads free of charge."
\end{framed}

This app appears to make use of two mobile apps: one for the carer, and the other for the cared-for. There is support for multiple cared-for to connect with a single carer, allowing a single carer to care for multiple people.

There is functionality for scheduling important tasks and reminders, monitoring wellbeing, panic alarm, trip/fall alert, GPS tracker and zone alert, essential health and contact information, and contact.

\section{Brief Overview of Methodology}
In terms of project specification, the original (KUPK01) specification will be taken as inspiration and extended further with more useful features, but at the same time fleshing out existing parts of the original specification.

In terms of project design, a client-server architecture will be pursued. The backend will comprise of a server working as a web and RESTful service connected to a database. The frontend will comprise of a mobile Android app which connects to the server and consumes its RESTful service, and a web-based administration interface supplied by the backend web service.

In terms of project implementation, it is likely the approach will mainly consist of Java. This means that the client-side mobile app will be programmed in Java using the Android SDK in conjunction with the Spring IO framework. And as far as the backend is concerned, Java will be used to develop the server application using the Spring IO framework allowing for the implementation of a web and RESTful service.

In terms of project verification, the project could potentially be verified against real people within the actual relevant demographic (i.e. elderly, mentally ill or impaired, sick) through contacting a care provider organisation willing to help. Not only this, but the iteratively developed test cases and use cases will enable verification of the features and workability of the system.

\section{Brief Overview of End Project Evaluation}
The end project will be evaluated by ensuring it meets the project specification and requirements. I will do this by testing the project against predefined manual use cases and automated test cases. Since iterative test driven development will be used, this should be fairly straightforward.

It is important during the evaluation period we test the project in the context for which it is designed for (i.e. remote server maintained by care organisation of which client mobile applications connect to over the Internet) so we can be certain of its usability. We must also evaluate the feasibility of offline scenarios.

Not only this, but I will consider getting in touch with relevant care organisations and asking for their thoughts on the project and application, as well as possibly getting relevant people within the demographic to try out the product, thus gaining real world feedback on usability, features, viability, etc.

\section{Initial Project Plan}
\begin{center}
\begin{tabular}{|l||l|}
\hline
\textbf{Semester 1} & \textbf{Semester 2} \\
\hline

\begin{tabular}[t]{c|p{5cm}}
\textbf{Week} & \textbf{Milestone} \\
6 & Finalise Project Scope \& Outline Plan. \\
7 & Further investigate and explore Android SDK and Spring IO. \\
8 & Settle on backend database solution and ensure familiarity with integration and usage. \\
9 & Initial client-server prototype (basic mobile app connecting to basic backend server). \\
10 & Ensure preparation for Full Project Specification/Plan and Project Poster is fully under way. \\
11 & Finalise Full Project Specification/Plan and Project Poster. \\
12 & End of semester, reflect on previous weeks work. \\
\ \\
\end{tabular}

&

\begin{tabular}[t]{c|p{8cm}}
\textbf{Week} & \textbf{Milestone} \\
1 & Ensure database backend is up and running with an initial skeletal schema. \\
2 & Backend web service for web-based administration interface. \\
3 & Finalise Project Progress Report. \\
4 & Backend RESTful service for communicating with server and consuming content. \\
5 & Consume RESTful service with mobile Android app. \\
6 & Finalise Project Report Outline and (some) Draft Chapters. \\
7 & Deal with potential authentication and security concerns. \\
8+9 & Finalise implementation, testing and documentation of server backend. \\
8+9 & Finalise implementation, testing and documentation of client frontend. \\
10 & Finalise Project Report for electronic submission. \\
11 & Submission of two bound copies of Project Report. Demo project. \\
\end{tabular}\\

\hline
\end{tabular}
\end{center}

\section{Choice of Marking Scheme}
Taking into consideration the predominantly software-oriented project objectives and the substantial practical element involved (i.e. client-server architecture, mobile app, etc.), it is therefore agreed upon that this project is a \emph{Software Development-based Project}. The following marking scheme will therefore be used:

\begin{itemize}
\item Student Performance - 10\%
\item Project Product - 35\%
	\begin{itemize}
		\item Software - 25\%
		\item Documentation - 10\%
	\end{itemize}
\item Project Process - 30\%
	\begin{itemize}
		\item Background study, requirements analysis, and design - 20\%
		\item Testing - 10\%
	\end{itemize}
\item Project Results and Evaluation - 15\%
\item Report Presentation - 10\%
\end{itemize}

\end{document}  
