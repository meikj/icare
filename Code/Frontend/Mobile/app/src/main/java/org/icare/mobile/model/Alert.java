package org.icare.mobile.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents an alert model. Provides properties for id, type, coords,
 * acknowledgement, date and time, and the associated client ID.
 * 
 * @author John Meikle
 *
 */
@JsonIgnoreProperties({"dateTimeString", "dateFormat"})
public class Alert extends Resource {

	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	private String type;
	private String coords;
	private boolean ack;
	private Date dateTime;

	public SimpleDateFormat dateFormat = new SimpleDateFormat(
			DATE_TIME_PATTERN);

	public Alert() {
		// for construction via getters setters
	}

	public Alert(int id, String type, String coords, boolean ack,
			Date dateTime, int clientId) {
		super(id, clientId);
		this.type = type;
		this.coords = coords;
		this.ack = ack;
		this.dateTime = dateTime;
	}

	public Alert(int id, String type, String coords, boolean ack,
			Timestamp dateTimestamp, int clientId) {
		super(id, clientId);
		this.type = type;
		this.coords = coords;
		this.ack = ack;
		this.dateTime = new Date(dateTimestamp.getTime());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCoords() {
		return coords;
	}

	public void setCoords(String coords) {
		this.coords = coords;
	}

	public boolean isAck() {
		return ack;
	}

	public void setAck(boolean ack) {
		this.ack = ack;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public void setDateTimestamp(Timestamp dateTimestamp) {
		this.dateTime = new Date(dateTimestamp.getTime());
	}

	public String getDateTimeString() {
		return dateFormat.format(dateTime.getTime());
	}

    @Override
    public String toString() {
        return coords;
    }

}
