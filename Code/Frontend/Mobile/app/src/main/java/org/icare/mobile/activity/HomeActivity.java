package org.icare.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.icare.mobile.App;
import org.icare.mobile.R;
import org.icare.mobile.activity.task.SendAlertTask;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;
import org.icare.mobile.service.TrackingService;

/**
 * This is the main activity which presents the user with 'home page' for accessing the core
 * features of the application (Tasks, Reminders, Contacts, Tracks, and Alert). There is also a
 * menu providing links to Settings and an About dialog.
 *
 * @author John Meikle
 */
public class HomeActivity extends LocationAwareActivity implements SendAlertTask.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Select and transition to the contacts activity.
     *
     * @param view the view.
     */
    public void selectContacts(View view) {
        Intent intent = new Intent(this, ResourceListActivity.class);
        intent.putExtra(ResourceListActivity.TITLE, "Contacts");
        intent.putExtra(ResourceDetailActivity.TITLE, "Contact Detail");
        intent.putExtra(ResourceType.RESOURCE_TYPE, ResourceType.CONTACTS);
        startActivity(intent);
    }

    /**
     * Select and transition to the tasks activity.
     *
     * @param view the view.
     */
    public void selectTasks(View view) {
        Intent intent = new Intent(this, ResourceListActivity.class);
        intent.putExtra(ResourceListActivity.TITLE, "Tasks");
        intent.putExtra(ResourceDetailActivity.TITLE, "Task Detail");
        intent.putExtra(ResourceType.RESOURCE_TYPE, ResourceType.TASKS);
        startActivity(intent);
    }

    /**
     * Select and transition to the reminders activity.
     *
     * @param view the view.
     */
    public void selectReminders(View view) {
        Intent intent = new Intent(this, ResourceListActivity.class);
        intent.putExtra(ResourceListActivity.TITLE, "Reminders");
        intent.putExtra(ResourceDetailActivity.TITLE, "Reminder Detail");
        intent.putExtra(ResourceType.RESOURCE_TYPE, ResourceType.REMINDERS);
        startActivity(intent);
    }

    /**
     * Select and transition to the tracks activity.
     *
     * @param view the view.
     */
    public void selectTracks(View view) {
        Intent intent = new Intent(this, TracksActivity.class);
        intent.putExtra(TrackingService.CURRENT_LOCATION, getCurrentLocationCoords());
        startActivity(intent);
    }

    /**
     * Make an alert. This uses the current location coordinates.
     *
     * @param view the view.
     */
    public void makeAlert(View view) {
        if (getCurrentLocationCoords() != null)
            new SendAlertTask(((App) getApplicationContext()).getRest(), this).execute(getCurrentLocationCoords());
    }

    @Override
    public void onAlertSuccess(Resource resource) {
        Toast.makeText(HomeActivity.this, "Alert sent to carer!", Toast.LENGTH_LONG).show();

        // Disable button until alert is acknowledged
        //((Button) findViewById(R.id.alert_button)).setEnabled(false);

        // TODO: Show a dialog with alert information and which listens for an acknowledgement
    }

    @Override
    public void onAlertError(Resource resource, String message) {
        Toast.makeText(HomeActivity.this, "Alert sending failed\n(" + message + ")", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart() {
        super.onStart();

        // This ensures the TrackingService sends out a last location broadcast when this Activity
        // starts.
        ((App) getApplicationContext()).restartTrackingService();
    }

    @Override
    protected void onUpdateCurrentLocation(String coords) {
        super.onUpdateCurrentLocation(coords);

        Log.d("HomeActivity", "Updating current location: " + coords);
    }

}
