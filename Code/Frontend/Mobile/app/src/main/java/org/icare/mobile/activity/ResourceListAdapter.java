package org.icare.mobile.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.icare.mobile.R;
import org.icare.mobile.model.Contact;
import org.icare.mobile.model.Reminder;
import org.icare.mobile.model.Resource;
import org.icare.mobile.model.Task;

import java.util.List;

/**
 * This is the list adapter for presenting a list of resources.
 *
 * @author John meikle
 */
public class ResourceListAdapter extends ArrayAdapter<Resource> {

    private final LayoutInflater layoutInflater;

    /**
     * Construct a new ResourceListAdapter with context.
     *
     * @param context the context.
     */
    public ResourceListAdapter(Context context) {
        super(context, R.layout.list_resource_row);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Set the data for the list.
     *
     * @param data the new data.
     */
    public void setData(List<? extends Resource> data) {
        clear();

        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.list_resource_row, parent, false);
        } else {
            view = convertView;
        }

        Resource resource = getItem(position);
        ((TextView) view.findViewById(R.id.list_resource_title)).setText(resource.toString());

        TextView des = (TextView) view.findViewById(R.id.list_resource_description);
        if (des != null) {
            // Only tasks, reminders and contacts support description
            if (resource instanceof Contact) {
                des.setText(((Contact) resource).getDescription());
            } else if (resource instanceof Task) {
                des.setText(((Task) resource).getDescription());
            } else if (resource instanceof Reminder) {
                des.setText(((Reminder) resource).getDescription());
            }
        }

        return view;
    }

}
