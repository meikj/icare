package org.icare.mobile.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.icare.mobile.R;
import org.icare.mobile.model.Contact;

/**
 * This is the form fragment implementation for a contact.
 *
 * @author John Meikle
 */
public class ContactFormFragment extends ResourceFormFragment {

    private Contact contact;

    private EditText name;
    private EditText description;
    private EditText phone;
    private EditText email;
    private EditText address;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (resource != null && resource instanceof Contact) {
            contact = (Contact) this.resource;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_form, container, false);

        name = (EditText) rootView.findViewById(R.id.contact_detail_name);
        description = (EditText) rootView.findViewById(R.id.contact_detail_description);
        phone = (EditText) rootView.findViewById(R.id.contact_detail_phone);
        email = (EditText) rootView.findViewById(R.id.contact_detail_email);
        address = (EditText) rootView.findViewById(R.id.contact_detail_address);
        ((Button) rootView.findViewById(R.id.resource_form_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateResource();
                callbacks.onButtonSelected(contact);
            }
        });

        if (contact == null) {
            contact = new Contact();
            resource = contact;
        } else {
            name.setText(contact.getFirstName() + " " + contact.getLastName());
            description.setText(contact.getDescription());
            phone.setText(contact.getPhone());
            email.setText(contact.getEmail());
            address.setText(contact.getAddress());
        }

        return rootView;
    }

    public void updateResource() {
        String[] names = name.getText().toString().split(" ");
        if (names.length >= 1) contact.setFirstName(names[0]); else contact.setFirstName("");
        if (names.length >= 2) contact.setLastName(names[1]); else contact.setLastName("");
        contact.setDescription(description.getText().toString());
        contact.setPhone(phone.getText().toString());
        contact.setEmail(email.getText().toString());
        contact.setAddress(address.getText().toString());
    }

}
