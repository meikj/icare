package org.icare.mobile.activity.task;

import android.os.AsyncTask;
import android.util.Log;

import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;
import org.icare.mobile.model.Track;
import org.icare.mobile.rest.RestAPI;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestClientException;

import java.util.Date;

/**
 * This task sends a track to the backend REST API with a coordinate string.
 *
 * @author John Meikle
 */
public class SendTrackTask extends AsyncTask<String, Void, Boolean> {

    private RestAPI rest;
    private String errorMessage;
    private Callbacks callbacks;
    private Track track;

    /**
     * Construct a new SendTrackTask.
     *
     * @param rest the REST API object.
     * @param callbacks the class to callback to.
     */
    public SendTrackTask(RestAPI rest, Callbacks callbacks) {
        this.rest = rest;
        this.callbacks = callbacks;
        track = new Track();

        if (callbacks == null) this.callbacks = dummyCallbacks;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String urlResource = ResourceType.getUrlResource(ResourceType.TRACKS);
        ParameterizedTypeReference responseType = ResourceType.getResponseType(ResourceType.TRACKS);
        String coords = params[0];
        track.setCoords(coords);
        track.setDateTime(new Date());

        try {
            track = (Track) rest.add(urlResource, responseType, track);
        } catch (RestClientException e) {
            Log.e("TrackTask",
                    "Error adding track (" + e.getMessage() + ").");
            errorMessage = e.getMessage();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean response) {
        if (response) {
            // All is good
            callbacks.onTrackSuccess(track);
        } else {
            // Something went wrong
            callbacks.onTrackError(track, errorMessage);
        }
    }

    /**
     * A callback interface for reporting track success and failure.
     */
    public interface Callbacks {

        /**
         * Called upon a successful track.
         *
         * @param resource the resulting resource.
         */
        public void onTrackSuccess(Resource resource);

        /**
         * Called upon an unsuccessful track.
         *
         * @param resource the original resource.
         * @param message an error message.
         */
        public void onTrackError(Resource resource, String message);

    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. This is used when callbacks is not set (i.e. null).
     */
    private static Callbacks dummyCallbacks = new Callbacks() {
        @Override
        public void onTrackSuccess(Resource resource) { }
        @Override
        public void onTrackError(Resource resource, String message) { }
    };

}
