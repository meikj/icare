package org.icare.mobile.activity.fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.util.Log;

import org.icare.mobile.App;
import org.icare.mobile.loader.ResourceLoader;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;

import java.util.List;

/**
 * This is an abstract implementation for a resource form fragment. This provides a callback
 * interface so that an activity can be notified of the submit button press. The ID of the resource
 * to fetch can be passed by the ResourceType.RESOURCE_ID argument. This class uses a ResourceLoader
 * to fetch up to date content.
 *
 * @author John Meikle
 */
public abstract class ResourceDetailFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<List<? extends Resource>> {

    /**
     * The resource ID.
     */
    protected int resourceId;

    /**
     * The resource type.
     */
    protected ResourceType resourceType;

    /**
     * The resource.
     */
    protected Resource resource;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ResourceDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("RESOURCE_ID")) {
            resourceId = getArguments().getInt("RESOURCE_ID");
        }
        if (getArguments().containsKey("RESOURCE_TYPE")) {
            resourceType = (ResourceType) getArguments().get("RESOURCE_TYPE");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    public static ResourceDetailFragment newInstance(ResourceType resourceType) throws IllegalArgumentException {
        if (resourceType == null)
            throw new IllegalArgumentException("resourceType cannot be null.");

        Bundle args = new Bundle();
        ResourceDetailFragment fragment;

        switch (resourceType) {
            case CONTACTS:
                fragment = new ContactDetailFragment();
                break;
            case TASKS:
                fragment = new TaskDetailFragment();
                break;
            case REMINDERS:
                fragment = new ReminderDetailFragment();
                break;
            default:
                return null;
        }

        args.putSerializable(ResourceType.RESOURCE_TYPE, resourceType);
        fragment.setArguments(args);
        Log.d("ResourceDetailFragment", "New instance (" + resourceType.name() + ").");
        return fragment;
    }

    @Override
    public Loader<List<? extends Resource>> onCreateLoader(int id, Bundle args) {
        Log.d("ResourceListFragment", "Creating ResourceLoader for " + resourceType.name());
        return new ResourceLoader(getActivity(), ((App) getActivity().getApplicationContext()).getRest(), resourceType);
    }

    @Override
    public void onLoadFinished(Loader<List<? extends Resource>> loader, List<? extends Resource> data) {
        for (Resource r : data) {
            if (r.getId() == resourceId) {
                resource = r;
                loadResource(resource);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<List<? extends Resource>> loader) {
       resource = null;
    }

    /**
     * Called when the resource is loaded from the ResourceLoader.
     *
     * @param resource the resulting resource.
     */
    public abstract void loadResource(Resource resource);

}
