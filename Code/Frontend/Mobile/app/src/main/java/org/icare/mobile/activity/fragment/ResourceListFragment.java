package org.icare.mobile.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import org.icare.mobile.App;
import org.icare.mobile.activity.ResourceListAdapter;
import org.icare.mobile.loader.ResourceLoader;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;

import java.util.List;

/**
 * A list fragment representing a list of Resources. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link ResourceDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ResourceListFragment extends ListFragment
        implements LoaderManager.LoaderCallbacks<List<? extends Resource>> {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    private ResourceListAdapter adapter;
    private List<? extends Resource> resources;
    private ResourceType resourceType;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(Resource resource);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(Resource resource) { }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ResourceListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ResourceType.RESOURCE_TYPE)) {
            resourceType = (ResourceType) getArguments().getSerializable(ResourceType.RESOURCE_TYPE);
            Log.d("ResourceListFragment", "resourceType set to " + resourceType.name());
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText("No resources");
        adapter = new ResourceListAdapter(getActivity());
        setListAdapter(adapter);
        setListShown(false);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        Resource resource = resources.get(position);
        mCallbacks.onItemSelected(resource);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    @Override
    public Loader<List<? extends Resource>> onCreateLoader(int id, Bundle args) {
        Log.d("ResourceListFragment", "Creating ResourceLoader for " + resourceType.name());
        return new ResourceLoader(getActivity(), ((App) getActivity().getApplicationContext()).getRest(), resourceType);
    }

    @Override
    public void onLoadFinished(Loader<List<? extends Resource>> loader, List<? extends Resource> data) {
        resources = data;
        adapter.setData(data);

        if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<? extends Resource>> loader) {
        adapter.setData(null);
    }

    /**
     * Factory method for constructing new ResourceListFragments. This accepts a resource type,
     * which specifies what resources the list will represent.
     *
     * @param resourceType the resource type.
     * @return the new ResourceListFragment.
     * @throws java.lang.IllegalArgumentException if resourceType is null.
     */
    public static ResourceListFragment newInstance(ResourceType resourceType) throws IllegalArgumentException {
        if (resourceType == null)
            throw new IllegalArgumentException("resourceType cannot be null.");

        ResourceListFragment fragment = new ResourceListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ResourceType.RESOURCE_TYPE, resourceType);
        fragment.setArguments(args);
        Log.d("ResourceListFragment", "New instance (" + resourceType.name() + ").");
        return fragment;
    }

}
