package org.icare.mobile.activity;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.icare.mobile.App;
import org.icare.mobile.R;
import org.icare.mobile.loader.ResourceLoader;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;
import org.icare.mobile.model.Track;
import org.icare.mobile.service.TrackingService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the tracks activity which presents the user with an overall view of their tracks.
 *
 * @author John Meikle
 */
public class TracksActivity extends LocationAwareActivity
        implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<List<? extends Resource>> {

    /**
     * The zoom value for the map camera.
     */
    private static final int MAP_ZOOM = 12;

    /**
     * The list of all loaded tracks.
     */
    private List<Track> tracks;

    /**
     * Shown to the user during track loading.
     */
    private ProgressDialog progressDialog;

    /**
     * The list of scaled plots. This is just coordinates that have been scaled/rounded so that
     * subsequent plots aren't too close.
     */
    private List<String> scaledPlots;

    @Override
    public void onStart() {
        super.onStart();
        progressDialog.show();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks);

        // Fallback current location
        if (currentLocationCoords == null)
            currentLocationCoords = getIntent().getStringExtra(TrackingService.CURRENT_LOCATION);

        // Set up progress dialog
        progressDialog = new ProgressDialog(TracksActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading tracks...");
        progressDialog.setCancelable(false);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    protected void onUpdateCurrentLocation(String coords) {
        super.onUpdateCurrentLocation(coords);

        Log.d("TracksActivity", "Updating current location: " + coords);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        String[] coordStringSplit = getCurrentLocationCoords().split(",");
        double lat = Double.parseDouble(coordStringSplit[0]);
        double lng = Double.parseDouble(coordStringSplit[1]);
        LatLng coords = new LatLng(lat, lng);

        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coords, MAP_ZOOM));

        map.addMarker(new MarkerOptions().title("Current Location").snippet("Your currently recorded location.").position(coords));
        isPlotted(coords); // ensures a scaled plot exists for current location

        // Add past locations to map
        addPastLocations(map);
    }

    @Override
    public Loader<List<? extends Resource>> onCreateLoader(int id, Bundle args) {
        ResourceType resourceType = ResourceType.TRACKS;
        Log.d("TracksActivity", "Creating ResourceLoader for " + resourceType.name());
        return new ResourceLoader(this, ((App) getApplicationContext()).getRest(), resourceType);
    }

    @Override
    public void onLoadFinished(Loader<List<? extends Resource>> loader, List<? extends Resource> data) {
        if (tracks == null) {
            tracks = new ArrayList<>();
            scaledPlots = new ArrayList<>();
        }

        // Add all tracks to the list
        for (Resource r : data) {
            if (r instanceof Track) {
                tracks.add((Track) r);
            }
        }

        // Load finished, dismiss progress dialog
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        // Create the map fragment now that tracks have been loaded
        createMapFragment();
    }

    @Override
    public void onLoaderReset(Loader<List<? extends Resource>> loader) {
        tracks = null;
        scaledPlots = null;
    }

    /**
     * Create the MapFragment.
     */
    private void createMapFragment() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.tracks_map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Add past locations from tracks to the map.
     *
     * @param map the map.
     */
    private void addPastLocations(GoogleMap map) {
        for (Track t : tracks) {
            LatLng coords = coordsToLatLng(t.getCoords());

            if (!isPlotted(coords)) {
                Log.d("TracksActivity", String.format("Adding past location %d: %s", t.getId(), t.getCoords()));
                String title = String.format("Tracked location (ID: %d)", t.getId());
                String snippet = String.format("Your recorded location on %s.", t.getDateTimeString());
                map.addMarker(new MarkerOptions().title(title).snippet(snippet).position(coords));
            } else {
                Log.d("TracksActivity", "There's already a plot within range to: " + t.getCoords());
            }
        }
    }

    /**
     * Convert a coordinate string to LatLng.
     *
     * @param coords the coordinate string.
     * @return the resulting LatLng.
     */
    private LatLng coordsToLatLng(String coords) {
        String[] coordStringSplit = coords.split(",");
        double parsedLat = Double.parseDouble(coordStringSplit[0]);
        double parsedLng = Double.parseDouble(coordStringSplit[1]);
        return new LatLng(parsedLat, parsedLng);
    }

    /**
     * Check if the coordinates are already plotted within range. To check within range, the
     * coordinates are scaled to 3 decimal places, then compared.
     *
     * @param coords the coordinates.
     * @return  If no scaled plot exists, it is added and this method returns false. Otherwise an
     * identical scaled plot exists, therefore already plotted, and returns true.
     */
    private boolean isPlotted(LatLng coords) {
        BigDecimal scaledLat = BigDecimal.valueOf(coords.latitude).setScale(3, BigDecimal.ROUND_HALF_UP);
        BigDecimal scaledLng = BigDecimal.valueOf(coords.longitude).setScale(3, BigDecimal.ROUND_HALF_UP);
        String scaledPlot = String.format("%s,%s", scaledLat.toString(), scaledLng.toString());

        if (!scaledPlots.contains(scaledPlot)) {
            scaledPlots.add(scaledPlot);
            return false;
        }

        return true;
    }

}
