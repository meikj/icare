package org.icare.mobile.activity.fragment;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import org.icare.mobile.App;
import org.icare.mobile.R;
import org.icare.mobile.config.AppConfig;

/**
 * Created by john on 27/03/15.
 */
public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

    public static final String KEY_REST_API_URL = "pref_key_rest_api_url";
    public static final String KEY_REST_API_PORT = "pref_key_rest_api_port";
    public static final String KEY_REST_API_AUTH_CLEAR = "pref_key_rest_api_auth_clear";
    public static final String KEY_REST_API_AUTH_REGEN = "pref_key_rest_api_auth_regen";
    public static final String KEY_TRACKING_ENABLED = "pref_key_tracking_enabled";
    public static final String KEY_TRACKING_INTERVAL = "pref_key_tracking_interval";

    private AppConfig appConfig;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load preferences XML
        addPreferencesFromResource(R.xml.preferences);

        // Save the preferences for registering/unregistering a listener
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());


        // Fetch a copy of the AppConfig from the parent activity (SettingsActivity)
        appConfig = new AppConfig(((App) getActivity().getApplicationContext()).getAppConfig().getKeyValue());

        // Set the settings values
        setValues(sharedPreferences);
    }

    /**
     * Set the preference values and summaries from appConfig.
     */
    private void setValues(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Preference restUrlPref = findPreference(KEY_REST_API_URL);
        restUrlPref.setDefaultValue(appConfig.getRestUrl());
        restUrlPref.setSummary(appConfig.getRestUrl());
        editor.putString(KEY_REST_API_URL, appConfig.getRestUrl());

        Preference restPortPref = findPreference(KEY_REST_API_PORT);
        restPortPref.setDefaultValue(appConfig.getRestPort());
        restPortPref.setSummary(Integer.toString(appConfig.getRestPort()));
        editor.putString(KEY_REST_API_PORT, Integer.toString(appConfig.getRestPort()));

        Preference trackEnabledPref = findPreference(KEY_TRACKING_ENABLED);
        trackEnabledPref.setDefaultValue(appConfig.isTrackEnabled());
        trackEnabledPref.setSummary("Tracking is " + (appConfig.isTrackEnabled() ? "enabled." : "disabled."));
        editor.putBoolean(KEY_TRACKING_ENABLED, appConfig.isTrackEnabled());

        Preference trackIntervalPref = findPreference(KEY_TRACKING_INTERVAL);
        trackIntervalPref.setDefaultValue(appConfig.getTrackInterval());
        trackIntervalPref.setSummary(Integer.toString(appConfig.getTrackInterval()) + " milliseconds");
        editor.putString(KEY_TRACKING_INTERVAL, Integer.toString(appConfig.getTrackInterval()));

        editor.apply();

        // Add click listeners to auth token clear and regen
        // TODO
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_REST_API_URL)) {
            String restUrl = sharedPreferences.getString(key, "");
            Log.d("SettingsFragment", "restUrl = " + restUrl);
            appConfig.setRestUrl(restUrl);
        } else if (key.equals(KEY_REST_API_PORT)) {
            int restPort = Integer.parseInt(sharedPreferences.getString(key, ""));
            Log.d("SettingsFragment", "restPort = " + restPort);
            appConfig.setRestPort(restPort);
        } else if (key.equals(KEY_TRACKING_ENABLED)) {
            boolean trackEnabled = sharedPreferences.getBoolean(key, false);
            Log.d("SettingsFragment", "trackEnabled = " + trackEnabled);
            appConfig.setTrackEnabled(trackEnabled);
        } else if (key.equals(KEY_TRACKING_INTERVAL)) {
            int trackInterval = Integer.parseInt(sharedPreferences.getString(key, ""));
            Log.d("SettingsFragment", "trackInterval = " + trackInterval);
            appConfig.setTrackInterval(trackInterval);
        }
        setValues(sharedPreferences);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Start listening for changes
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        // Stop listening for changes
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);

        // Load the new appConfig into the application
        ((App) getActivity().getApplicationContext()).loadAppConfig(appConfig);

        // Save the appConfig to file
        ((App) getActivity().getApplicationContext()).saveAppConfig();
    }

}
