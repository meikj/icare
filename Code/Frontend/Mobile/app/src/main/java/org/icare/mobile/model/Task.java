package org.icare.mobile.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a task model.
 *
 * @author John Meikle
 *
 */
@JsonIgnoreProperties({"startDateTime", "endDateTime", "dateFormat"})
public class Task extends Resource {

    private static final long serialVersionUID = 7113919541914827751L;

    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm";

    private String title;
    private String description;
    private String location;
    private boolean complete;
    private Date startDate;
    private Date endDate;

    public SimpleDateFormat dateFormat = new SimpleDateFormat(
            DATE_TIME_PATTERN);

    public Task() {
        // for construction via getters and setters
    }

    public Task(int id, String title, String description, Date startDate,
                Date endDate, String location, boolean complete, int clientId) {
        super(id, clientId);
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.complete = complete;
    }

    public Task(int id, String title, String description,
                Timestamp startTimestamp, Timestamp endTimestamp, String location,
                boolean complete, int clientId) {
        super(id, clientId);
        this.title = title;
        this.description = description;
        this.startDate = new Date(startTimestamp.getTime());
        this.endDate = new Date(endTimestamp.getTime());
        this.location = location;
        this.complete = complete;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateTime() {
        return dateFormat.format(getStartDate().getTime());
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStartDateTimestamp(Timestamp startTimestamp) {
        this.startDate = new Date(startTimestamp.getTime());
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateTime() {
        return dateFormat.format(getEndDate().getTime());
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setEndDateTimestamp(Timestamp endTimestamp) {
        this.endDate = new Date(endTimestamp.getTime());
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    @Override
    public String toString() {
        return title;
    }

}
