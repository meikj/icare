package org.icare.mobile.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a client model. Provides properties for id, name, address, phone,
 * email, auth token and their carers username.
 * 
 * @author John Meikle
 *
 */
public class Client extends Resource {

	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy";

	private String firstName;
	private String lastName;
	private String address;
	private String phone;
	private String email;
	private String authToken;
	private int userId;
	private Date dob;

	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			DATE_TIME_PATTERN);

	/**
	 * Construct an empty client model.
	 */
	public Client() {
		// for construction via setters
	}

	/**
	 * Construct a new client model.
	 * 
	 * @param id
	 *            the id.
	 * @param firstName
	 *            the first name.
	 * @param lastName
	 *            the last name.
	 * @param dob
	 *            the date of birth.
	 * @param address
	 *            the address.
	 * @param phone
	 *            the phone number.
	 * @param email
	 *            the email address.
	 * @param authToken
	 *            the auth token.
	 * @param userId
	 *            the carers users id.
	 */
	public Client(int id, String firstName, String lastName, Date dob,
			String address, String phone, String email, String authToken,
			int userId) {
		super(id, id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = dob;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.authToken = authToken;
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public String getDobFormattedString() {
		return dateFormat.format(dob);
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

}
