package org.icare.mobile.activity.fragment;

import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.icare.mobile.R;
import org.icare.mobile.model.Resource;
import org.icare.mobile.model.Reminder;


/**
 * This is the detail fragment implementation for a reminder.
 *
 * @author John Meikle
 */
public class ReminderDetailFragment extends ResourceDetailFragment {

    private Reminder reminder;
    private TextView title;
    private TextView description;
    private TextView startDate;
    private TextView address;
    private TextView frequencyType;
    private TextView frequencyValue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reminder = new Reminder();
        resource = reminder;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reminder_detail, container, false);
        title = (TextView) rootView.findViewById(R.id.reminder_detail_title);
        description = (TextView) rootView.findViewById(R.id.reminder_detail_description);
        startDate = (TextView) rootView.findViewById(R.id.reminder_detail_start_date);
        address = (TextView) rootView.findViewById(R.id.reminder_detail_address);
        frequencyType = (TextView) rootView.findViewById(R.id.reminder_detail_frequency_type);
        frequencyValue = (TextView) rootView.findViewById(R.id.reminder_detail_frequency_value);
        return rootView;
    }

    @Override
    public void loadResource(Resource resource) {
        if (resource != null && resource instanceof Reminder) {
            reminder = (Reminder) resource;
            title.setText(reminder.getTitle());
            description.setText(reminder.getDescription());
            startDate.setText(reminder.getStartDateTime());
            address.setText(reminder.getLocation());
            Linkify.addLinks(address, Linkify.MAP_ADDRESSES);
            frequencyType.setText(reminder.getFrequencyType());
            frequencyValue.setText(Integer.toString(reminder.getFrequencyValue()));
        }
    }

}
