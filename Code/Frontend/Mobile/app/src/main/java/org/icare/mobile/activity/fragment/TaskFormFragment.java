package org.icare.mobile.activity.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.icare.mobile.R;
import org.icare.mobile.model.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This is the form fragment implementation for a task.
 *
 * @author John Meikle
 */
public class TaskFormFragment extends ResourceFormFragment implements DatePickerFragment.Callbacks, TimePickerFragment.Callbacks {

    private Task task;

    private EditText title;
    private EditText description;
    private TextView startDate;
    private TextView startTime;
    private TextView endDate;
    private TextView endTime;
    private EditText address;

    private int startYear;
    private int startMonth;
    private int startDay;
    private int startHour;
    private int startMinute;

    private int endYear;
    private int endMonth;
    private int endDay;
    private int endHour;
    private int endMinute;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (resource != null && resource instanceof Task) {
            task = (Task) this.resource;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_form, container, false);

        title = (EditText) rootView.findViewById(R.id.task_detail_title);
        description = (EditText) rootView.findViewById(R.id.task_detail_description);
        startDate = (TextView) rootView.findViewById(R.id.task_detail_start_date);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new DatePickerFragment();
                if (startYear > 0 && startMonth > 0 && startDay > 0) {
                    Bundle args = new Bundle();
                    args.putInt("YEAR", startYear);
                    args.putInt("MONTH", startMonth);
                    args.putInt("DAY", startDay);
                    dateFragment.setArguments(args);
                }
                dateFragment.setTargetFragment(TaskFormFragment.this, 0); // 0 = start date
                dateFragment.show(getFragmentManager(), "DatePicker");
            }
        });
        startTime = (TextView) rootView.findViewById(R.id.task_detail_start_time);
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new TimePickerFragment();
                if (startHour > 0 && startMinute > 0) {
                    Bundle args = new Bundle();
                    args.putInt("HOUR", startHour);
                    args.putInt("MINUTE", startMinute);
                    dateFragment.setArguments(args);
                }
                dateFragment.setTargetFragment(TaskFormFragment.this, 0); // 0 = start time
                dateFragment.show(getFragmentManager(), "TimePicker");
            }
        });
        endDate = (TextView) rootView.findViewById(R.id.task_detail_end_date);
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new DatePickerFragment();
                if (endYear > 0 && endMonth > 0 && endDay > 0) {
                    Bundle args = new Bundle();
                    args.putInt("YEAR", endYear);
                    args.putInt("MONTH", endMonth);
                    args.putInt("DAY", endDay);
                    dateFragment.setArguments(args);
                }
                dateFragment.setTargetFragment(TaskFormFragment.this, 1); // 1 = end date
                dateFragment.show(getFragmentManager(), "DatePicker");
            }
        });
        endTime = (TextView) rootView.findViewById(R.id.task_detail_end_time);
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new TimePickerFragment();
                if (endHour > 0 && endMinute > 0) {
                    Bundle args = new Bundle();
                    args.putInt("HOUR", endHour);
                    args.putInt("MINUTE", endMinute);
                    dateFragment.setArguments(args);
                }
                dateFragment.setTargetFragment(TaskFormFragment.this, 1); // 1 = end time
                dateFragment.show(getFragmentManager(), "TimePicker");
            }
        });
        address = (EditText) rootView.findViewById(R.id.task_detail_address);
        ((Button) rootView.findViewById(R.id.resource_form_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateResource();
                callbacks.onButtonSelected(task);
            }
        });

        if (task == null) {
            task = new Task();
            resource = task;
        } else {
            title.setText(task.getTitle());
            description.setText(task.getDescription());
            String[] dateTime = task.getStartDateTime().split(" ");
            startDate.setText(dateTime[0]);
            startTime.setText(dateTime[1]);
            dateTime = task.getEndDateTime().split(" ");
            endDate.setText(dateTime[0]);
            endTime.setText(dateTime[1]);
            address.setText(task.getLocation());
        }

        return rootView;
    }

    public void updateResource() {
        task.setTitle(title.getText().toString());
        task.setDescription(description.getText().toString());

        if (startDay > 0 && startMonth > 0 && startYear > 0 && startHour > 0 && startMinute > 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, startYear);
            calendar.set(Calendar.MONTH, startMonth);
            calendar.set(Calendar.DAY_OF_MONTH, startDay);
            calendar.set(Calendar.HOUR_OF_DAY, startHour);
            calendar.set(Calendar.MINUTE, startMinute);

            task.setStartDate(calendar.getTime());
        }
        if (endDay > 0 && endMonth > 0 && endYear > 0 && endHour > 0 && endMinute > 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, endYear);
            calendar.set(Calendar.MONTH, endMonth);
            calendar.set(Calendar.DAY_OF_MONTH, endDay);
            calendar.set(Calendar.HOUR_OF_DAY, endHour);
            calendar.set(Calendar.MINUTE, endMinute);

            task.setEndDate(calendar.getTime());
        }

        task.setLocation(address.getText().toString());
    }

    @Override
    public void onDateEntered(int requestCode, int year, int month, int day) {
        if (requestCode == 0) {
            // start date
            startYear = year;
            startMonth = month;
            startDay = day;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, startYear);
            calendar.set(Calendar.MONTH, startMonth);
            calendar.set(Calendar.DAY_OF_MONTH, startDay);
            Date date = calendar.getTime();

            startDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        } else {
            // end date
            endYear = year;
            endMonth = month;
            endDay = day;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, endYear);
            calendar.set(Calendar.MONTH, endMonth);
            calendar.set(Calendar.DAY_OF_MONTH, endDay);
            Date date = calendar.getTime();

            endDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        }
    }

    @Override
    public void onTimeEntered(int requestCode, int hour, int minute) {
        if (requestCode == 0) {
            // start date
            startHour = hour;
            startMinute = minute;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, startHour);
            calendar.set(Calendar.MINUTE, startMinute);
            Date date = calendar.getTime();

            startTime.setText(new SimpleDateFormat("HH:mm").format(date));

        } else {
            // end date
            endHour = hour;
            endMinute = minute;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, endHour);
            calendar.set(Calendar.MINUTE, endMinute);
            Date date = calendar.getTime();

            endTime.setText(new SimpleDateFormat("HH:mm").format(date));
        }
    }

}
