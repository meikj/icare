package org.icare.mobile.loader;

import android.content.Context;
import android.content.AsyncTaskLoader;

import org.icare.mobile.model.Alert;
import org.icare.mobile.model.Contact;
import org.icare.mobile.model.Reminder;
import org.icare.mobile.model.Resource;
import org.icare.mobile.model.Task;
import org.icare.mobile.model.Track;
import org.icare.mobile.rest.RestAPI;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

/**
 * This is a content loader for resources. This loads the list of all resources according to
 * resource type.
 *
 * @author John Meikle
 */
public class ResourceLoader extends AsyncTaskLoader<List<? extends Resource>> {

    private List<? extends Resource> resources;
    private RestAPI rest;
    private ResourceType type;

    /**
     * Construct a new ResourceLoader.
     *
     * @param context the context.
     * @param rest the REST API object.
     * @param type the resource type.
     */
    public ResourceLoader(Context context, RestAPI rest, ResourceType type) {
        super(context);
        this.rest = rest;
        this.type = type;
    }

    @Override
    public List<? extends Resource> loadInBackground() {
        String urlResource = ResourceType.getUrlResource(type);
        ParameterizedTypeReference responseType;
        switch (type) {
            case ALERTS:
                responseType = new ParameterizedTypeReference<List<Alert>>() { };
                break;
            case CONTACTS:
                responseType = new ParameterizedTypeReference<List<Contact>>() { };
                break;
            case TASKS:
                responseType = new ParameterizedTypeReference<List<Task>>() { };
                break;
            case REMINDERS:
                responseType = new ParameterizedTypeReference<List<Reminder>>() { };
                break;
            case TRACKS:
                responseType = new ParameterizedTypeReference<List<Track>>() { };
                break;
            default:
                return null;
        }
        return rest.getAll(urlResource, responseType);
    }

    @Override
    public void deliverResult(List<? extends Resource> result) {
        resources = result;

        if (isStarted()) {
            super.deliverResult(result);
        }
    }

    @Override
    protected void onStartLoading() {
        // force a refresh
        forceLoad();
    }

}
