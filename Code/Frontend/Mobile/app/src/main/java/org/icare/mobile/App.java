package org.icare.mobile;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.icare.mobile.activity.AuthActivity;
import org.icare.mobile.config.AppConfig;
import org.icare.mobile.rest.RestAPI;
import org.icare.mobile.rest.RestAPIImpl;
import org.icare.mobile.service.TrackingService;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * Application main class. This essentially provides a general application context with methods for
 * accessing an instance of the REST API and application config.
 *
 * @author John Meikle
 */
public class App extends Application {

    /**
     * The name of the JSON-formatted (see AppConfig) application configuration file.
     */
    public static final String APP_CONFIG_FILE_NAME = "icare_config.json";

    /**
     * The application REST API instance.
     */
    private RestAPI restApi;

    /**
     * The application configuration.
     */
    private AppConfig appConfig;

    /**
     * The tracking service Intent.
     */
    private Intent trackServiceIntent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Open application config file from internal storage
        AppConfig config = openAppConfig(APP_CONFIG_FILE_NAME);
        if (config != null) {
            // Load parsed config
            appConfig = config;
            Log.i("App", String.format(
                    "Loaded AppConfig (%s): %s", APP_CONFIG_FILE_NAME, appConfig.getKeyValue()));
        } else {
            // Otherwise, load defaults
            appConfig = new AppConfig();
            Log.i("App", "Loaded default AppConfig: " + appConfig.getKeyValue());
        }

        // Initialise the RestAPI instance
        initRestApi();

        // Start the tracking service
        trackServiceIntent = new Intent(this, TrackingService.class);
        startService(trackServiceIntent);
    }

    /**
     * Get the application config. Provides methods for getting config values.
     */
    public AppConfig getAppConfig() {
        return appConfig;
    }

    /**
     * Get the application REST API instance.
     */
    public RestAPI getRest() {
        return restApi;
    }

    /**
     * Save the AppConfig to internal storage with the file name as specified by APP_CONFIG_FILE_NAME.
     */
    public void saveAppConfig() {
        // Open file
        File file = new File(getFilesDir(), APP_CONFIG_FILE_NAME);
        ObjectMapper mapper = new ObjectMapper();
        Map configKeyValue = (Map) appConfig.getKeyValue();
        try {
            FileOutputStream fo = new FileOutputStream(file, false);
            mapper.writeValue(fo, configKeyValue);
            fo.close();
            Log.i("App", String.format("Saved AppConfig (%s): %s", file.getPath(), appConfig.getKeyValue()));
        } catch(Exception e) {
            Log.e("App", String.format("Error writing AppConfig to file (%s): %s", file.getPath(), e.getMessage()));
        }
    }

    /**
     * Load an AppConfig. This will compare configuration values from the current AppConfig to the
     * specific newConfig so as to detect any changes. This separately detects the REST API needing
     * reinitialisation and the tracking service needing reinitialisation.
     *
     * @param newConfig the new AppConfig.
     */
    public void loadAppConfig(AppConfig newConfig) {
        if (newConfig == null)
            throw new IllegalArgumentException("Cannot load a null AppConfig.");

        boolean reinitRest = false;
        boolean reinitTracking = false;

        if (!appConfig.getRestUrl().equals(newConfig.getRestUrl()))
            reinitRest = true;
        if (appConfig.getRestPort() != newConfig.getRestPort())
            reinitRest = true;
        if (!appConfig.getAuthToken().equals(newConfig.getAuthToken()))
            reinitRest = true;
        if (appConfig.isTrackEnabled() != newConfig.isTrackEnabled())
            reinitTracking = true;
        if (appConfig.getTrackInterval() != newConfig.getTrackInterval())
            reinitTracking = true;
        if (appConfig.getTrackPriority() != newConfig.getTrackPriority())
            reinitTracking = true;

        // Update app config reference now that they've been compared
        appConfig = newConfig;

        if (reinitRest) {
            // Reinitialise the REST API and reauthenticate
            initRestApi();
            Intent authIntent = new Intent(this, AuthActivity.class);
            authIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(authIntent);
        }
        if (reinitTracking) {
            // Reinitialise the tracking service
            restartTrackingService();
        }
    }

    /**
     * Restart the tracking service.
     */
    public void restartTrackingService() {
        Log.i("App", "Restarting tracking service.");

        stopService(trackServiceIntent);
        startService(trackServiceIntent);
    }

    /**
     * Open the the specified file name and attempt to parse.
     *
     * @param fileName the file name.
     * @return the resulting parsed new AppConfig, or null if file is invalid.
     */
    private AppConfig openAppConfig(String fileName) {
        // Ensure the file exists first
        File file = new File(getFilesDir(), fileName);
        if (!file.exists())
            return null;

        // Read the file and deserialise its JSON content into a Map
        ObjectMapper mapper = new ObjectMapper();
        Map result;

        try {
            result = mapper.readValue(file, Map.class);
            return new AppConfig(result);
        } catch (Exception e){
            // File is not valid JSON
            Log.e("App", String.format("Open AppConfig failed (%s): %s", file.getPath(), e.getMessage()));
        }

        return null;
    }

    /**
     * Initialise the REST API.
     */
    private void initRestApi() {
        Log.i("App", "Initialising REST API.");

        if (appConfig.getRestUrl() == null || appConfig.getRestUrl().isEmpty())
            // reset appConfig to defaults since config seems to be missing URL
            appConfig.loadDefaults();

        // Connect
        if (appConfig.getRestPort() > 0)
            restApi = new RestAPIImpl(appConfig.getRestUrl(), appConfig.getRestPort());
        else
            restApi = new RestAPIImpl(appConfig.getRestUrl());
    }

}
