package org.icare.mobile.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.icare.mobile.App;
import org.icare.mobile.R;
import org.icare.mobile.activity.fragment.ResourceDetailFragment;
import org.icare.mobile.activity.task.ResourceDeleteTask;
import org.icare.mobile.activity.task.ResourceUpdateTask;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;

/**
 * This activity acts as a "shell" for a ResourceDetailFragment. The type of resource presented is
 * determined by the ResourceType.RESOURCE_TYPE argument. The ID of the resource to fetch can be
 * passed by the ResourceType.RESOURCE_ID argument. The title of this activity can be passed
 * through by the TITLE argument.
 *
 * @author John Meikle
 */
public class ResourceDetailActivity extends ActionBarActivity
        implements ResourceDeleteTask.Callbacks, ResourceUpdateTask.Callbacks {

    /**
     * The Intent key for the activity title string.
     */
    public static final String TITLE = "RESOURCE_DETAIL_TITLE";

    /**
     * The resource type for the activity.
     */
    private ResourceType resourceType;

    /**
     * The resource for the activity.
     */
    private Resource resource;

    /**
     * The update/refresh progress dialog.
     */
    private ProgressDialog progressDialog;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu items for use in action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_resource_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_detail);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // This sets resource type, resource, and title
            restoreFromArgs(getIntent().getExtras());

            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            createFragment(bundleArgs(getIntent().getExtras()));
        } else {
            restoreFromArgs(savedInstanceState);
        }

        // Set up progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Updating...");
        progressDialog.setCancelable(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            finish();
            return true;
        } else if (id == R.id.action_edit) {
            // Edit resource
            startEdit();
            return true;
        } else if (id == R.id.action_delete) {
            // Delete resource
            startDelete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                progressDialog.show();
                resource = (Resource) data.getSerializableExtra(ResourceType.RESOURCE_OBJECT);
                new ResourceUpdateTask(((App) getApplicationContext()).getRest(), resourceType, this).execute(resource);
            }
        }
    }

    @Override
    public void onDeleteSuccess(Resource resource) {
        Toast.makeText(ResourceDetailActivity.this, "Deletion successful", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onDeleteError(Resource resource, String message) {
        Toast.makeText(ResourceDetailActivity.this, "Deletion failed\n(" + message + ")", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateSuccess(Resource resource) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        this.resource = resource;
        createFragment(bundleArgs(getIntent().getExtras())); // refresh
        Toast.makeText(ResourceDetailActivity.this, "Update successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateError(Resource resource, String message) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        Toast.makeText(ResourceDetailActivity.this, "Update failed\n(" + message + ")", Toast.LENGTH_SHORT).show();
    }

    /**
     * Start the ResourceDeleteTask for the current resource. This is called in the event
     * of the toolbar 'Delete' button being pressed.
     */
    public void startDelete() {
        new ResourceDeleteTask(((App) getApplicationContext()).getRest(), resourceType, this).execute(resource);
    }

    /**
     * Start the ResourceFormActivity for editing the current resource. This is called in the event
     * of the toolbar 'Edit' button being pressed.
     */
    public void startEdit() {
        Intent intent = new Intent(this, ResourceFormActivity.class);
        Bundle args = bundleArgs(getIntent().getExtras());
        args.putString(ResourceFormActivity.TITLE, "Edit");
        intent.putExtras(args);
        startActivityForResult(intent, 0);
    }

    /**
     * Create the ResourceFormFragment with the specified bundled arguments. This deletes any
     * existing fragment in the container.
     *
     * @param args the bundled arguments.
     */
    private void createFragment(Bundle args) {
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.resource_detail_container);
        if (currentFragment != null)
            getFragmentManager().beginTransaction().remove(currentFragment).commit();

        ResourceDetailFragment newFragment = ResourceDetailFragment.newInstance(resourceType);
        newFragment.setArguments(args);
        getFragmentManager().beginTransaction()
                .add(R.id.resource_detail_container, newFragment)
                .commit();
    }

    /**
     * Restore this activity from the specified bundled arguments. There is recognition for
     * resource type, resource object, and title.
     *
     * @param args the bundled arguments.
     */
    private void restoreFromArgs(Bundle args) {
        // Get resource type
        if (args.getSerializable(ResourceType.RESOURCE_TYPE) != null) {
            resourceType = (ResourceType) getIntent().getSerializableExtra(ResourceType.RESOURCE_TYPE);
            Log.d("ResourceDetailActivity", "resourceType set to " + resourceType.name());
        }

        // Get resource
        if (args.getSerializable(ResourceType.RESOURCE_OBJECT) != null) {
            resource = (Resource) getIntent().getSerializableExtra(ResourceType.RESOURCE_OBJECT);
            Log.d("ResourceDetailActivity", "resource set to \"" + resource + "\".");
        }

        // Get title
        if (args.getString(TITLE) != null) {
            setTitle(args.getString(TITLE));
        }
    }

    /**
     * Bundle this activities arguments. This ensures the resource object is up to date.
     *
     * @param args the bundled arguments.
     * @return the up to date bundled arguments.
     */
    private Bundle bundleArgs(Bundle args) {
        Bundle thisArgs = new Bundle(args);

        if (resource != null) {
            thisArgs.putSerializable(ResourceType.RESOURCE_OBJECT, resource);
            thisArgs.putSerializable(ResourceType.RESOURCE_ID, resource.getId());
        }

        return thisArgs;
    }

}
