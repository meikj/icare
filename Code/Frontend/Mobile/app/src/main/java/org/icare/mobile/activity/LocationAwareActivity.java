package org.icare.mobile.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;

import org.icare.mobile.service.TrackingService;

/**
 * This is an abstract implementation of a location aware activity. This provides concrete
 * implementations with access to the latest current location coordinate string that is reported by
 * the TrackingService. It does this by acting as a BroadcastReceiver.
 *
 * @author John Meikle
 */
public abstract class LocationAwareActivity extends ActionBarActivity {

    protected String currentLocationCoords;

    /**
     * Get the current location coordinate string.
     *
     * @return the current location.
     */
    public String getCurrentLocationCoords() {
        return currentLocationCoords;
    }

    @Override
    public void onStop() {
        super.onStop();

        // Unregister this activity as broadcast receiver, it's (re)registered onStart()
        unregisterReceiver(coordsReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Register this activity as a broadcast receiver for TrackingService location updates
        registerReceiver(coordsReceiver, new IntentFilter(TrackingService.BROADCAST_LOCATION_ACTION));
    }

    /**
     * Update the current location of this activity.
     *
     * @param coords the new current location coordinates.
     */
    protected void onUpdateCurrentLocation(String coords) {
        this.currentLocationCoords = coords;
    }

    /**
     * The broadcast receiver for coordinate location updating.
     */
    private BroadcastReceiver coordsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(TrackingService.CURRENT_LOCATION)) {
                String coords = intent.getStringExtra(TrackingService.CURRENT_LOCATION);
                onUpdateCurrentLocation(coords);
            }
        }
    };

}
