package org.icare.mobile.activity.task;

import android.os.AsyncTask;
import android.util.Log;

import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;
import org.icare.mobile.rest.RestAPI;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestClientException;

/**
 * This task adds a resource to the backend REST API. There is callback functionality for operation
 * success and failure.
 *
 * @author John Meikle
 */
public class ResourceAddTask extends AsyncTask<Resource, Void, Boolean> {

    private RestAPI rest;
    private ResourceType resourceType;
    private Callbacks callbacks;
    private Resource resource;
    private String errorMessage;

    /**
     * Construct a new ResourceAddTask.
     *
     * @param rest the REST API object.
     * @param resourceType the resource type.
     * @param callbacks the class to callback to.
     */
    public ResourceAddTask(RestAPI rest, ResourceType resourceType, Callbacks callbacks) {
        super();
        this.rest = rest;
        this.resourceType = resourceType;
        this.callbacks = callbacks;
    }

    @Override
    protected Boolean doInBackground(Resource... params) {
        resource = params[0];
        ParameterizedTypeReference responseType = ResourceType.getResponseType(resourceType);
        String urlResource = ResourceType.getUrlResource(resourceType);

        try {
            resource = rest.add(urlResource, responseType, resource);
        } catch (RestClientException e) {
            Log.e("ResourceDeleteTask",
                    "Error adding resource (" + e.getMessage() + ").");
            errorMessage = e.getMessage();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean response) {
        if (response) {
            // All is good
            callbacks.onAddSuccess(resource);
        } else {
            // Something went wrong
            callbacks.onAddError(resource, errorMessage);
        }
    }

    /**
     * A callback interface for reporting add success and failure.
     */
    public interface Callbacks {

        /**
         * Called upon a successful add.
         *
         * @param resource the resulting resource.
         */
        public void onAddSuccess(Resource resource);

        /**
         * Called upon an unsuccessful add.
         *
         * @param resource the original resource.
         * @param message an error message.
         */
        public void onAddError(Resource resource, String message);

    }

}
