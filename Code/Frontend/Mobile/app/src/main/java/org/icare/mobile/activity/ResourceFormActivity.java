package org.icare.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;

import org.icare.mobile.R;
import org.icare.mobile.activity.fragment.ResourceFormFragment;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;

/**
 * This activity acts as a "shell" for a ResourceFormFragment. The type of resource presented is
 * determined by the ResourceType.RESOURCE_TYPE argument. Optionally (e.g. for editing), a resource
 * object can be passed by the ResourceType.RESOURCE_OBJECT argument. The title of this activity
 * can be passed through by the TITLE argument.
 *
 * @author John Meikle
 */
public class ResourceFormActivity extends ActionBarActivity
        implements ResourceFormFragment.Callbacks {

    /**
     * The Bundle key for the activity title string.
     */
    public static final String TITLE = "RESOURCE_FORM_TITLE";

    /**
     * The resource type for the activity.
     */
    private ResourceType resourceType;

    /**
     * The resource for the activity.
     */
    private Resource resource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_form);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // This sets resource type and resource
            restoreFromArgs(getIntent().getExtras());

            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            createFragment(bundleArgs(getIntent().getExtras()));
        } else {
            restoreFromArgs(savedInstanceState);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            Intent intent = new Intent(this, ResourceListActivity.class);
            intent.putExtras(bundleArgs(getIntent().getExtras()));
            navigateUpTo(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onButtonSelected(Resource resource) {
        // Finish the activity and return result
        Intent returnIntent = new Intent();
        returnIntent.putExtra(ResourceType.RESOURCE_OBJECT, resource);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    /**
     * Create the ResourceFormFragment with the specified bundled arguments. This deletes any
     * existing fragment in the container.
     *
     * @param args the bundled arguments.
     */
    private void createFragment(Bundle args) {
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.resource_form_container);
        if (currentFragment != null)
            getFragmentManager().beginTransaction().remove(currentFragment).commit();

        ResourceFormFragment newFragment = ResourceFormFragment.newInstance(resourceType);
        newFragment.setArguments(args);
        getFragmentManager().beginTransaction()
                .add(R.id.resource_form_container, newFragment)
                .commit();
    }

    /**
     * Restore this activity from the specified bundled arguments. There is recognition for
     * resource type, resource object, and title.
     *
     * @param args the bundled arguments.
     */
    private void restoreFromArgs(Bundle args) {
        // Get resource type
        if (args.getSerializable(ResourceType.RESOURCE_TYPE) != null) {
            resourceType = (ResourceType) getIntent().getSerializableExtra(ResourceType.RESOURCE_TYPE);
            Log.d("ResourceFormActivity", "resourceType set to " + resourceType.name());
        }

        // Get resource
        if (args.getSerializable(ResourceType.RESOURCE_OBJECT) != null) {
            resource = (Resource) getIntent().getSerializableExtra(ResourceType.RESOURCE_OBJECT);
            Log.d("ResourceFormActivity", "resource set to \"" + resource + "\".");
        }

        // Get title
        if (args.getString(TITLE) != null) {
            setTitle(args.getString(TITLE));
        }
    }

    /**
     * Bundle this activities arguments. This ensures the resource object is up to date.
     *
     * @param args the bundled arguments.
     * @return the up to date bundled arguments.
     */
    private Bundle bundleArgs(Bundle args) {
        Bundle thisArgs = new Bundle(args);

        if (resource != null)
            thisArgs.putSerializable(ResourceType.RESOURCE_OBJECT, resource);

        return thisArgs;
    }

}
