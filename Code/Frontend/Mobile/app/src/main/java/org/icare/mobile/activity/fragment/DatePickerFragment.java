package org.icare.mobile.activity.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * This represents a Date Picker dialog. There is a Callback interface to notify when a date has
 * been entered.
 *
 * @author John Meikle
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private Callbacks callbacks;
    private int year;
    private int month;
    private int day;

    public interface Callbacks {

        public void onDateEntered(int requestCode, int year, int month, int day);
    }

    private static Callbacks dummyCallbacks = new Callbacks() {
        @Override
        public void onDateEntered(int requestCode, int year, int month, int day) { }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getTargetFragment() instanceof Callbacks) {
            callbacks = (Callbacks) getTargetFragment();
        } else {
            callbacks = dummyCallbacks;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null && args.containsKey("YEAR") && args.containsKey("MONTH") && args.containsKey("DAY")) {
            year = args.getInt("YEAR");
            month = args.getInt("MONTH");
            day = args.getInt("DAY");
        } else {
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        callbacks.onDateEntered(getTargetRequestCode(), year, monthOfYear, dayOfMonth);
    }

}
