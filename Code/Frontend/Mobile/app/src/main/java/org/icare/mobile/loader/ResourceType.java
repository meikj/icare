package org.icare.mobile.loader;

import org.icare.mobile.R;
import org.icare.mobile.model.Alert;
import org.icare.mobile.model.Contact;
import org.icare.mobile.model.Reminder;
import org.icare.mobile.model.Task;
import org.icare.mobile.model.Track;
import org.icare.mobile.rest.RestAPIImpl;
import org.springframework.core.ParameterizedTypeReference;

/**
 * This represents a resource type in the application, with helper methods for getting a URL string
 * or response type from a ResourceType.
 *
 * @author John Meikle
*/
public enum ResourceType {

    ALERTS, CONTACTS, TASKS, REMINDERS, TRACKS;

    /**
     * The Bundle key for resource type.
     */
    public static final String RESOURCE_TYPE = "RESOURCE_TYPE";

    /**
     * The Bundle key for resource object.
     */
    public static final String RESOURCE_OBJECT = "RESOURCE_OBJECT";

    /**
     * The Bundle key for resource ID.
     */
    public static final String RESOURCE_ID = "RESOURCE_ID";

    /**
     * Get the URL resource from a resource type for the backend iCare REST API (RestAPIImpl).
     *
     * @param resourceType the resource type.
     * @return the URL resource.
     */
    public static String getUrlResource(ResourceType resourceType) {
        switch (resourceType) {
            case ALERTS:
                return  RestAPIImpl.REST_URL_ALERTS;
            case CONTACTS:
                return RestAPIImpl.REST_URL_CONTACTS;
            case TASKS:
                return RestAPIImpl.REST_URL_TASKS;
            case REMINDERS:
                return RestAPIImpl.REST_URL_REMINDERS;
            case TRACKS:
                return RestAPIImpl.REST_URL_TRACKS;
        }
        return null;
    }

    public static int getToolbarIcon(ResourceType resourceType) {
        switch (resourceType) {
            case CONTACTS:
                return R.mipmap.ic_face_unlock_grey600_24dp;
            case TASKS:
                return R.mipmap.ic_assignment_grey600_24dp;
            case REMINDERS:
                return R.mipmap.ic_alarm_grey600_24dp;
            case TRACKS:
                return R.mipmap.ic_explore_grey600_24dp;
        }
        return -1;
    }

    /**
     * Get the response type from a resource type for the backend iCare REST API (RestAPIImpl).
     *
     * @param resourceType the resource type.
     * @return the type reference.
     */
    public static ParameterizedTypeReference getResponseType(ResourceType resourceType) {
        switch(resourceType) {
            case ALERTS:
                return new ParameterizedTypeReference<Alert>() { };
            case CONTACTS:
                return new ParameterizedTypeReference<Contact>() { };
            case TASKS:
                return new ParameterizedTypeReference<Task>() { };
            case REMINDERS:
                return new ParameterizedTypeReference<Reminder>() { };
            case TRACKS:
                return new ParameterizedTypeReference<Track>() { };
            default:
                return null;
        }
    }

}
