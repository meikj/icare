package org.icare.mobile.activity.task;

import android.os.AsyncTask;
import android.util.Log;

import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;
import org.icare.mobile.rest.RestAPI;
import org.springframework.web.client.RestClientException;

/**
 * This task deletes a resource from the backend REST API. There is callback functionality for operation
 * success and failure.
 *
 * @author John Meikle
 */
public class ResourceDeleteTask extends AsyncTask<Resource, Void, Boolean> {

    private RestAPI rest;
    private ResourceType resourceType;
    private Callbacks callbacks;
    private Resource resource;
    private String errorMessage;

    /**
     * Construct a new ResourceDeleteTask.
     *
     * @param rest the REST API object.
     * @param resourceType the resource type.
     * @param callbacks the class to callback to.
     */
    public ResourceDeleteTask(RestAPI rest, ResourceType resourceType, Callbacks callbacks) {
        super();
        this.rest = rest;
        this.resourceType = resourceType;
        this.callbacks = callbacks;
    }

    @Override
    protected Boolean doInBackground(Resource... params) {
        resource = params[0];
        String urlResource = ResourceType.getUrlResource(resourceType);

        try {
            rest.delete(urlResource, resource.getId());
        } catch (RestClientException e) {
            Log.e("ResourceDeleteTask",
                    "Error deleting resource ID " + resource.getId() + " (" + e.getMessage() + ").");
            errorMessage = e.getMessage();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean response) {
        if (response) {
            // All is good
            callbacks.onDeleteSuccess(resource);
        } else {
            // Something went wrong
            callbacks.onDeleteError(resource, errorMessage);
        }
    }

    /**
     * A callback interface for reporting delete success and failure.
     */
    public interface Callbacks {

        /**
         * Called upon a successful delete.
         *
         * @param resource the resulting resource.
         */
        public void onDeleteSuccess(Resource resource);

        /**
         * Called upon an unsuccessful delete.
         *
         * @param resource the original resource.
         * @param message an error message.
         */
        public void onDeleteError(Resource resource, String message);

    }

}
