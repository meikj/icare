package org.icare.mobile.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;

import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;

/**
 * This is an abstract implementation for a resource form fragment. This provides a callback
 * interface so that an activity can be notified of the submit button press. The form can be
 * populated through the ResourceType.RESOURCE_OBJECT argument.
 *
 * @author John Meikle
 */
public abstract class ResourceFormFragment extends Fragment {

    /**
     * The resource object.
     */
    protected Resource resource;

    /**
     * The callbacks for submit button selection.
     */
    protected Callbacks callbacks;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when the submit button has been selected.
         */
        public void onButtonSelected(Resource resource);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onButtonSelected(Resource resource) { }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ResourceFormFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        callbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callbacks = sDummyCallbacks;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ResourceType.RESOURCE_OBJECT)) {
            resource = (Resource) getArguments().get(ResourceType.RESOURCE_OBJECT);
        }
    }

    /**
     * A factory method for constructing a concrete ResourceFormFragment depending on the resource
     * type.
     *
     * @param resourceType the resource type.
     * @return the newly constructed ResourceFormFragment.
     * @throws IllegalArgumentException if the resource type is null.
     */
    public static ResourceFormFragment newInstance(ResourceType resourceType) throws IllegalArgumentException {
        if (resourceType == null)
            throw new IllegalArgumentException("resourceType cannot be null.");

        Bundle args = new Bundle();
        ResourceFormFragment fragment;

        switch (resourceType) {
            case CONTACTS:
                fragment = new ContactFormFragment();
                break;
            case TASKS:
                fragment = new TaskFormFragment();
                break;
            case REMINDERS:
                fragment = new ReminderFormFragment();
                break;
            default:
                return null;
        }

        args.putSerializable(ResourceType.RESOURCE_TYPE, resourceType);
        fragment.setArguments(args);
        Log.d("ResourceFormFragment", "New instance (" + resourceType.name() + ").");
        return fragment;
    }

}
