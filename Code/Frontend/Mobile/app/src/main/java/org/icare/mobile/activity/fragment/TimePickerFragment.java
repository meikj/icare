package org.icare.mobile.activity.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * This represents a Time Picker dialog. There is a Callback interface to notify when a time has
 * been entered.
 *
 * @author John Meikle
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private Callbacks callbacks;

    private int hour;
    private int minute;

    public interface Callbacks {

        public void onTimeEntered(int requestCode, int hour, int minute);
    }

    private static Callbacks dummyCallbacks = new Callbacks() {
        @Override
        public void onTimeEntered(int requestCode, int hour, int minute) { }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getTargetFragment() instanceof Callbacks) {
            callbacks = (Callbacks) getTargetFragment();
        } else {
            callbacks = dummyCallbacks;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null && args.containsKey("HOUR") && args.containsKey("MINUTE")) {
            hour = args.getInt("HOUR");
            minute = args.getInt("MINUTE");
        } else {
            Calendar calendar = Calendar.getInstance();
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
        }
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        callbacks.onTimeEntered(getTargetRequestCode(), hourOfDay, minute);
    }

}
