package org.icare.mobile.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.icare.mobile.App;
import org.icare.mobile.activity.task.SendTrackTask;
import org.icare.mobile.config.AppConfig;
import org.icare.mobile.model.Resource;
import org.icare.mobile.rest.RestAPI;


/**
 * The location tracking service for updating the iCare backend with location updates. This uses the
 * Google API client and iCare REST API. There is support for broadcasting location updates to
 * BroadcastReceiver's using the BROADCAST_LOCATION_ACTION action string. The location update can
 * be accessed using the CURRENT_LOCATION Intent/Bundle key.
 *
 * Even in the event that tracking.enabled is false, the TrackingService still updates BroadcastReceivers
 * with location updates for functionality like alerts. Its just that location updates are not
 * sent to the backend server in this event.
 *
 * @author John Meikle
 */
public class TrackingService extends IntentService
        implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener, SendTrackTask.Callbacks {

    /**
     * The service name.
     */
    public static final String SERVICE_NAME = "org.icare.mobile.service.TRACKING_SERVICE";

    /**
     * The Intent action string for broadcasting current location.
     */
    public static final String BROADCAST_LOCATION_ACTION = "org.icare.mobile.CURRENT_LOCATION";

    /**
     * The Bundle key for current location.
     */
    public static final String CURRENT_LOCATION = "CURRENT_LOCATION";

    private GoogleApiClient api;
    private LocationRequest locationRequest;
    private Location currentLocation;
    private boolean enabled;
    private int interval;
    private int priority;
    private Intent resultIntent;

    /**
     * Construct a new TrackingService using SERVICE_NAME as name.
     */
    public TrackingService() {
        super(SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        resultIntent = new Intent(BROADCAST_LOCATION_ACTION);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("TrackingService", "Google API client connected.");

        setLocationToLast();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("TrackingService", "Google Play Services API connection suspended.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("TrackingService", "Google Play Services API connection failed.");
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;

        // Pass location as coordinate string (Location is not serializable)
        resultIntent.putExtra(CURRENT_LOCATION, locationToCoords(currentLocation));

        // This will update broadcast receiver(s) with new current location
        sendBroadcast(resultIntent);

        // Send new location to backend
        sendLocation();
    }

    @Override
    public void onTrackSuccess(Resource resource) {
        Log.d("TrackingService", "Sending track was successful.");
    }

    @Override
    public void onTrackError(Resource resource, String message) {
        Log.e("TrackingService", "Sending track failed.");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        updateFromAppConfig();
        constructLocationRequest();
        buildAndStartGoogleApiClient();

        while (api.isConnecting() || api.isConnected()) {
            // keep service alive
            synchronized (this) {
                try {
                    wait(interval);
                } catch (Exception e) {
                    break;
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (api != null && api.isConnected()) {
            Log.d("TrackingService", "Removing location updates and disconnecting.");
            LocationServices.FusedLocationApi.removeLocationUpdates(api, this);
            api.disconnect();
        }
    }



    /**
     * Build and start the Google Play Services API client with Location Services and callback
     * functionality.
     */
    private synchronized void buildAndStartGoogleApiClient() {
        api = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        api.connect();
    }

    /**
     * Set the current location to the last recorded location.
     */
    private synchronized void setLocationToLast() {
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(api);

        if (lastLocation == null)
            Log.w("TrackingService", "Last location is null.");
        else
            onLocationChanged(lastLocation);
    }

    /**
     * Construct the Location Request for updating the service with latest location.
     */
    private synchronized void constructLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(interval);
        locationRequest.setFastestInterval(interval);
        locationRequest.setPriority(priority);
    }

    /**
     * Start requesting location updates from Google API.
     */
    private synchronized void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(api, locationRequest, this);
    }

    /**
     * Sends the current location to the backend iCare REST API. This is only done if enabled is
     * true.
     */
    private synchronized void sendLocation() {
        RestAPI rest = (RestAPI) ((App) getApplicationContext()).getRest();
        if (rest != null && rest.isAuthenticated() && enabled) {
            Log.i("TrackingService", "Sending current location to backend: " + currentLocation);
            new SendTrackTask(rest, this).execute(locationToCoords(currentLocation));
        }
    }

    /**
     * Update the tracking service config values from the application configuration.
     */
    private synchronized void updateFromAppConfig() {
        // Get app config then set location request values
        AppConfig config = ((App) getApplicationContext()).getAppConfig();
        enabled = config.isTrackEnabled();
        interval = config.getTrackInterval();
        priority = config.getTrackPriority();

        Log.d("TrackingService", "updateFromAppConfig(): " + config.getKeyValue());
    }

    /**
     * Convert a Location object to a lat-long coordinate string (e.g. "54.332,-4.874").
     *
     * @param location the Location object.
     * @return the coordinate string.
     */
    private String locationToCoords(Location location) {
        return String.format("%f,%f", location.getLatitude(), location.getLongitude());
    }

}
