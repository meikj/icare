package org.icare.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.icare.mobile.App;
import org.icare.mobile.R;
import org.icare.mobile.activity.fragment.ResourceDetailFragment;
import org.icare.mobile.activity.fragment.ResourceListFragment;
import org.icare.mobile.activity.task.ResourceAddTask;
import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Resource;

/**
 * This is the resource list activity for presenting the user with a list of resources which can be
 * pressed for navigation to a more detailed view. The type of resource presented is determined by
 * the ResourceType.RESOURCE_TYPE argument. This class uses a ResourceLoader to fetch up to date
 * content. The title of this activity can be passed through by the TITLE argument.
 *
 * @author John Meikle
 */
public class ResourceListActivity extends ActionBarActivity
        implements ResourceListFragment.Callbacks, ResourceAddTask.Callbacks {

    /**
     * The Bundle key for the activity title string.
     */
    public static final String TITLE = "RESOURCE_LIST_TITLE";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    /**
     * The resource type for this activity and the detail activity.
     */
    private ResourceType resourceType;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu items for use in action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_resource_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_list);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            // This sets resourceType and title
            restoreFromArgs(getIntent().getExtras());

            // Create new fragment
            ResourceListFragment fragment = ResourceListFragment.newInstance(resourceType);
            fragment.getArguments().putAll(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .add(R.id.resource_list, fragment)
                    .commit();
        } else {
            restoreFromArgs(savedInstanceState);
        }

        if (findViewById(R.id.resource_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((ResourceListFragment) getFragmentManager()
                    .findFragmentById(R.id.resource_list))
                    .setActivateOnItemClick(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpFromSameTask(this);
            return true;
        } else if (id == R.id.action_add) {
            startAddActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Callback method from {@link ResourceListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(Resource resource) {
        // Bundle arguments together
        Bundle args = new Bundle(getIntent().getExtras());
        args.putSerializable(ResourceType.RESOURCE_OBJECT, resource);

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            createFragment(args);
        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            startDetailActivity(args);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save all
        outState.putAll(getIntent().getExtras());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                Resource resource = (Resource) data.getSerializableExtra(ResourceType.RESOURCE_OBJECT);
                new ResourceAddTask(((App) getApplicationContext()).getRest(), resourceType, this).execute(resource);
            }
        }
    }

    @Override
    public void onAddSuccess(Resource resource) {
        Toast.makeText(ResourceListActivity.this, "Addition successful", Toast.LENGTH_SHORT).show();

        // Upon success we should transition to a more detailed view
        Bundle args = new Bundle(getIntent().getExtras());
        args.putSerializable(ResourceType.RESOURCE_OBJECT, resource);
        startDetailActivity(args);
    }

    @Override
    public void onAddError(Resource resource, String message) {
        Toast.makeText(ResourceListActivity.this, "Addition failed\n(" + message + ")", Toast.LENGTH_SHORT).show();
    }

    /**
     * Start the ResourceFormActivity for resource addition purposes. This is called in the event
     * of the toolbar 'Add' button being pressed.
     */
    public void startAddActivity() {
        Intent intent = new Intent(this, ResourceFormActivity.class);
        Bundle args = new Bundle(getIntent().getExtras());
        args.putString(ResourceFormActivity.TITLE, "Add");
        args.putSerializable(ResourceType.RESOURCE_OBJECT, null); // ensure fresh model is used
        intent.putExtras(args);
        startActivityForResult(intent, 0);
    }

    /**
     * Create the ResourceListFragment with the specified bundled arguments.
     *
     * @param args the bundled arguments.
     */
    private void createFragment(Bundle args) {
        ResourceDetailFragment fragment = ResourceDetailFragment.newInstance(resourceType);
        fragment.setArguments(args);
        getFragmentManager().beginTransaction()
                .replace(R.id.resource_detail_container, fragment)
                .commit();
    }

    /**
     * Start the ResourceDetailActivity with the specified bundled arguments.
     *
     * @param args the bundled arguments.
     */
    private void startDetailActivity(Bundle args) {
        Intent detailIntent = new Intent(this, ResourceDetailActivity.class);
        detailIntent.putExtras(args);
        startActivity(detailIntent);
    }

    /**
     * Restore this activity from the specified bundled arguments. There is recognition for
     * resource type and title.
     *
     * @param args the bundled arguments.
     */
    private void restoreFromArgs(Bundle args) {
        // Get resource type
        if (args.getSerializable(ResourceType.RESOURCE_TYPE) != null) {
            resourceType = (ResourceType) args.getSerializable(ResourceType.RESOURCE_TYPE);
            Log.d("ResourceListActivity", "resourceType set to " + resourceType.name());
        }

        // Get title
        if (args.getString(TITLE) != null) {
            setTitle(args.getString(TITLE));
        }
    }

}
