package org.icare.mobile.activity.task;

import android.os.AsyncTask;
import android.util.Log;

import org.icare.mobile.loader.ResourceType;
import org.icare.mobile.model.Alert;
import org.icare.mobile.model.Resource;
import org.icare.mobile.rest.RestAPI;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestClientException;

import java.util.Date;

/**
 * This task sends an alert to the backend REST API with a coordinate string.
 *
 * @author John Meikle
 */
public class SendAlertTask extends AsyncTask<String, Void, Boolean> {

    private RestAPI rest;
    private String errorMessage;
    private Callbacks callbacks;
    private Alert alert;

    /**
     * Construct a new AlertTask.
     *
     * @param rest the REST API object.
     * @param callbacks the class to callback to.
     */
    public SendAlertTask(RestAPI rest, Callbacks callbacks) {
        this.rest = rest;
        this.callbacks = callbacks;
        alert = new Alert();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String urlResource = ResourceType.getUrlResource(ResourceType.ALERTS);
        ParameterizedTypeReference responseType = ResourceType.getResponseType(ResourceType.ALERTS);
        String coords = params[0];
        alert.setCoords(coords);
        alert.setType("emergency");
        alert.setDateTime(new Date());

        try {
            alert = (Alert) rest.add(urlResource, responseType, alert);
        } catch (RestClientException e) {
            Log.e("AlertTask",
                    "Error adding alert (" + e.getMessage() + ").");
            errorMessage = e.getMessage();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean response) {
        if (response) {
            // All is good
            callbacks.onAlertSuccess(alert);
        } else {
            // Something went wrong
            callbacks.onAlertError(alert, errorMessage);
        }
    }

    /**
     * A callback interface for reporting alert success and failure.
     */
    public interface Callbacks {

        /**
         * Called upon a successful alert.
         *
         * @param resource the resulting resource.
         */
        public void onAlertSuccess(Resource resource);

        /**
         * Called upon an unsuccessful alert.
         *
         * @param resource the original resource.
         * @param message an error message.
         */
        public void onAlertError(Resource resource, String message);

    }

}
