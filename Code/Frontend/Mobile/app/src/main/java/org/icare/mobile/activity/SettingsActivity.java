package org.icare.mobile.activity;

import android.app.Activity;
import android.os.Bundle;

import org.icare.mobile.R;
import org.icare.mobile.activity.fragment.SettingsFragment;

/**
 * Created by john on 27/03/15.
 */
public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Display SettingsFragment
        getFragmentManager().beginTransaction()
                .replace(R.id.settings_container, new SettingsFragment())
                .commit();
    }

}
