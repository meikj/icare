package org.icare.mobile.rest;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.icare.mobile.model.Resource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.StringWriter;
import java.util.List;

/**
 * This implements the iCare REST service API through the RestAPI interface.
 */
public class RestAPIImpl implements RestAPI {

    /**
     * The format of the Authorization header.
     */
    public static final String REST_AUTH_FORMAT = "ICARE-REST token=%s";

    /**
     * The contacts URL.
     */
    public static final String REST_URL_CONTACTS = "/contacts";

    /**
     * The tasks URL.
     */
    public static final String REST_URL_TASKS = "/tasks";

    /**
     * The reminders URL.
     */
    public static final String REST_URL_REMINDERS = "/reminders";

    /**
     * The alerts URL.
     */
    public static final String REST_URL_ALERTS = "/alerts";

    /**
     * The tracks URL.
     */
    public static final String REST_URL_TRACKS = "/tracks";

    /**
     * The auth check URL.
     */
    public static final String REST_URL_AUTH_CHECK = "/auth_check";

    private String url;
    private String authToken;
    private boolean authenticated = false;
    private RestTemplate restTemplate;

    /**
     * Construct a new RestAPI implementation for a specified URL (by default, port 80).
     *
     * @param url the server URL.
     */
    public RestAPIImpl(String url) {
        this.url = generateUrl(url);
        initRestTemplate();
    }

    /**
     * Construct a new RestAPI implementation for a specified URL and port.
     *
     * @param url the server URL.
     * @param port the server port number.
     */
    public RestAPIImpl(String url, int port) {
        this.url = generateUrl(url, port);
        initRestTemplate();
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void authenticate(String authToken) throws HttpClientErrorException, ResourceAccessException {
        this.authToken = authToken;
        Log.d("RestAPI", "authenticating with auth token " + authToken);
        get(REST_URL_AUTH_CHECK);

        // if we reach this point, no exception, thus authorised
        authenticated = true;
        Log.d("RestAPI", "authentication successful");
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    public Object get(String urlResource) {
        HttpEntity<Object> response = restTemplate.exchange(url + urlResource, HttpMethod.GET,
                authHeaderEntity(), Object.class);
        return response.getBody();
    }

    @Override
    public <T extends Resource> T getId(String urlResource, int id, ParameterizedTypeReference<T> responseType) {
        HttpEntity<T> response = restTemplate.exchange(url + urlResource + "/" + id, HttpMethod.GET,
                authHeaderEntity(), responseType);
        return response.getBody();
    }

    @Override
    public <T extends Resource> List<T> getAll(String urlResource, ParameterizedTypeReference<List<T>> responseType) {
        HttpEntity<List<T>> response = restTemplate.exchange(url + urlResource, HttpMethod.GET,
                authHeaderEntity(), responseType);
        return response.getBody();
    }

    @Override
    public <T extends Resource> T update(String urlResource, ParameterizedTypeReference<T> responseType, T resource) {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();

        try {
            mapper.writeValue(writer, resource);
            Log.d("RestAPI (update)", writer.toString());
        } catch (Exception e) {
            Log.e("RestAPI (update)", e.getMessage());
        }

        HttpEntity<T> request = new HttpEntity<>(resource, authHeader());
        HttpEntity<T> response = restTemplate.exchange(url + urlResource + "/" + resource.getId(), HttpMethod.PUT,
                request, responseType);
        return response.getBody();
    }

    @Override
    public <T extends Resource> T add(String urlResource, ParameterizedTypeReference<T> responseType, T resource) {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();

        try {
            mapper.writeValue(writer, resource);
            Log.d("RestAPI (add)", writer.toString());
        } catch (Exception e) {
            Log.e("RestAPI (add)", e.getMessage());
        }

        HttpEntity<T> request = new HttpEntity<>(resource, authHeader());
        HttpEntity<T> response = restTemplate.exchange(url + urlResource, HttpMethod.POST,
                request, responseType);
        return response.getBody();
    }

    @Override
    public void delete(String urlResource, int id) throws RestClientException {
        HttpEntity<Object> response = restTemplate.exchange(url + urlResource + "/" + id, HttpMethod.DELETE,
                authHeaderEntity(), Object.class);
    }

    /**
     * Initialises the RestTemplate with Jackson for JSON string to object mapping.
     */
    private void initRestTemplate() {
        Log.d("RestAPIImpl", "url = " + url);
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }

    /**
     * Generates a correct URL from a URL string. This ensures that there is a HTTP protocol
     * prepended, thus validity.
     *
     * @param url the server URL for processing.
     * @return the fully formed server URL.
     */
    private String generateUrl(String url) {
        if (!url.startsWith("http"))
            url = "http://" + url; // if no protocol specified, just use http
        return url;
    }

    /**
     * Generates a correct URL from a URL string. This ensures that there is a HTTP protocol
     * prepended, thus validity. This also appends a specified port number.
     *
     * @param url the server URL for processing.
     * @param port the server port number for appending.
     * @return the fully formed server URL.
     */
    private String generateUrl(String url, int port) {
        return generateUrl(url) + ":" + port;
    }

    /**
     * Generates a body-less HttpEntity containing the authHeader().
     *
     * @return the HttpEntity.
     */
    private HttpEntity<String> authHeaderEntity() {
        HttpEntity<String> entity = new HttpEntity<>(authHeader());
        return entity;
    }

    /**
     * Generates the Authorization header complete with token.
     *
     * @return the HttpHeaders.
     */
    private HttpHeaders authHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", String.format(REST_AUTH_FORMAT, authToken));
        return headers;
    }

}
