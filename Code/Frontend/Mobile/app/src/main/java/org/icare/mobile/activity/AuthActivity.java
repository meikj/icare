package org.icare.mobile.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.icare.mobile.App;
import org.icare.mobile.R;
import org.icare.mobile.activity.task.VerifyAuthTokenTask;

/**
 * This is the initial activity clients are presented. This can be thought of as a one-time auth
 * token setup activity. This activity has functionality for scanning a QR code using a barcode
 * scanner app by Google (see Google ZXing) producing a string auth token. There is verification
 * support.
 *
 * @author John Meikle
 */
public class AuthActivity extends Activity implements VerifyAuthTokenTask.Callbacks {

    /**
     * This points to the barcode scanner app from Google ZXing for QR code image scanning.
     */
    public static final String BARCODE_SCANNER = "com.google.zxing.client.android.SCAN";

    /**
     * Shown to the user during the auth token verification task.
     */
    private ProgressDialog progressDialog;

    @Override
    public void onStart() {
        super.onStart();

        // If the REST API is unauthenticated, check the AppConfig for an auth token
        if (!((App) getApplicationContext()).getRest().isAuthenticated()) {
            String authToken = ((App) getApplicationContext()).getAppConfig().getAuthToken();
            if (authToken != null && !authToken.isEmpty())
                // If so, verify it
                new VerifyAuthTokenTask(((App) getApplicationContext()).getRest(), this).execute(authToken);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        // Set up progress dialog
        progressDialog = new ProgressDialog(AuthActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Verifying authorisation token...");
        progressDialog.setCancelable(false);
    }

    /**
     * Initiates the scanning of a QR code yielding a result.
     *
     * @param view the parent view.
     */
    public void scanQrCode(View view) {
        Intent intent = new Intent(BARCODE_SCANNER);
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        intent.putExtra("SAVE_HISTORY", false);
        startActivityForResult(intent, 0);
    }

    @Override
    public void onVerifyAuthTokenSuccess(String authToken) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (!authToken.equals(((App) getApplicationContext()).getAppConfig().getAuthToken())) {
            // Save to storage if new auth token differs from current
            ((App) getApplicationContext()).getAppConfig().setAuthToken(authToken);
            ((App) getApplicationContext()).saveAppConfig();
        }

        Toast.makeText(AuthActivity.this, "Verification successful", Toast.LENGTH_SHORT).show();
        nextActivity();
    }

    @Override
    public void onVerifyAuthTokenError(String errorMessage) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        String message = String.format("Verification failed (%s)", errorMessage);
        Toast.makeText(AuthActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("SCAN_RESULT");
                new VerifyAuthTokenTask(((App) getApplicationContext()).getRest(), this).execute(result);
            }
        }
    }

    /**
     * Transition to the HomeActivity.
     */
    private void nextActivity() {
        Intent intent = new Intent(AuthActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
