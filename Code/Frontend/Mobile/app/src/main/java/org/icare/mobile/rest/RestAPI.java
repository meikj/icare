package org.icare.mobile.rest;

import org.icare.mobile.model.Resource;
import org.springframework.core.ParameterizedTypeReference;

import java.io.Serializable;
import java.util.List;

/**
 * This is an interface for the iCare REST API. See the iCare REST API documentation for more
 * information.
 */
public interface RestAPI extends Serializable {

    /**
     * Authenticate the RestAPI using an auth token. Upon success, isAuthenticated() will be true.
     *
     * @param authToken the auth token.
     */
    public void authenticate(String authToken);

    /**
     * Check whether or not this RestAPI instance is authenticated.
     */
    public boolean isAuthenticated();

    /**
     * Get the full server URL.
     *
     * @return the full server URL.
     */
    public String getUrl();

    /**
     * Get the list of all resources of a certain type.
     *
     * @param urlResource the URL of the resource.
     * @param responseType the type of response.
     * @param <T> the type of resource.
     * @return the list of all resources.
     */
    public <T extends Resource> List<T> getAll(String urlResource, ParameterizedTypeReference<List<T>> responseType);

    /**
     * Get the resource of a certain type which corresponds to the specified ID.
     *
     * @param urlResource the URL resource.
     * @param id the resource ID.
     * @param responseType the type of response.
     * @param <T> the type of resource.
     * @return the resource.
     */
    public <T extends Resource> T getId(String urlResource, int id, ParameterizedTypeReference<T> responseType);

    /**
     * Update the resource of a certain type using the contents of a model which corresponds to the
     * specified ID in the model.
     *
     * @param urlResource the URL resource.
     * @param responseType the type of response.
     * @param <T> the type of resource.
     * @return the resulting resource.
     */
    public <T extends Resource> T update(String urlResource, ParameterizedTypeReference<T> responseType, T resource);

    /**
     * Add the resource of a certain type using the contents of a model.
     *
     * @param urlResource the URL resource.
     * @param responseType the type of response.
     * @param <T> the type of resource.
     * @return the resulting resource.
     */
    public <T extends Resource> T add(String urlResource, ParameterizedTypeReference<T> responseType, T resource);

    /**
     * Delete the resource which corresponds to the specified ID.
     *
     * @param urlResource the URL resource.
     * @param id the resource ID.
     */
    public void delete(String urlResource, int id);

}
