package org.icare.mobile.activity.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.icare.mobile.R;
import org.icare.mobile.model.Reminder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This is the form fragment implementation for a reminder.
 *
 * @author John Meikle
 */
public class ReminderFormFragment extends ResourceFormFragment
        implements AdapterView.OnItemSelectedListener, DatePickerFragment.Callbacks, TimePickerFragment.Callbacks {

    private Reminder reminder;

    private EditText title;
    private EditText description;
    private TextView startDate;
    private TextView startTime;
    private EditText address;
    private Spinner frequencyType;
    private String frequencyTypeString;
    private EditText frequencyValue;

    private int startYear;
    private int startMonth;
    private int startDay;
    private int startHour;
    private int startMinute;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (resource != null && resource instanceof Reminder) {
            reminder = (Reminder) this.resource;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reminder_form, container, false);

        title = (EditText) rootView.findViewById(R.id.reminder_detail_title);
        description = (EditText) rootView.findViewById(R.id.reminder_detail_description);
        startDate = (TextView) rootView.findViewById(R.id.reminder_detail_start_date);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new DatePickerFragment();
                if (startYear > 0 && startMonth > 0 && startDay > 0) {
                    Bundle args = new Bundle();
                    args.putInt("YEAR", startYear);
                    args.putInt("MONTH", startMonth);
                    args.putInt("DAY", startDay);
                    dateFragment.setArguments(args);
                }
                dateFragment.setTargetFragment(ReminderFormFragment.this, 0); // 0 = start date
                dateFragment.show(getFragmentManager(), "DatePicker");
            }
        });
        startTime = (TextView) rootView.findViewById(R.id.reminder_detail_start_time);
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dateFragment = new TimePickerFragment();
                if (startHour > 0 && startMinute > 0) {
                    Bundle args = new Bundle();
                    args.putInt("HOUR", startHour);
                    args.putInt("MINUTE", startMinute);
                    dateFragment.setArguments(args);
                }
                dateFragment.setTargetFragment(ReminderFormFragment.this, 0); // 0 = start time
                dateFragment.show(getFragmentManager(), "TimePicker");
            }
        });
        address = (EditText) rootView.findViewById(R.id.reminder_detail_address);
        frequencyType = (Spinner) rootView.findViewById(R.id.reminder_detail_frequency_type);
        ArrayAdapter<CharSequence> freqTypeAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.frequency_types, android.R.layout.simple_spinner_item);
        freqTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        frequencyType.setAdapter(freqTypeAdapter);
        frequencyType.setOnItemSelectedListener(this);
        frequencyValue = (EditText) rootView.findViewById(R.id.reminder_detail_frequency_value);
        ((Button) rootView.findViewById(R.id.resource_form_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateResource();
                callbacks.onButtonSelected(reminder);
            }
        });

        if (reminder == null) {
            reminder = new Reminder();
            resource = reminder;
        } else {
            title.setText(reminder.getTitle());
            description.setText(reminder.getDescription());
            String[] dateTime = reminder.getStartDateTime().split(" ");
            startDate.setText(dateTime[0]);
            startTime.setText(dateTime[1]);
            address.setText(reminder.getLocation());
            int pos = freqTypeAdapter.getPosition(reminder.getFrequencyType());
            frequencyType.setSelection(pos);
            frequencyValue.setText(Integer.toString(reminder.getFrequencyValue()));
        }

        return rootView;
    }

    public void updateResource() {
        reminder.setTitle(title.getText().toString());
        reminder.setDescription(description.getText().toString());

        if (startDay > 0 && startMonth > 0 && startYear > 0 && startHour > 0 && startMinute > 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, startYear);
            calendar.set(Calendar.MONTH, startMonth);
            calendar.set(Calendar.DAY_OF_MONTH, startDay);
            calendar.set(Calendar.HOUR_OF_DAY, startHour);
            calendar.set(Calendar.MINUTE, startMinute);

            reminder.setStartDate(calendar.getTime());
        }

        reminder.setLocation(address.getText().toString());
        reminder.setFrequencyType(frequencyTypeString);
        try {
            reminder.setFrequencyValue(Integer.parseInt(frequencyValue.getText().toString()));
        } catch (Exception e) { }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        frequencyTypeString = (String) parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDateEntered(int requestCode, int year, int month, int day) {
        if (requestCode == 0) {
            // start date
            startYear = year;
            startMonth = month;
            startDay = day;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, startYear);
            calendar.set(Calendar.MONTH, startMonth);
            calendar.set(Calendar.DAY_OF_MONTH, startDay);
            Date date = calendar.getTime();

            startDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        }
    }

    @Override
    public void onTimeEntered(int requestCode, int hour, int minute) {
        if (requestCode == 0) {
            // start date
            startHour = hour;
            startMinute = minute;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, startHour);
            calendar.set(Calendar.MINUTE, startMinute);
            Date date = calendar.getTime();

            startTime.setText(new SimpleDateFormat("HH:mm").format(date));

        }
    }

}
