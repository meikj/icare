package org.icare.mobile.activity.fragment;

import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.icare.mobile.R;
import org.icare.mobile.model.Resource;
import org.icare.mobile.model.Task;

/**
 * This is the detail fragment implementation for a task.
 *
 * @author John Meikle
 */
public class TaskDetailFragment extends ResourceDetailFragment {

    private Task task;
    private TextView title;
    private TextView description;
    private TextView startDate;
    private TextView endDate;
    private TextView address;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        task = new Task();
        resource = task;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_detail, container, false);
        title = (TextView) rootView.findViewById(R.id.task_detail_title);
        description = (TextView) rootView.findViewById(R.id.task_detail_description);
        startDate = (TextView) rootView.findViewById(R.id.task_detail_start_date);
        endDate = (TextView) rootView.findViewById(R.id.task_detail_end_date);
        address = (TextView) rootView.findViewById(R.id.task_detail_address);
        return rootView;
    }

    @Override
    public void loadResource(Resource resource) {
        if (resource != null && resource instanceof Task) {
            task = (Task) resource;
            title.setText(task.getTitle());
            description.setText(task.getDescription());
            startDate.setText(task.getStartDateTime());
            endDate.setText(task.getEndDateTime());
            address.setText(task.getLocation());
            Linkify.addLinks(address, Linkify.MAP_ADDRESSES);
        }
    }

}
