package org.icare.mobile.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

import org.icare.mobile.R;

/**
 * Created by john on 27/03/15.
 */
public class AboutActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // Make the email text linkable
        TextView emailText = (TextView) findViewById(R.id.about_email_text);
        Linkify.addLinks(emailText, Linkify.EMAIL_ADDRESSES);
    }

}
