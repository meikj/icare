package org.icare.mobile.activity.fragment;

import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.icare.mobile.R;
import org.icare.mobile.model.Contact;
import org.icare.mobile.model.Resource;

/**
 * This is the detail fragment implementation for a contact.
 *
 * @author John Meikle
 */
public class ContactDetailFragment extends ResourceDetailFragment {

    private Contact contact;
    private TextView name;
    private TextView description;
    private TextView phone;
    private TextView email;
    private TextView address;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contact = new Contact();
        resource = contact;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_detail, container, false);
        name = (TextView) rootView.findViewById(R.id.contact_detail_name);
        description = (TextView) rootView.findViewById(R.id.contact_detail_description);
        phone = (TextView) rootView.findViewById(R.id.contact_detail_phone);
        email = (TextView) rootView.findViewById(R.id.contact_detail_email);
        address = (TextView) rootView.findViewById(R.id.contact_detail_address);
        return rootView;
    }

    @Override
    public void loadResource(Resource resource) {
        if (resource != null && resource instanceof Contact) {
            contact = (Contact) resource;
            name.setText(contact.getFirstName() + " " + contact.getLastName());
            description.setText(contact.getDescription());
            phone.setText(contact.getPhone());
            Linkify.addLinks(phone, Linkify.PHONE_NUMBERS);
            email.setText(contact.getEmail());
            Linkify.addLinks(email, Linkify.EMAIL_ADDRESSES);
            address.setText(contact.getAddress());
            Linkify.addLinks(address, Linkify.MAP_ADDRESSES);
        }
    }

}
