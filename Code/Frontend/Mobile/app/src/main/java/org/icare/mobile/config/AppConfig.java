package org.icare.mobile.config;

import android.util.Log;

import com.google.android.gms.location.LocationRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * This represents an application configuration class. A config can be constructed from a key-value
 * mapped store (i.e. Map). The key-value store should take the following format in JSON (which
 * applies to application.json), for example:
 *
 *  { "restApi":  { "url": "http://meikj.com",
 *                  "port": 8080,
 *                  "authToken": "my_secret_unique_auth_token" },
 *    "tracking": { "enabled": true,
 *                  "intervalMillis": 10000,
 *                  "priorityLevel": 1 } }
 *
 */
public class AppConfig {

    /**
     * The default configuration value for the REST API URL.
     */
    public static final String DEFAULT_REST_API_URL = "https://meikj.com";

    /**
     * The default configuration value for the REST API port number.
     */
    public static final int DEFAULT_REST_API_PORT = 5050;

    /**
     * The default configuration value for tracking enabled.
     */
    public static final boolean DEFAULT_TRACKING_ENABLED = true;

    /**
     * The default configuration value for tracking update interval in milliseconds.
     */
    public static final int DEFAULT_TRACKING_INTERVAL_MS = 1000 * 60; // 1 minute

    /**
     * The default configuration value for tracking priority.
     */
    public static final int DEFAULT_TRACKING_PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY;

    // Reference to key-value mapped store
    private Map keyValue;

    // REST API properties
    private String restUrl;
    private int restPort;
    private String authToken;

    // Tracking properties
    private boolean trackEnabled;
    private int trackInterval;
    private int trackPriority;

    // TODO: More...?

    /**
     * Construct a new AppConfig with default configuration values.
     */
    public AppConfig() {
        loadDefaults();
    }

    /**
     * Construct a new AppConfig using a key-value store.
     *
     * @param keyValue the key-value store.
     */
    public AppConfig(Map keyValue) {
        this.keyValue = keyValue;

        // This ensures sane fallback values
        loadDefaults();
        processKeyValue(keyValue);
    }

    /**
     * Load the default configuration values (see this classes DEFAULT_* constants) for a fresh
     * configuration.
     */
    public synchronized void loadDefaults() {
        restUrl = DEFAULT_REST_API_URL;
        restPort = DEFAULT_REST_API_PORT;
        authToken = "";
        trackEnabled = DEFAULT_TRACKING_ENABLED;
        trackInterval = DEFAULT_TRACKING_INTERVAL_MS;
        trackPriority = DEFAULT_TRACKING_PRIORITY;
    }

    /**
     * Get the REST API URL value (restApi.url).
     */
    public synchronized String getRestUrl() {
        return restUrl;
    }

    /**
     * Get the REST API port number value (restApi.port).
     */
    public synchronized int getRestPort() {
        return restPort;
    }

    /**
     * Get the REST API auth token value (restApi.authToken).
     */
    public synchronized String getAuthToken() {
        return authToken;
    }

    /**
     * Check whether or not tracking is enabled (tracking.enabled).
     */
    public synchronized boolean isTrackEnabled() {
        return trackEnabled;
    }

    /**
     * Get the track location update interval in milliseconds (tracking.intervalMillis).
     */
    public synchronized int getTrackInterval() {
        return trackInterval;
    }

    /**
     * Get the track priority for location updates (a Location.Services priority constant)
     * (tracking.priority).
     */
    public synchronized int getTrackPriority() {
        return trackPriority;
    }

    /**
     * Get the Map key-value representation of this configuration.
     *
     * @return the Map in the format as specified in this class doc.
     */
    public synchronized Map getKeyValue() {
        makeKeyValue(); // ensures keyValue has up to date values
        return keyValue;
    }

    /**
     * Set the REST API URL value (restApi.url).
     *
     * @param restUrl the new REST API URL value.
     */
    public synchronized void setRestUrl(String restUrl) {
        this.restUrl = restUrl;
    }

    /**
     * Set the REST API port number value (restApi.port).
     *
     * @param restPort the new REST API port number value.
     */
    public synchronized void setRestPort(int restPort) {
        this.restPort = restPort;
    }

    /**
     * Set the REST API auth token value (restApi.authToken).
     *
     * @param authToken the new REST API auth token value.
     */
    public synchronized void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    /**
     * Set whether or not tracking is enabled (tracking.enabled).
     *
     * @param trackEnabled the new tracking enabled value.
     */
    public synchronized void setTrackEnabled(boolean trackEnabled) {
        this.trackEnabled = trackEnabled;
    }

    /**
     * Set the track update interval value in milliseconds (tracking.intervalMillis).
     *
     * @param trackInterval the new track interval value.
     */
    public synchronized void setTrackInterval(int trackInterval) {
        this.trackInterval = trackInterval;
    }

    /**
     * Set the track priority for location updates (a Location.Services priority constant)
     * (tracking.priority).
     *
     * @param trackPriority the new track priority value.
     */
    public synchronized void setTrackPriority(int trackPriority) {
        this.trackPriority = trackPriority;
    }

    /**
     * Process a key-value store for application configuration mappings in the format as specified
     * in this classes doc.
     *
     * @param keyValue the key-value store.
     */
    private void processKeyValue(Map keyValue) {
        if (keyValue == null || keyValue.isEmpty()) {
            Log.e("AppConfig", "Can't process an empty key-value store.");
            return;
        }

        Log.d("AppConfig", "Processing key-value store: " + keyValue);

        // Process REST API config
        if (keyValue.containsKey("restApi")) {
            Map restApi = (Map) keyValue.get("restApi");

            Log.d("AppConfig", " restApi = " + restApi);

            if (restApi.containsKey("url")) {
                Log.d("AppConfig", "  url = " + restApi.get("url"));
                restUrl = (String) restApi.get("url");
            }
            if (restApi.containsKey("port")) {
                Log.d("AppConfig", "  port = " + restApi.get("port"));
                restPort = (int) restApi.get("port");
            }
            if (restApi.containsKey("authToken")) {
                Log.d("AppConfig", "  authToken = " + restApi.get("authToken"));
                authToken = (String) restApi.get("authToken");
            }
        }

        // Process tracking config
        if (keyValue.containsKey("tracking")) {
            Map tracking = (Map) keyValue.get("tracking");

            Log.d("AppConfig", " tracking = " + tracking);

            if (tracking.containsKey("enabled")) {
                Log.d("AppConfig", "  enabled = " + tracking.get("enabled"));
                trackEnabled = (boolean) tracking.get("enabled");
            }
            if (tracking.containsKey("intervalMillis")) {
                Log.d("AppConfig", "  intervalMillis = " + tracking.get("intervalMillis"));
                trackInterval = (int) tracking.get("intervalMillis");
            }
            if (tracking.containsKey("priorityLevel")) {
                Log.d("AppConfig", "  priorityLevel = " + tracking.get("priorityLevel"));
                trackPriority = (int) tracking.get("priorityLevel");
            }
        }
    }

    /**
     * Makes a key-value mapped store from this AppConfigs properties adhering to the format
     * specified in this class doc and sets this classes keyValue to it.
     */
    private void makeKeyValue() {
        Map<String, Object> map = new HashMap<>(2); // 2 mappings reside in root
        map.put("restApi", makeMapRestApi());
        map.put("tracking", makeMapTracking());
        keyValue = map;
    }

    /**
     * Makes a mapping of the REST API configuration values.
     *
     * @return the map of REST API config values.
     */
    private Map<String, Object> makeMapRestApi() {
        Map<String, Object> map = new HashMap<>(3); // 3 mappings reside in restApi
        map.put("url", restUrl);
        map.put("port", restPort);
        map.put("authToken", authToken);
        return map;
    }

    /**
     * Makes a mapping of the tracking configuration values.
     *
     * @return the map of the tracking config values.
     */
    private Map<String, Object> makeMapTracking() {
        Map<String, Object> map = new HashMap<>(3); // 3 mappings reside in tracking
        map.put("enabled", trackEnabled);
        map.put("intervalMillis", trackInterval);
        map.put("priorityLevel", trackPriority);
        return map;
    }

}
