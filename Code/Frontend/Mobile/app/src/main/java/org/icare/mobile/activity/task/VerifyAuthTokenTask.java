package org.icare.mobile.activity.task;

import android.os.AsyncTask;
import android.util.Log;

import org.icare.mobile.rest.RestAPI;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

/**
 * This is a task for verifying an auth token against the app context REST API instance. If the
 * auth token is valid, then the RestAPI is authenticated.
 *
 * @author John Meikle
 */
public class VerifyAuthTokenTask extends AsyncTask<String, Void, Boolean> {

    private RestAPI rest;
    private Callbacks callbacks;
    private String errorMessage;
    private String authToken;

    /**
     * Construct a new VerifyAuthTokenTask.
     *
     * @param rest the REST API object.
     * @param callbacks the class to callback to.
     */
    public VerifyAuthTokenTask(RestAPI rest, Callbacks callbacks) {
        this.rest = rest;
        this.callbacks = callbacks;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        authToken = params[0];

        try {
            rest.authenticate(authToken);
        } catch (HttpClientErrorException e) {
            errorMessage = e.getMessage();
            return false;
        } catch (ResourceAccessException e) {
            Log.e("VerifyAuthTokenTask", e.getMessage());
            errorMessage = "Failed to connect to server";
            return false;
        }

        return rest.isAuthenticated();
    }

    @Override
    protected void onPostExecute(Boolean response) {
        if (response) {
            callbacks.onVerifyAuthTokenSuccess(authToken);
        } else {
            callbacks.onVerifyAuthTokenError(errorMessage);
        }
    }

    /**
     * A callback interface for reporting auth token verification success and failure.
     */
    public interface Callbacks {
        /**
         * Called upon a successful auth token verification (i.e. auth token is valid).
         *
         * @param authToken the valid auth token.
         */
        public void onVerifyAuthTokenSuccess(String authToken);

        /**
         * Called upon an unsuccessful auth token verification (i.e. auth token is invalid).
         *
         * @param errorMessage the error message.
         */
        public void onVerifyAuthTokenError(String errorMessage);
    }

}
