package org.icare.mobile.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a tracking model. Provides properties for id, coords, date and
 * time, and the associated client ID.
 * 
 * @author John Meikle
 *
 */
@JsonIgnoreProperties({"dateTimeString", "dateFormat"})
public class Track extends Resource {

	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	private String coords;
	private Date dateTime;

	public SimpleDateFormat dateFormat = new SimpleDateFormat(
			DATE_TIME_PATTERN);

	public Track() {
		// for construction via getters setters
	}

	public Track(int id, String coords, Timestamp dateTimestamp, int clientId) {
		super(id, clientId);
		this.coords = coords;
		this.dateTime = new Date(dateTimestamp.getTime());
	}

	public Track(int id, String coords, Date dateTime, int clientId) {
		super(id, clientId);
		this.coords = coords;
		this.dateTime = dateTime;
	}

	public String getCoords() {
		return coords;
	}

	public void setCoords(String coords) {
		this.coords = coords;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public void setDateTimestamp(Timestamp dateTimestamp) {
		this.dateTime = new Date(dateTimestamp.getTime());
	}

	public String getDateTimeString() {
		return dateFormat.format(dateTime.getTime());
	}

    @Override
    public String toString() {
        return coords;
    }

}
