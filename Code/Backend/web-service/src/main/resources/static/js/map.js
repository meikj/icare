var map;
$(document).ready(function() {
	map = new GMaps({
		div : '#map',
		lat : 55.864237,
		lng : -4.251805,
		width : '400px',
		height : '300px'
	});
	
	var element = document.getElementById('address');
	if (element) {
		var ad = element.textContent;
		searchAddress(ad, function(results, status) {
			if (status == 'OK') {
				var coords = results[0].geometry.location;
				map.setCenter(coords.lat(), coords.lng());
				map.addMarker({
					lat : coords.lat(),
					lng : coords.lng()
				});
			}
		});
	}
});

function searchAddress(ad, cb) {
	GMaps.geocode({
		address : ad,
		callback : cb
	});
}

function onAddressSearchClick() {
	var ad = $('#input-address').val();
	searchAddress(ad, function(results, status) {
		if (status == 'OK') {
			var coords = results[0].geometry.location;
			map.setCenter(coords.lat(), coords.lng());
			map.addMarker({
				lat : coords.lat(),
				lng : coords.lng()
			});
			var address = results[0].formatted_address;
			var addressLines = address.split(',');
			var newAddress = "";

			for (var i = 0; i < addressLines.length; i++) {
				newAddress += addressLines[i] + '\n';
			}

			$('#input-address').val(newAddress);
		}
	});
}

$(function() {
	$('#input-start-date').datetimepicker({
		locale : 'en-GB',
		format : 'DD/MM/YYYY HH:mm'
	});
	$('#input-end-date').datetimepicker({
		locale : 'en-GB',
		format : 'DD/MM/YYYY HH:mm'
	});
	$('#input-dob').datetimepicker({
		locale : 'en-GB',
		format : 'DD/MM/YYYY'
	});
});