package org.icare.web.controller;

import org.icare.shared.model.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This is an implementation for an Alert resource controller. This controls the
 * alerts section of the web service.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/alerts")
public class AlertsController extends ResourceController<Alert> {
	
	private static final Logger log = LoggerFactory
			.getLogger(AlertsController.class);

	public static final String PAGE_NAME = "alerts";
	public static final String RESOURCE_NAME = "alert";

	public AlertsController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(Alert model) {
		// no need to do anything
	}

	@RequestMapping(value = "/{id}/ack", method = RequestMethod.GET)
	public String ack(
			@PathVariable int id,
			@RequestHeader(value = "Referer", required = false) final String referer) {
		Alert alert = (Alert) resourceRepository.findById(id);
		alert.setAck(true);
		resourceRepository.update(alert);
		
		log.info(String
				.format("(Username = %s, Referer = %s) Acknowledged alert ID %d",
						getUser().getUsername(), referer, alert.getId()));

		// Form a redirect string using either the Referer or alerts page
		String redirect = "redirect:";
		redirect += (referer == null) ? "/alerts" : referer;

		return redirect;
	}

	@RequestMapping(value = "/{id}/unack", method = RequestMethod.GET)
	public String unack(
			@PathVariable int id,
			@RequestHeader(value = "Referer", required = false) final String referer) {
		Alert alert = (Alert) resourceRepository.findById(id);
		alert.setAck(false);
		resourceRepository.update(alert);
		
		log.info(String
				.format("(Username = %s, Referer = %s) Unacknowledged alert ID %d",
						getUser().getUsername(), referer, alert.getId()));

		// Form a redirect string using either the Referer or alerts page
		String redirect = "redirect:";
		redirect += (referer == null) ? "/alerts" : referer;

		return redirect;
	}

	@Override
	public Alert getNewResource() {
		return new Alert();
	}

	/**
	 * Adding new alerts is not supported from web interface.
	 */
	@Override
	public boolean canNew() {
		return false;
	}

	/**
	 * Editing alerts is not supported from web interface.
	 */
	@Override
	public boolean canEdit() {
		return false;
	}

}
