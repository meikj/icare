package org.icare.web.controller;

import org.icare.shared.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This is an implementation for a Task resource controller. This controls the
 * tasks section of the web service.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/tasks")
public class TasksController extends ResourceController<Task> {

	private static final Logger log = LoggerFactory
			.getLogger(TasksController.class);

	public static final String PAGE_NAME = "tasks";
	public static final String RESOURCE_NAME = "task";

	public TasksController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(Task task) {
		// no need to do anything
	}

	@RequestMapping(value = "/{id}/complete", method = RequestMethod.GET)
	public String complete(
			@PathVariable int id,
			@RequestHeader(value = "Referer", required = false) final String referer) {
		Task task = (Task) resourceRepository.findById(id);
		task.setComplete(true);
		resourceRepository.update(task);

		log.info(String
				.format("(Username = %s, Referer = %s) Marked task ID %d ('%s') complete",
						getUser().getUsername(), referer, task.getId(),
						task.toString()));

		// Form a redirect string using either the Referer or task page
		String redirect = "redirect:";
		redirect += (referer == null) ? "/tasks" : referer;

		return redirect;
	}

	@RequestMapping(value = "/{id}/incomplete", method = RequestMethod.GET)
	public String incomplete(
			@PathVariable int id,
			@RequestHeader(value = "Referer", required = false) final String referer) {
		Task task = (Task) resourceRepository.findById(id);
		task.setComplete(false);
		resourceRepository.update(task);

		log.info(String
				.format("(Username = %s, Referer = %s) Marked task ID %d ('%s') incomplete",
						getUser().getUsername(), referer, task.getId(),
						task.toString()));

		// Form a redirect string using either the Referer or task page
		String redirect = "redirect:";
		redirect += (referer == null) ? "/tasks" : referer;

		return redirect;
	}

	@Override
	public Task getNewResource() {
		return new Task();
	}

}
