package org.icare.web.controller;

import java.util.List;

import org.icare.shared.data.QueryConstructor;
import org.icare.shared.data.QueryFilter;
import org.icare.shared.data.ResourceRepository;
import org.icare.shared.model.Alert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controls the home section of the web application. This implements the
 * BaseController for bare minimal support.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/")
public class HomeController extends BaseController {

	/**
	 * This is the page name used to construct the BaseController.
	 */
	public static final String PAGE_NAME = "home";
	
	/**
	 * This is the model attribute string used for recent alerts.
	 */
	public static final String RECENT_ALERTS_ATTRIBUTE = "recentAlerts";
	
	@Value("${home.recentAlertLimit}")
	private int recentAlertLimit;
	
	@Autowired
	private ResourceRepository<Alert> alertRepository;

	/**
	 * Construct a new HomeController.
	 */
	public HomeController() {
		super(PAGE_NAME);
	}
	
	/**
	 * Provides a list of recent alerts currently unacknowledged for the home
	 * section.
	 */
	@ModelAttribute(RECENT_ALERTS_ATTRIBUTE)
	public List<Alert> recentAlerts() {
		QueryFilter filterUnack = QueryConstructor.createFilter(
				QueryConstructor.FILTER_TYPE_WHERE, "ack");
		filterUnack.addParamValue(0); // false
		QueryFilter filterLimit = QueryConstructor.createFilter(
				QueryConstructor.FILTER_TYPE_LIMIT, recentAlertLimit);
		
		return alertRepository.findAll(filterUnack, filterLimit);
	}

}
