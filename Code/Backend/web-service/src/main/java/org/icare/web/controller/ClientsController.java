package org.icare.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.icare.shared.exception.InternalServerErrorException;
import org.icare.shared.model.Client;
import org.icare.shared.model.User;
import org.icare.shared.security.AuthTokenProvider;
import org.icare.web.service.QRCodeImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This is an implementation for a Client resource controller. This controls the
 * clients section of the web service.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/clients")
public class ClientsController extends ResourceController<Client> {

	private static final Logger log = LoggerFactory
			.getLogger(ClientsController.class);

	public static final String PAGE_NAME = "clients";
	public static final String RESOURCE_NAME = "client";

	@Autowired
	private AuthTokenProvider authTokenProvider;

	@Autowired
	private QRCodeImageService qrImageService;

	public ClientsController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(Client client) {
		// Set the carer and generate an auth token before adding a client
		client.setUserId(getUser().getId());
		client.setAuthToken(authTokenProvider.generateToken());
	}

	@RequestMapping(value = "/{id}/auth_token", method = RequestMethod.GET, produces = "image/png")
	public @ResponseBody byte[] getAuthTokenQRImage(@PathVariable int id) {
		Client client = clientRepository.findById(id);
		ByteArrayOutputStream byteArray = new ByteArrayOutputStream();

		try {
			BufferedImage image = qrImageService.generateBufferedImage(client
					.getAuthToken());
			ImageIO.write(image, "png", byteArray);
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}

		log.info(String
				.format("(Username = %s) Generated auth token for client ID %d (%d bytes)",
						getUser().getUsername(), id, byteArray.size()));

		// TODO: It would be good to fetch from cache, generation is quite
		// expensive

		return byteArray.toByteArray();
	}

	@RequestMapping(value = "/{id}/auth_token/regen", method = RequestMethod.GET)
	public String regenAuthToken(
			@PathVariable int id,
			@RequestHeader(value = "Referer", required = false) final String referer) {
		Client client = clientRepository.findById(id);
		client.setAuthToken(authTokenProvider.generateToken());
		clientRepository.update(client);

		log.info(String.format(
				"(Username = %s) Regenerated auth token for client ID %d",
				getUser().getUsername(), id));

		// Form a redirect string using either the Referer or client page
		String redirect = "redirect:";
		redirect += (referer == null) ? ("/clients/" + id) : referer;

		return redirect;
	}

	@RequestMapping(value = "/{id}/select", method = RequestMethod.GET)
	public String selectId(
			@PathVariable int id,
			@RequestHeader(value = "Referer", required = false) final String referer) {
		// findById() ensures the user is authorised to select the client
		Client client = clientRepository.findById(id);
		((User) SecurityContextHolder.getContext().getAuthentication()
				.getDetails()).setSelectedClientId(client.getId());
		log.info(String
				.format("(Username = %s, Referer = %s) Selected client: { id = %d, name = %s }",
						getUser().getUsername(), referer, client.getId(),
						client.toString()));

		// Form a redirect string using either the Referer or client page
		String redirect = "redirect:";
		redirect += (referer == null) ? ("/clients/" + id) : referer;

		return redirect;
	}

	@RequestMapping(value = "/select_all", method = RequestMethod.GET)
	public String selectAll(
			@RequestHeader(value = "Referer", required = false) final String referer) {
		// Setting selectedClientId to 0 means select all
		((User) SecurityContextHolder.getContext().getAuthentication()
				.getDetails()).setSelectedClientId(0);
		log.info(String.format(
				"(Username = %s, Referer = %s) Selected ALL clients", getUser()
						.getUsername(), referer));

		// Form a redirect string using either the Referer or client page
		String redirect = "redirect:";
		redirect += (referer == null) ? "/clients" : referer;

		return redirect;
	}

	@Override
	public Client getNewResource() {
		return new Client();
	}

}
