package org.icare.web.controller;

import org.icare.shared.model.Track;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This is an implementation for a Track resource controller. This controls the
 * tracking section of the web service.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/tracks")
public class TrackingController extends ResourceController<Track> {

	public static final String PAGE_NAME = "tracks";
	public static final String RESOURCE_NAME = "track";

	public TrackingController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(Track model) {
		// no need to do anything
	}

	@Override
	public Track getNewResource() {
		return new Track();
	}

	/**
	 * Adding new tracks is not supported from web interface.
	 */
	@Override
	public boolean canNew() {
		return false;
	}

	/**
	 * Editing tracks is not supported from web interface.
	 */
	@Override
	public boolean canEdit() {
		return false;
	}

}
