package org.icare.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Entry point to the web application. This auto configures the application
 * by detecting what kind of beans are loaded. This will therefore auto
 * configure this application to be a web application, thus loading and
 * sanely configuring all necessary web application beans.
 * 
 * @author John Meikle
 *
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("org.icare")
public class Application {
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Application.class);
		app.setShowBanner(false);
		app.run(args);
	}

}
