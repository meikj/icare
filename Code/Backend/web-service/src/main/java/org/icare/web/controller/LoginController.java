package org.icare.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controls the login section of the web application. Spring Security will point
 * to here and the login template string is returned.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/login")
public class LoginController {

	@RequestMapping(method = RequestMethod.GET)
	public String login() {
		return "login";
	}

}
