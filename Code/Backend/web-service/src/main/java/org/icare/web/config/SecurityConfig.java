package org.icare.web.config;

import javax.sql.DataSource;

import org.icare.shared.security.AuthTokenProvider;
import org.icare.web.security.AuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;

/**
 * The web security configuration of the web application.
 * 
 * @author John Meikle
 *
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	/**
	 * This bean is required for Thymeleaf Spring Security support.
	 */
	@Bean
	public SpringSecurityDialect securityDialect() {
		return new SpringSecurityDialect();
	}

	/**
	 * This bean ensures AuthProvider is used as the Spring Security
	 * AuthenticationProvider.
	 */
	@Bean
	public AuthenticationProvider authProvider() {
		return new AuthProvider();
	}

	/**
	 * This bean provides access to auth token methods.
	 */
	@Bean
	public AuthTokenProvider authTokenProvider() {
		return new AuthTokenProvider();
	}

	/**
	 * Configures the web application security to require all requests to be
	 * authorised. If a user is unauthorised, they are directed to a login page.
	 * There is also logout support.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and().formLogin()
				.loginPage("/login").permitAll().and().logout().permitAll();
	}

	/**
	 * Configures the web application authentication procedure. This uses JDBC
	 * authentication with the dataSource bean. The username and authority
	 * queries have been explicitly set to ensure support for backend database
	 * schema.
	 * 
	 * @param auth
	 *            The AuthenticationManagerBuilder for building in JDBC based
	 *            authentication.
	 * @throws Exception
	 *             If an error with JDBC based authentication occurs.
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(authProvider());
	}

}
