package org.icare.web.controller;

import org.icare.shared.model.Contact;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This is an implementation for a Contact resource controller. This controls
 * the contacts section of the web service.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/contacts")
public class ContactsController extends ResourceController<Contact> {

	public static final String PAGE_NAME = "contacts";
	public static final String RESOURCE_NAME = "contact";

	public ContactsController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(Contact model) {
		// no need to do anything
	}

	@Override
	public Contact getNewResource() {
		return new Contact();
	}

}
