package org.icare.web.controller;

import java.util.List;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.exception.InternalServerErrorException;
import org.icare.shared.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * This is a basic abstract implementation for all resource controllers. This
 * works with any Resource model providing basic supporting for adding, viewing,
 * editing and deleting resources.
 * 
 * @author John Meikle
 * 
 * @param <T>
 *            the type of Resource.
 */
public abstract class ResourceController<T extends Resource> extends
		BaseController {

	private static final Logger log = LoggerFactory
			.getLogger(ResourceController.class);

	public static final String RESOURCES_TEMPLATE = "resources";
	public static final String RESOURCE_TEMPLATE = "resource";
	public static final String RESOURCE_EDIT_TEMPLATE = "resource_edit";

	public static final String REDIRECT_SUCCESS = "redirect:/%s?success";
	public static final String REDIRECT_ERROR = "redirect:/%s?error";
	public static final String REDIRECT_EDIT_SUCCESS = "redirect:/%s/%d?success";
	public static final String REDIRECT_EDIT_ERROR = "redirect:/%s/%d?error";

	@Autowired
	protected ResourceRepository<T> resourceRepository;
	protected final String resourceName;
	protected final String redirectSuccess;
	protected final String redirectError;

	/**
	 * Construct a ResourceController with a page name and resource name. The
	 * page name is used to construct the BaseController. The resource name is
	 * used to superficially name a singular resource of type T.
	 * 
	 * @param pageName
	 *            the page name.
	 * @param resourceName
	 *            the resource name.
	 */
	public ResourceController(final String pageName, final String resourceName) {
		super(pageName);
		this.resourceName = resourceName;
		this.redirectSuccess = String.format(REDIRECT_SUCCESS, pageName);
		this.redirectError = String.format(REDIRECT_ERROR, pageName);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String get() {
		return RESOURCES_TEMPLATE;
	}

	/**
	 * Executed before adding resource to repository in add(). Use this to
	 * perform operations on a resource model before it is added, such as adding
	 * or manipulating fields.
	 * 
	 * @param model
	 *            the resource model.
	 */
	public abstract void beforeAdd(T model);

	/**
	 * Executed when a POST request is made to this resource controller.
	 * 
	 * @param model
	 *            the resource model from the request.
	 * @param result
	 *            the result of the binding.
	 * @param redirectAttrs
	 *            the redirect attributes.
	 * @return the redirect string.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public String add(@ModelAttribute("resource") T model,
			BindingResult result, RedirectAttributes redirectAttrs) {
		if (!canNew())
			throw new InternalServerErrorException("Adding " + resourceName
					+ " is unsupported.");

		try {
			beforeAdd(model);
			log.info(String.format(
					"(Username = %s) Adding resource %s ('%s') to %s",
					getUser().getUsername(), resourceName, model.toString(),
					resourceRepository.toString()));
			resourceRepository.add(model);
		} catch (DataAccessException e) {
			throw new InternalServerErrorException(e.getMessage());
		} catch (BadRequestException e) {
			redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
			return redirectError;
		}
		return redirectSuccess;
	}

	/**
	 * Used to populate a model with a resource model.
	 * 
	 * @param id
	 *            the resource ID.
	 * @param model
	 *            the model.
	 */
	private void addResourceToModel(int id, Model model) {
		T resource = resourceRepository.findById(id);
		model.addAttribute("resource", resource);
	}

	/**
	 * Executed when a GET request to a specific resource ID is made to this
	 * resource controller.
	 * 
	 * @param id
	 *            the resource ID parameter value.
	 * @param model
	 *            the view model.
	 * @return the resource name view string.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getId(@PathVariable int id, Model model) {
		addResourceToModel(id, model);
		return RESOURCE_TEMPLATE;
	}

	/**
	 * Executed when a GET request to a specific resource ID's edit path is made
	 * to this resource controller.
	 * 
	 * @param id
	 *            the resource ID parameter value.
	 * @param model
	 *            the view model.
	 * @return the resource edit view string.
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
		if (!canEdit())
			throw new InternalServerErrorException("Editing " + resourceName
					+ " is unsupported.");

		addResourceToModel(id, model);
		return RESOURCE_EDIT_TEMPLATE;
	}

	/**
	 * Executed when a POST request to a specific resource ID's edit path is
	 * made to this resource controller.
	 * 
	 * @param model
	 *            the view model.
	 * @param result
	 *            the result of the binding.
	 * @param redirectAttrs
	 *            the redirect attributes.
	 * @return the resulting redirect string.
	 */
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
	public String edit(@ModelAttribute("resource") T model,
			BindingResult result, RedirectAttributes redirectAttrs) {
		if (!canEdit())
			throw new InternalServerErrorException("Editing " + resourceName
					+ " is unsupported.");

		try {
			log.info(String
					.format("(Username = %s) Updating resource %s ID %d ('%s') from %s",
							getUser().getUsername(), resourceName,
							model.getId(), model.toString(),
							resourceRepository.toString()));
			resourceRepository.update(model);
		} catch (DataAccessException e) {
			throw new InternalServerErrorException(e.getMessage());
		} catch (BadRequestException e) {
			redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
			return redirectEditError(model.getId());
		}
		return redirectEditSuccess(model.getId());
	}

	/**
	 * Form the edit success redirect string using the resource ID.
	 * 
	 * @param id
	 *            the resource ID.
	 * @return the edit success redirect string.
	 */
	private String redirectEditSuccess(int id) {
		return String.format(REDIRECT_EDIT_SUCCESS, pageName, id);
	}

	/**
	 * Form the edit error redirect string using the resource ID.
	 * 
	 * @param id
	 *            the resource ID.
	 * @return the edit error redirect string.
	 */
	private String redirectEditError(int id) {
		return String.format(REDIRECT_EDIT_ERROR, pageName, id);
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public String delete(@PathVariable int id, RedirectAttributes redirectAttrs) {
		if (!canDelete())
			throw new InternalServerErrorException("Deleting " + resourceName
					+ " is unsupported.");

		try {
			log.info(String.format(
					"(Username = %s) Removing resource %s ID %d from %s",
					getUser().getUsername(), resourceName, id,
					resourceRepository.toString()));
			resourceRepository.remove(id);
		} catch (DataAccessException e) {
			throw new InternalServerErrorException(e.getMessage());
		} catch (BadRequestException e) {
			redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
			return redirectError;
		}
		return redirectSuccess;
	}

	/**
	 * Returns the list of all resources in the repository.
	 */
	@ModelAttribute("resources")
	public List<T> getResources() {
		return resourceRepository.findAll();
	}

	/**
	 * Returns a new resource.
	 */
	@ModelAttribute("newResource")
	public abstract T getNewResource();

	/**
	 * Returns the resource name.
	 */
	@ModelAttribute("resourceName")
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * Returns whether or not this resource supports new/adding.
	 */
	@ModelAttribute("canNew")
	public boolean canNew() {
		return true;
	}

	/**
	 * Returns whether or not this resource supports editing.
	 */
	@ModelAttribute("canEdit")
	public boolean canEdit() {
		return true;
	}

	/**
	 * Returns whether or not this resource supports deleting.
	 */
	@ModelAttribute("canDelete")
	public boolean canDelete() {
		return true;
	}

}
