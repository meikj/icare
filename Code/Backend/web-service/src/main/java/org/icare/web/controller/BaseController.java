package org.icare.web.controller;

import java.util.List;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.model.Client;
import org.icare.shared.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This is a bare minimal abstract implementation of a controller in the system.
 * This provides view models with useful information, as well as basic GET
 * support.
 * 
 * @author John Meikle
 *
 */
public abstract class BaseController {

	@Autowired
	protected ResourceRepository<Client> clientRepository;
	protected final String pageName;

	public BaseController(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * Executed when a GET request is made to this controller.
	 * 
	 * @return the page name view string.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String get() {
		return pageName;
	}

	/**
	 * Returns the page name.
	 */
	@ModelAttribute("page")
	public String getPageName() {
		return pageName;
	}

	/**
	 * Returns the authenticated user model.
	 */
	@ModelAttribute("user")
	public User getUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication()
				.getDetails();
	}

	/**
	 * Returns the list of all clients from the repository.
	 */
	@ModelAttribute("clients")
	public List<Client> getClients() {
		return clientRepository.findAll();
	}

	/**
	 * Returns the model of the selected client. Returns null if all clients are
	 * selected.
	 */
	@ModelAttribute("selectedClient")
	public Client getSelectedClient() {
		int selectedClientId = getUser().getSelectedClientId();

		if (selectedClientId > 0) {
			try {
				return clientRepository.findById(selectedClientId);
			} catch (Exception e) {
				getUser().setSelectedClientId(0);
			}
		}

		return null;
	}

}
