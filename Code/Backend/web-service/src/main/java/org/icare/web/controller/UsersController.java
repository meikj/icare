package org.icare.web.controller;

import org.icare.shared.model.User;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/users")
@Secured("ROLE_ADMIN")
public class UsersController extends ResourceController<User> {
	
	public static final String PAGE_NAME = "users";
	public static final String RESOURCE_NAME = "user";

	public UsersController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(User model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getNewResource() {
		return new User();
	}
	
	@Override
	@Secured("ROLE_ADMIN")
	public String get() {
		return super.get();
	}
	
	@Override
	@Secured("ROLE_ADMIN")
	public String add(@ModelAttribute("resource") User model,
			BindingResult result, RedirectAttributes redirectAttrs) {
		return super.add(model, result, redirectAttrs);
	}
	
	@Override
	@Secured("ROLE_ADMIN")
	public String delete(@PathVariable int id, RedirectAttributes redirectAttrs) {
		return super.delete(id, redirectAttrs);
	}
	
}
