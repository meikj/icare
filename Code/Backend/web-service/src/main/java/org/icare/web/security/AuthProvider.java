package org.icare.web.security;

import org.icare.shared.data.UserRepository;
import org.icare.shared.exception.UnauthorisedAccessException;
import org.icare.shared.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;

/**
 * This implements the AuthenticationProvider interface using a UserRepository.
 * 
 * @author John Meikle
 *
 */
public class AuthProvider implements AuthenticationProvider {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Authentication authenticate(Authentication authentication) {
		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();
		
		try {
			User user = userRepository.findByCredentials(username, password);
			if (!user.isEnabled()) {
				authentication.setAuthenticated(false);
				throw new UnauthorisedAccessException("Account disabled.");
			}
			
			AuthUser authUser = new AuthUser(user);
			return authUser;
		} catch (Exception e) {
			authentication.setAuthenticated(false);
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}
