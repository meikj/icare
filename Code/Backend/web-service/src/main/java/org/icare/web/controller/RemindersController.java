package org.icare.web.controller;

import org.icare.shared.model.Reminder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This is an implementation for a Reminder resource controller. This controls
 * the reminders section of the web service.
 * 
 * @author John Meikle
 *
 */
@Controller
@RequestMapping("/reminders")
public class RemindersController extends ResourceController<Reminder> {

	public static final String PAGE_NAME = "reminders";
	public static final String RESOURCE_NAME = "reminder";

	public RemindersController() {
		super(PAGE_NAME, RESOURCE_NAME);
	}

	@Override
	public void beforeAdd(Reminder model) {
		// no need to do anything
	}

	@Override
	public Reminder getNewResource() {
		return new Reminder();
	}

}
