package org.icare.web.service;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * This is a service class for QR code image generation.
 * 
 * @author John Meikle
 *
 */
@Service
public class QRCodeImageService {

	public static final int SIZE = 250;

	/**
	 * Generates a QR code using the encode string and stores it in a
	 * BufferedImage.
	 * 
	 * @param encodeString
	 *            the encode string.
	 * @return the BufferedImage.
	 * @throws WriterException
	 *             if a problem occurs when encoding the string.
	 */
	public BufferedImage generateBufferedImage(String encodeString)
			throws WriterException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(encodeString,
				BarcodeFormat.QR_CODE, SIZE, SIZE);
		BufferedImage image = new BufferedImage(bitMatrix.getWidth(),
				bitMatrix.getHeight(), BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, bitMatrix.getWidth(), bitMatrix.getHeight());
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < bitMatrix.getWidth(); i++) {
			for (int j = 0; j < bitMatrix.getHeight(); j++) {
				if (bitMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}

		return image;
	}

}
