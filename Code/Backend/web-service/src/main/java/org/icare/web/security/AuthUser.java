package org.icare.web.security;

import java.util.Collection;
import java.util.List;

import org.icare.shared.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * This provides an implementation of Authentication with support for a user.
 * 
 * @author John Meikle
 *
 */
public class AuthUser implements Authentication {

	private static final long serialVersionUID = 2478562734475800447L;

	private List<GrantedAuthority> authorities;
	private boolean authenticated;
	private User user;

	/**
	 * Construct a new AuthUser using a user model and authorities.
	 * 
	 * @param user
	 *            the user model.
	 */
	public AuthUser(User user) {
		this.user = user;
		this.authorities = AuthorityUtils.createAuthorityList(user.getAuthority());
		authenticated = true;
	}

	/**
	 * Returns the users username.
	 */
	@Override
	public String getName() {
		return user.getUsername();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/**
	 * Returns null.
	 */
	@Override
	public Object getCredentials() {
		return null;
	}

	/**
	 * Returns the user model.
	 */
	@Override
	public Object getDetails() {
		return user;
	}

	/**
	 * Returns the users username.
	 */
	@Override
	public Object getPrincipal() {
		return user.getUsername();
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	/**
	 * Cannot be set to true (but is true by default), only false. Set to false
	 * if authentication fails.
	 */
	@Override
	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		if (isAuthenticated) {
			throw new IllegalArgumentException(
					"setAuthenticated(true) disallowed");
		} else {
			authenticated = false;
		}
	}

}
