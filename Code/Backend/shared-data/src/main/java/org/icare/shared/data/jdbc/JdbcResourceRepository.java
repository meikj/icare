package org.icare.shared.data.jdbc;

import java.util.ArrayList;
import java.util.List;

import org.icare.shared.data.AuthorityFilter;
import org.icare.shared.data.AuthorityFilterImpl;
import org.icare.shared.data.QueryConstructor;
import org.icare.shared.data.QueryFilter;
import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.exception.ResourceNotFoundException;
import org.icare.shared.exception.UnauthorisedAccessException;
import org.icare.shared.model.Client;
import org.icare.shared.model.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;

/**
 * This is a general abstract implementation for JDBC resource repositories.
 * 
 * @author John Meikle
 *
 * @param <T>
 *            the type of resource.
 */
public abstract class JdbcResourceRepository<T extends Resource> implements ResourceRepository<T> {

	protected String table;
	protected String selectQuery;
	protected String selectByIdQuery;
	protected String selectByClientIdQuery;
	protected String removeByIdQuery;
	protected RowMapper<T> rowMapper;
	protected ResourceRepository<Client> clientRepository;
	protected JdbcOperations jdbcOperations;
	protected QueryConstructor queryConstructor;
	protected AuthorityFilter authorityFilter;

	/**
	 * Construct a new JdbcResourceRepository using a JdbcOperations object for
	 * querying a data source, the table name, and an appropriate RowMapper.
	 * 
	 * By default, this uses an AuthorityFilterImpl for authority filtering.
	 * 
	 * @param jdbcOperations
	 *            an appropriate implementation of JdbcOperations.
	 * @param table
	 *            the table name for querying purposes.
	 * @param rowMapper
	 *            an appropriate RowMapper for mapping rows to objects.
	 */
	public JdbcResourceRepository(JdbcOperations jdbcOperations, String table,
			RowMapper<T> rowMapper) {
		this.jdbcOperations = jdbcOperations;
		this.table = table;
		this.rowMapper = rowMapper;
		this.authorityFilter = new AuthorityFilterImpl();
		initQueries();
	}

	/**
	 * Construct a new JdbcResourceRepository using a JdbcOperations object for
	 * querying a data source, a client repository, the table name, and an
	 * appropriate RowMapper.
	 * 
	 * By default, this uses an AuthorityFilterImpl for authority filtering.
	 * 
	 * @param jdbcOperations
	 *            an appropriate implementation of JdbcOperations.
	 * @param clientRepository
	 *            the client repository instance to use.
	 * @param table
	 *            the table name for querying purposes.
	 * @param rowMapper
	 *            an appropriate RowMapper for mapping rows to objects.
	 */
	public JdbcResourceRepository(JdbcOperations jdbcOperations,
			ResourceRepository<Client> clientRepository, String table,
			RowMapper<T> rowMapper) {
		this.jdbcOperations = jdbcOperations;
		this.clientRepository = clientRepository;
		this.table = table;
		this.rowMapper = rowMapper;
		this.authorityFilter = new AuthorityFilterImpl();
		initQueries();
	}

	/**
	 * Initialise the query strings for this repository using the table field.
	 * This also initialises the QueryConstructor.
	 */
	private void initQueries() {
		queryConstructor = new QueryConstructor(table);
		
		QueryFilter filterById = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "id");
		QueryFilter filterByClientId = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "client_id");
		QueryFilter filterLimit = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_LIMIT, 1);
		
		selectQuery = queryConstructor.baseQuerySelect();
		selectByIdQuery = QueryConstructor.constructWithFilters(selectQuery, filterById);
		selectByClientIdQuery = QueryConstructor.constructWithFilters(selectQuery, filterByClientId);
		removeByIdQuery = QueryConstructor.constructWithFilters(queryConstructor.baseQueryDelete(), filterById, filterLimit);
	}

	/**
	 * Get the associated RowMapper for this repository.
	 */
	public RowMapper<T> getRowMapper() {
		return rowMapper;
	}

	/**
	 * Get the associated QueryConstructor for this repository.
	 */
	public QueryConstructor getQueryConstructor() {
		return queryConstructor;
	}
	
	/**
	 * Get the AuthorityFilter in use by this repository.
	 */
	public AuthorityFilter getAuthorityFilter() {
		return authorityFilter;
	}
	
	/**
	 * Set the AuthorityFilter used by this repository.
	 */
	public void setAuthorityFilter(AuthorityFilter authorityFilter) {
		this.authorityFilter = authorityFilter;
	}

	/**
	 * This method is used to validate the resource model before inserting or
	 * updating.
	 * 
	 * @param model
	 *            the resource model.
	 * @return whether or not the resource model is valid.
	 */
	public abstract void validate(T model) throws BadRequestException;

	/**
	 * This method specifies the operations that should be performed (e.g. JDBC
	 * insert query) when adding a resource.
	 * 
	 * @param model
	 *            the resource model for your purposes.
	 * @param keyHolder
	 *            the key holder for storing the inserted resource ID.
	 * @throws DataAccessException
	 *             if a problem occurs when adding the data.
	 */
	protected abstract void doAdd(T model, GeneratedKeyHolder keyHolder)
			throws DataAccessException;

	@Override
	public T add(T model) throws DataAccessException,
			UnauthorisedAccessException, BadRequestException {
		// This will throw BadRequestException with appropriate message if
		// invalid
		validate(model);

		// This is used to get the resulting key from insertion
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

		if (authorityFilter.isCarer() || authorityFilter.isAdmin()) {
			int clientId = authorityFilter.getUser().getSelectedClientId();

			if (clientRepository != null && clientId == 0) {
				// Add to all clients
				List<Client> clients = clientRepository.findAll();
				for (Client client : clients) {
					model.setClientId(client.getId());
					doAdd(model, keyHolder);
				}
			} else {
				// Add to single client
				Client client = clientRepository.findById(clientId);
				model.setClientId(client.getId());
				doAdd(model, keyHolder);
			}
		} else if (authorityFilter.isClient()) {
			model.setClientId(authorityFilter.getClient().getId());
			doAdd(model, keyHolder);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to add resource.");
		}

		// Ensure a key is held, so that the new resource can be returned
		if (keyHolder.getKey() != null)
			return findById(keyHolder.getKey().intValue());
		else
			throw new BadRequestException(
					"Can't add resource. Please ensure a client is selected.");
	}

	@Override
	public List<T> findAll() throws DataAccessException,
			UnauthorisedAccessException {
		if (authorityFilter.isClient()) {
			return jdbcOperations.query(selectByClientIdQuery, rowMapper,
					authorityFilter.getClient().getId());
		} else if (authorityFilter.isCarer()) {
			String query = QueryConstructor.constructWithFilters(selectQuery, filterOnlyMyClients());
			return jdbcOperations.query(query, rowMapper);
		} else if (authorityFilter.isAdmin()) {
			return jdbcOperations.query(selectQuery, rowMapper);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view all resources.");
		}
	}
	
	@Override
	public List<T> findAll(QueryFilter... filters) {
		if (authorityFilter.isClient()) {
			String query = QueryConstructor.constructWithFilters(selectByClientIdQuery, filters);
			return jdbcOperations.query(query, rowMapper, authorityFilter.getClient().getId());
		} else if (authorityFilter.isCarer()) {
			String query = QueryConstructor.constructWithFilters(selectQuery, filters);
			query = QueryConstructor.constructWithFilters(query, filterOnlyMyClients());
			return jdbcOperations.query(query, rowMapper);
		} else if (authorityFilter.isAdmin()) {
			String query = QueryConstructor.constructWithFilters(selectQuery, filters);
			return jdbcOperations.query(query, rowMapper);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view all resources.");
		}
	}

	@Override
	public T findById(int id) throws DataAccessException,
			UnauthorisedAccessException, ResourceNotFoundException {
		T resource;

		// Ensure resource exists first
		try {
			resource = jdbcOperations.queryForObject(selectByIdQuery,
					rowMapper, id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException(
					"No resource can be found by that ID: " + id);
		}

		if (authorityFilter.isClient()) {
			// Ensure the requesting client matches the resource client
			if (authorityFilter.getClient().getId() == resource.getClientId()) {
				return resource;
			} else {
				throw new UnauthorisedAccessException(
						"Client is not authorised to view this resource.");
			}
		} else if (authorityFilter.isCarer()) {
			// Ensure the requesting carer matches the resource clients carer
			Client client = clientRepository.findById(resource.getClientId());
			if (authorityFilter.getUser().getId() == client.getUserId()) {
				return resource;
			} else {
				throw new UnauthorisedAccessException(
						"Carer is not authorised to view this resource.");
			}
		} else if (authorityFilter.isAdmin()) {
			// Admin can access all resources
			return resource;
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view this resource.");
		}
	}

	@Override
	public void remove(int id) throws DataAccessException,
			UnauthorisedAccessException, ResourceNotFoundException {
		T resource = findById(id);
		if (authorityFilter.isClient()) {
			// Ensure the requesting client matches the resource client
			if (resource.getClientId() == authorityFilter.getClient().getId()) {
				jdbcOperations.update(removeByIdQuery, id);
			} else {
				throw new UnauthorisedAccessException(
						"Client is not authorised to remove this resource.");
			}
		} else if (authorityFilter.isCarer()) {
			// Ensure the requesting carer matches the resource clients carer
			Client client = clientRepository.findById(resource.getClientId());
			if (authorityFilter.getUser().getId() == client.getUserId()) {
				jdbcOperations.update(removeByIdQuery, id);
			} else {
				throw new UnauthorisedAccessException(
						"Carer is not authorised to remove this clients resource.");
			}
		} else if (authorityFilter.isAdmin()) {
			// Admin can access all resources
			jdbcOperations.update(removeByIdQuery, id);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to remove this resource.");
		}
	}

	/**
	 * This method specifies the operations that should be performed (e.g. JDBC
	 * insert query) when updating a resource.
	 * 
	 * @param model
	 *            the resource model for your purposes.
	 * @throws DataAccessException
	 *             if a problem occurs when updating the data.
	 */
	protected abstract void doUpdate(T model) throws DataAccessException;

	@Override
	public T update(T model) throws DataAccessException,
			UnauthorisedAccessException, BadRequestException {
		// This will throw BadRequestException with appropriate message if
		// invalid
		validate(model);

		if (authorityFilter.isClient()) {
			// Ensure the requesting client matches the resource client
			if (model.getClientId() == authorityFilter.getClient().getId()) {
				doUpdate(model);
			} else {
				throw new UnauthorisedAccessException(
						"Client is not authorised to update this resource.");
			}
		} else if (authorityFilter.isCarer()) {
			// Ensure the requesting carer matches the resource clients carer
			Client client = clientRepository.findById(model.getClientId());
			if (authorityFilter.getUser().getId() == client.getUserId()) {
				doUpdate(model);
			} else {
				throw new UnauthorisedAccessException(
						"Carer is not authorised to update this clients resource.");
			}
		} else if (authorityFilter.isAdmin()) {
			// Admin can access all resources
			doUpdate(model);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to update this resource.");
		}

		return findById(model.getId());
	}
	
	/**
	 * Returns a query filtering for filtering only the clients present in the
	 * client repository. For instance, for a carer, this will filter only the
	 * clients under their 'control'.
	 * 
	 * For example: "... where client_id = 1 or client_id = 2 or client_id = 3..."
	 * 
	 * @return the QueryFilter.
	 */
	private QueryFilter filterOnlyMyClients() {
		List<Object> clientIds = new ArrayList<>();
		List<Client> clients = clientRepository.findAll();
		
		if (clients.size() == 0)
			return null;
		
		for (int i = 0; i < clients.size(); i++) {
			clientIds.add("client_id");
			if (i < (clients.size() - 1))
				clientIds.add("or");
		}
		
		QueryFilter filter = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, clientIds.toArray());
		
		for (Client c : clients)
			filter.addParamValue(c.getId());
		
		return filter;
	}

}
