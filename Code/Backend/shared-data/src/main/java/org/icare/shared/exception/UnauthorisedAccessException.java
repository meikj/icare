package org.icare.shared.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is for when access is unauthorised, resulting in a HTTP 401
 * response.
 * 
 * @author John Meikle
 *
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorisedAccessException extends RuntimeException {

	private static final long serialVersionUID = -1912508826702268896L;

	public UnauthorisedAccessException() {
		super();
	}

	public UnauthorisedAccessException(String message) {
		super(message);
	}

}
