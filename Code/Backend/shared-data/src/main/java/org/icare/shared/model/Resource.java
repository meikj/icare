package org.icare.shared.model;

import java.io.Serializable;

/**
 * Represents a general resource with properties for resource ID and associated
 * client ID.
 * 
 * @author John Meikle
 *
 */
public abstract class Resource implements Serializable {

	private static final long serialVersionUID = -7736696170508296156L;
	
	protected int id;
	protected int clientId;

	public Resource() {
		// for construction via setters
	}

	public Resource(int id, int clientId) {
		this.id = id;
		this.clientId = clientId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Resource))
			return false;
		if (obj == this)
			return true;

		Resource res = (Resource) obj;
		return res.getId() == id;
	}

	@Override
	public int hashCode() {
		return id;
	}

}
