package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.icare.shared.data.ClientRepository;
import org.icare.shared.data.QueryConstructor;
import org.icare.shared.data.QueryFilter;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.exception.ResourceNotFoundException;
import org.icare.shared.exception.UnauthorisedAccessException;
import org.icare.shared.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for a Client resource repository as well as an
 * implementation for the specialised ClientRepository interface. This
 * repository relies on an appropriate JdbcOperations object for querying.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcClientRepository extends JdbcResourceRepository<Client>
		implements ClientRepository {

	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "clients";
	
	/**
	 * This represents the columns in the table for inserting and updating.
	 */
	public static final String[] TABLE_COLUMNS = new String[]{
		"first_name",
		"last_name",
		"dob",
		"address",
		"phone",
		"email",
		"auth_token",
		"user_id"
	};
	
	private String insertQuery;
	private String updateQuery;
	private String selectByAuthTokenQuery;
	private String selectByCarerQuery;
	
	@Autowired
	public JdbcClientRepository(JdbcOperations jdbcOperations) {
		super(jdbcOperations, TABLE_NAME, new ClientRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS);
		updateQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS);
		QueryFilter filterAuth = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "auth_token");
		QueryFilter filterLimit = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_LIMIT, 1);
		selectByAuthTokenQuery = QueryConstructor.constructWithFilters(queryConstructor.baseQuerySelect(), filterAuth, filterLimit);
		QueryFilter filterByCarer = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "user_id");
		selectByCarerQuery = QueryConstructor.constructWithFilters(queryConstructor.baseQuerySelect(), filterByCarer);
	}
	
	@Override
	public Client add(Client client) {
		// This will throw BadRequestException with appropriate message if
		// invalid
		validate(client);

		// This is used to get the resulting key from insertion
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

		if (authorityFilter.isCarer() || authorityFilter.isAdmin()) {
			client.setUserId(getAuthorityFilter().getUser().getId());
			doAdd(client, keyHolder);
		} else {
			throw new UnauthorisedAccessException("Not authorised to add a client.");
		}

		// Ensure a key is held, so that the new resource can be returned
		if (keyHolder.getKey() != null)
			return findById(keyHolder.getKey().intValue());
		else
			throw new BadRequestException(
					"Adding client potentially failed -- no key was returned.");
	}
	
	@Override
	public List<Client> findAll() {
		if (authorityFilter.isCarer()) {
			return jdbcOperations.query(selectByCarerQuery, getRowMapper(),
					authorityFilter.getUser().getId());
		}
		return super.findAll();
	}
	
	@Override
	public List<Client> findAll(QueryFilter... filters) {
		if (authorityFilter.isCarer()) {
			String query = QueryConstructor.constructWithFilters(selectByCarerQuery, filters);
			return jdbcOperations.query(query, getRowMapper(), authorityFilter.getUser().getId());
		}
		return super.findAll(filters);
	}
	
	@Override
	public Client findById(int id) {
		Client client;

		// Ensure resource exists first
		try {
			client = jdbcOperations.queryForObject(selectByIdQuery,
					getRowMapper(), id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException(
					"No client can be found by that ID: " + id);
		}

		if (authorityFilter.isClient()) {
			// Ensure the requesting client matches the resource client
			if (authorityFilter.getClient().getId() == client.getId()) {
				return client;
			} else {
				throw new UnauthorisedAccessException(
						"Client is not authorised to view this client.");
			}
		} else if (authorityFilter.isCarer()) {
			// Ensure the requesting carer matches the resource clients carer
			if (authorityFilter.getUser().getId() == client.getUserId()) {
				return client;
			} else {
				throw new UnauthorisedAccessException(
						"Carer is not authorised to view this client.");
			}
		} else if (authorityFilter.isAdmin()) {
			// Admin can access all resources
			return client;
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view this client.");
		}
	}
	
	@Override
	public void remove(int id) {
		Client client = findById(id);
		
		if (authorityFilter.isCarer()) {
			if (authorityFilter.getUser().getId() == client.getUserId()) {
				jdbcOperations.update(removeByIdQuery, id);
			} else {
				throw new UnauthorisedAccessException(
						"Carer is not authorised to remove this client.");
			}
		} else if (authorityFilter.isAdmin()) {
			// Admin can access all resources
			jdbcOperations.update(removeByIdQuery, id);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to remove this client.");
		}
	}
	
	@Override
	public Client update(Client client) {
		// This will throw BadRequestException with appropriate message if
		// invalid
		validate(client);

		if (authorityFilter.isClient()) {
			// Ensure the requesting client matches the resource client
			if (client.getId() == authorityFilter.getClient().getId()) {
				doUpdate(client);
			} else {
				throw new UnauthorisedAccessException(
						"Client is not authorised to update this client.");
			}
		} else if (authorityFilter.isCarer()) {
			if (authorityFilter.getUser().getId() == client.getUserId()) {
				doUpdate(client);
			} else {
				throw new UnauthorisedAccessException(
						"Carer is not authorised to update this client.");
			}
		} else if (authorityFilter.isAdmin()) {
			// Admin can access all resources
			doUpdate(client);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to update this client.");
		}

		return findById(client.getId());
	}

	@Override
	public void validate(Client client) throws BadRequestException {
		if (client.getFirstName() == null || client.getFirstName().isEmpty()) {
			throw new BadRequestException(
					"Client first name missing or invalid.");
		} else if (client.getLastName() == null
				|| client.getLastName().isEmpty()) {
			throw new BadRequestException(
					"Client last name missing or invalid.");
		} else if (client.getAddress() == null || client.getAddress().isEmpty()) {
			throw new BadRequestException("Client address missing or invalid.");
		} else if (client.getDob() == null) {
			throw new BadRequestException(
					"Client date of birth missing or invalid.");
		} else if (client.getPhone() == null || client.getPhone().isEmpty()) {
			throw new BadRequestException(
					"Client phone number missing or invalid.");
		} else if (client.getAuthToken() == null
				|| client.getAuthToken().isEmpty()) {
			throw new BadRequestException(
					"Client auth token missing or invalid.");
		} else if (client.getUserId() <= 0) {
			throw new BadRequestException("Client user id invalid.");
		}
	}

	@Override
	protected void doAdd(Client client, GeneratedKeyHolder keyHolder) {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, client.getFirstName());
				ps.setString(2, client.getLastName());
				ps.setDate(3, new Date(client.getDob().getTime()));
				ps.setString(4, client.getAddress());
				ps.setString(5, client.getPhone());
				ps.setString(6, client.getEmail());
				ps.setString(7, client.getAuthToken());
				ps.setInt(8, client.getUserId());
				return ps;
			}

		}, keyHolder);
	}

	@Override
	protected void doUpdate(Client client) throws DataAccessException {
		jdbcOperations.update(updateQuery,
				client.getFirstName(),
				client.getLastName(),
				client.getDob(),
				client.getAddress(),
				client.getPhone(),
				client.getEmail(),
				client.getAuthToken(),
				client.getUserId(),
				client.getId());
	}

	@Override
	public Client findByAuthToken(String authToken) throws DataAccessException,
			ResourceNotFoundException {
		try {
			return jdbcOperations.queryForObject(selectByAuthTokenQuery,
					new ClientRowMapper(), authToken);
		} catch (IncorrectResultSizeDataAccessException e) {
			throw new ResourceNotFoundException(
					"No client can be found by the specified auth token.");
		}
	}

	/**
	 * Helper class for mapping Rows to Clients.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class ClientRowMapper implements RowMapper<Client> {

		@Override
		public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Client(
					rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getDate("dob"),
					rs.getString("address"),
					rs.getString("phone"),
					rs.getString("email"),
					rs.getString("auth_token"),
					rs.getInt("user_id"));
		}

	}

}
