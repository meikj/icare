package org.icare.shared.security;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Provides methods for client authentication tokens.
 * 
 * @author John Meikle
 *
 */
public class AuthTokenProvider {
	
	private SecureRandom rand = new SecureRandom();
	
	/**
	 * Generates a random authentication token string.
	 * 
	 * "This works by choosing 130 bits from a crytographically secure random
	 *  bit generator, and encoding them in base-32."
	 *  
	 * Taken from: http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
	 * 
	 * @return the random authentication token.
	 */
	public String generateToken() {
		return new BigInteger(130, rand).toString(32);
	}

}
