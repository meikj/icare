package org.icare.shared.data;

import java.util.List;

/**
 * This class represents a query filter.
 * 
 * @author John Meikle
 *
 */
public class QueryFilter {
	
	private String origFilter;
	private String filter;

	/**
	 * Construct a new QueryFilter using a filter string.
	 * 
	 * @param filter the filter string.
	 */
	public QueryFilter(String filter) {
		this.origFilter = new String(filter);
		this.filter = filter;
	}

	/**
	 * Add a parameter value to the next '?' in filter string.
	 * 
	 * @param value the parameter value.
	 */
	public void addParamValue(Object value) {
		if (filter.contains("?"))
			if (value instanceof Integer)
				filter = filter.replaceFirst("[?]", Integer.toString((Integer) value));
			else
				filter = filter.replaceFirst("[?]", String.format("'%s'", value.toString()));
		else
			throw new IllegalStateException("Filter string does not have any replacable '?'.");
	}

	/**
	 * Add a parameter value to the specified param '?' (e.g. my_param = ?, ...)
	 * in filter string.
	 * 
	 * @param the parameter name/key.
	 * @param value the parameter value.
	 */
	public void addParamValue(String param, String value) {

	}

	/**
	 * Get the list of parameter names/keys in the filter string.
	 */
	public List<String> getParams() {
		return null;
	}
	
	/**
	 * Get the original filter string used to construct this QueryFilter.
	 */
	public String getOriginalFilter() {
		return origFilter;
	}
	
	@Override
	public String toString() {
		return filter;
	}
	
}
