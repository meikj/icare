package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Alert;
import org.icare.shared.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for an Alert resource repository. This
 * repository relies on an appropriate JdbcOperations object for querying.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcAlertsRepository extends JdbcResourceRepository<Alert> {

	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "alerts";
	
	public static final String FILTER_WHERE_UNACK = "where ack = 0";
	public static final String FILTER_ORDER_BY_NEW = "order by id desc";
	
	/**
	 * This represents the columns in the table for inserting.
	 */
	public static final String[] TABLE_COLUMNS_INSERT = new String[]{
		"type",
		"coords",
		"ack",
		"datetime",
		"client_id",
	};
	
	/**
	 * This represents the columns in the table for updating.
	 */
	public static final String[] TABLE_COLUMNS_UPDATE = new String[]{ "ack" };
	
	private String insertQuery;
	private String updateQuery;

	/**
	 * Construct a new AlertsRepository using a JdbcOperations object for
	 * querying.
	 * 
	 * @param jdbcOperations
	 *            the JdbcOperations object.
	 */
	@Autowired
	public JdbcAlertsRepository(JdbcOperations jdbcOperations,
			ResourceRepository<Client> clientRepository) {
		super(jdbcOperations, clientRepository, TABLE_NAME, new AlertRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS_INSERT);
		updateQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS_UPDATE);
	}

	@Override
	public void validate(Alert alert) throws BadRequestException {
		if (alert.getType() == null || alert.getType().isEmpty()) {
			throw new BadRequestException("Alert type missing or invalid.");
		} else if (alert.getCoords() == null || alert.getCoords().isEmpty()) {
			throw new BadRequestException("Alert coords missing or invalid.");
		}
	}

	@Override
	protected void doAdd(Alert alert, GeneratedKeyHolder keyHolder) {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, alert.getType());
				ps.setString(2, alert.getCoords());
				ps.setBoolean(3, false);
				ps.setTimestamp(4, new Timestamp(new Date().getTime()));
				ps.setInt(5, alert.getClientId());
				return ps;
			}

		}, keyHolder);
	}

	@Override
	public void doUpdate(Alert alert) {
		jdbcOperations.update(updateQuery,
				alert.isAck(),
				alert.getId());
	}

	/**
	 * Helper class for mapping Rows to Alerts.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class AlertRowMapper implements RowMapper<Alert> {

		@Override
		public Alert mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Alert(
					rs.getInt("id"),
					rs.getString("type"),
					rs.getString("coords"),
					rs.getBoolean("ack"),
					rs.getTimestamp("datetime"),
					rs.getInt("client_id"));
		}

	}

}
