package org.icare.shared.data;

import org.icare.shared.model.Client;
import org.icare.shared.model.User;

/**
 * Provides an interface for filtering a repository on the basis of authority.
 * There are methods for retrieving currently authorised users as models for
 * accessing information about them.
 * 
 * @author John Meikle
 *
 */
public interface AuthorityFilter {
	
	/**
	 * Check if the current authorised user is a carer.
	 */
	public boolean isCarer();
	
	/**
	 * Check if the current authorised user is a client.
	 */
	public boolean isClient();
	
	/**
	 * Check if the current authorised user is an admin.
	 */
	public boolean isAdmin();
	
	/**
	 * Retrieve the currently authorised user (models both a carer and admin).
	 * 
	 * @precondition isCarer() || isAdmin()
	 */
	public User getUser();
	
	/**
	 * Retrieve the currently authorised client.
	 * 
	 * @precondition isClient()
	 */
	public Client getClient();

}
