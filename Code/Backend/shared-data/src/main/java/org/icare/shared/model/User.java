package org.icare.shared.model;

/**
 * Represents a user model. Provides properties for username, name, address,
 * phone, email and the selected client ID.
 * 
 * @author John Meikle
 *
 */
public class User extends Resource {

	private static final long serialVersionUID = -8134721460128077639L;
	
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String address;
	private String phone;
	private String email;
	private boolean enabled;
	private String authority;
	private int selectedClientId = 0;

	/**
	 * Construct an empty user model.
	 */
	public User() {
		// for construction via setters
	}

	/**
	 * Construct a new user model.
	 * 
	 * @param username
	 *            the username.
	 * @param password
	 *            the password.
	 * @param firstName
	 *            the first name.
	 * @param lastName
	 *            the last name.
	 * @param address
	 *            the address.
	 * @param phone
	 *            the phone number.
	 * @param email
	 *            the email.
	 * @param enabled
	 *            the enabled status.
	 * @param authority
	 *            the authority.
	 */
	public User(int id, String firstName, String lastName, String username, String password, 
			String email, String phone, boolean enabled, String address, String authority) {
		super(id, id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.enabled = enabled;
		this.address = address;
		this.authority = authority;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getAuthority() {
		return authority;
	}
	
	public void setAuthority(String authority) {
		this.authority = authority;
	}

	/**
	 * Retrieve the currently selected client ID.
	 * 
	 * @return the currently selected client ID, or 0 if all clients selected.
	 */
	public int getSelectedClientId() {
		return selectedClientId;
	}

	/**
	 * Set the currently selected client ID.
	 * 
	 * @param selectedClientId
	 *            the currently selected client ID, or 0 for all clients.
	 */
	public void setSelectedClientId(int selectedClientId) {
		this.selectedClientId = selectedClientId;
	}
	
	@Override
	public void setId(int id) {
		// keep ID and client ID in sync so they can be treated same
		super.setId(id);
		super.setClientId(id);
	}
	
    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

}
