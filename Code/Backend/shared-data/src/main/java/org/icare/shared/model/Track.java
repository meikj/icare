package org.icare.shared.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Represents a tracking model. Provides properties for id, coords, date and
 * time, and the associated client ID.
 * 
 * @author John Meikle
 *
 */
public class Track extends Resource {

	private static final long serialVersionUID = 7698767955702283180L;

	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	private String coords;

	@DateTimeFormat(pattern = DATE_TIME_PATTERN)
	private Date dateTime;

	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			DATE_TIME_PATTERN);

	public Track() {
		// for construction via getters setters
	}

	public Track(int id, String coords, Timestamp dateTimestamp, int clientId) {
		super(id, clientId);
		this.coords = coords;
		this.dateTime = new Date(dateTimestamp.getTime());
	}

	public Track(int id, String coords, Date dateTime, int clientId) {
		super(id, clientId);
		this.coords = coords;
		this.dateTime = dateTime;
	}

	public String getCoords() {
		return coords;
	}

	public void setCoords(String coords) {
		this.coords = coords;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public void setDateTimestamp(Timestamp dateTimestamp) {
		this.dateTime = new Date(dateTimestamp.getTime());
	}

	public String getDateTimeString() {
		return dateFormat.format(dateTime.getTime());
	}

}
