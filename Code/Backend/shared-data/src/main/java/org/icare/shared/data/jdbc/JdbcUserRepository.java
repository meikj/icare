package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

import org.icare.shared.data.AuthorityFilter;
import org.icare.shared.data.QueryConstructor;
import org.icare.shared.data.QueryFilter;
import org.icare.shared.data.UserRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.exception.ResourceNotFoundException;
import org.icare.shared.exception.UnauthorisedAccessException;
import org.icare.shared.model.Client;
import org.icare.shared.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for UserRepository. This extends a ResourceRepository
 * for add, update, find, and delete functionality. The AuthorityFilter is used to
 * ensure these operations can only be performed by an admin, as all aspects of user
 * management are admin only features.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcUserRepository extends JdbcResourceRepository<User> implements UserRepository {
	
	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "users";
	
	/**
	 * This represents the columns in the table for inserting and updating.
	 */
	public static final String[] TABLE_COLUMNS = new String[]{
		"first_name",
		"last_name",
		"username",
		"password",
		"email",
		"phone",
		"enabled",
		"address",
		"authority"
	};
	
	/**
	 * This represents the columns in the table for updating when no pass is
	 * supplied.
	 */
	public static final String[] TABLE_COLUMNS_NO_PASS = new String[]{
		"first_name",
		"last_name",
		"username",
		"email",
		"phone",
		"enabled",
		"address",
		"authority"
	};
	
	private String insertQuery;
	private String updateQuery;
	private String updateNoPassQuery;
	private boolean updateNoPass = false;

	/**
	 * Construct a new UserRepository using a JdbcOperations object for
	 * querying.
	 * 
	 * @param jdbcOperations
	 *            The JdbcOperations object.
	 */
	@Autowired
	public JdbcUserRepository(JdbcOperations jdbcOperations) {
		super(jdbcOperations, TABLE_NAME, new UserRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS);
		updateQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS);
		updateNoPassQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS_NO_PASS);
	}
	
	@Override
	public User add(User user) {
		// This will throw BadRequestException with appropriate message if
		// invalid
		validate(user);
		
		// This is used to get the resulting key from insertion
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		
		// Only admins can add users!
		if (authorityFilter.isAdmin()) {
			user.setEnabled(true);
			// TODO: Encrypt password before passing on to doAdd
			doAdd(user, keyHolder);
		} else {
			throw new UnauthorisedAccessException("Not authorised to add a user.");
		}
		
		// Ensure a key is held, so that the new resource can be returned
		if (keyHolder.getKey() != null)
			return findById(keyHolder.getKey().intValue());
		else
			throw new BadRequestException(
					"Adding user potentially failed -- no key was returned.");
	}
	
	@Override
	public void validate(User user) throws BadRequestException {
		if (user.getFirstName() == null || user.getFirstName().isEmpty()) {
			throw new BadRequestException(
					"User first name missing or invalid.");
		} else if (user.getLastName() == null
				|| user.getLastName().isEmpty()) {
			throw new BadRequestException(
					"User last name missing or invalid.");
		} else if (user.getUsername() == null || user.getUsername().isEmpty()) {
			throw new BadRequestException("User username missing or invalid.");
		} else if (!updateNoPass && (user.getPassword() == null || user.getPassword().isEmpty())) {
			throw new BadRequestException("User password missing or invalid.");
		} else if (user.getEmail() == null || user.getEmail().isEmpty()) {
			throw new BadRequestException("User email missing or invalid.");
		} else if (user.getPhone() == null || user.getPhone().isEmpty()) {
			throw new BadRequestException("User phone number missing or invalid.");
		} else if (user.getAddress() == null || user.getAddress().isEmpty()) {
			throw new BadRequestException("User address missing or invalid.");
		} else if (user.getPhone() == null || user.getPhone().isEmpty()) {
			throw new BadRequestException(
					"User phone number missing or invalid.");
		}
	}
	
	@Override
	protected void doAdd(User user, GeneratedKeyHolder keyHolder) {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, user.getFirstName());
				ps.setString(2, user.getLastName());
				ps.setString(3, user.getUsername());
				ps.setString(4, user.getPassword());
				ps.setString(5, user.getEmail());
				ps.setString(6, user.getPhone());
				ps.setBoolean(7, user.isEnabled());
				ps.setString(8, user.getAddress());
				ps.setString(9, user.getAuthority());
				return ps;
			}

		}, keyHolder);
	}
	
	@Override
	protected void doUpdate(User user) throws DataAccessException {
		if (updateNoPass) {
			updateNoPass = false;
			jdbcOperations.update(updateNoPassQuery,
					user.getFirstName(),
					user.getLastName(),
					user.getUsername(),
					user.getEmail(),
					user.getPhone(),
					user.isEnabled(),
					user.getAddress(),
					user.getAuthority(),
					user.getId());
		} else {
			jdbcOperations.update(updateQuery,
					user.getFirstName(),
					user.getLastName(),
					user.getUsername(),
					user.getPassword(),
					user.getEmail(),
					user.getPhone(),
					user.isEnabled(),
					user.getAddress(),
					user.getAuthority(),
					user.getId());
		}
	}

	@Override
	public List<User> findAll() throws DataAccessException,
			UnauthorisedAccessException {
		if (authorityFilter.isAdmin()) {
			return super.findAll();
		} else if (authorityFilter.isCarer()) {
			return Arrays.asList(findById(authorityFilter.getUser().getId()));
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view all users.");
		}
	}
	
	@Override
	public List<User> findAll(QueryFilter... filters) {
		if (authorityFilter.isAdmin()) {
			return super.findAll(filters);
		} else if (authorityFilter.isCarer()) {
			return Arrays.asList(findById(authorityFilter.getUser().getId()));
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view all users.");
		}
	}
	
	@Override
	public User findById(int id) {
		if (authorityFilter.isAdmin()) {
			return super.findById(id);
		} else if (authorityFilter.isCarer()) {
			if (authorityFilter.getUser().getId() == id) {
				return jdbcOperations.queryForObject(selectByIdQuery,
						rowMapper, id);
			} else {
				throw new UnauthorisedAccessException(
						"Not authorised to view this user.");
			}
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to view this user.");
		}
	}
	
	@Override
	public void remove(int id) {
		if (authorityFilter.isAdmin()) {
			super.remove(id);
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to remove this user.");
		}
	}
	
	@Override
	public User update(User user) {
		if (authorityFilter.isAdmin()) {
			if (user.getPassword() == null || user.getPassword().isEmpty())
				updateNoPass = true;
			return super.update(user);
		} else if (authorityFilter.isCarer()) {
			if (authorityFilter.getUser().getId() == user.getId()) {
				doUpdate(user);
				return findById(authorityFilter.getUser().getId());
			} else {
				throw new UnauthorisedAccessException(
						"Not authorised to update this user.");
			}
		} else {
			throw new UnauthorisedAccessException(
					"Not authorised to update this user.");
		}
	}
	
	@Override
	public User findByUsername(String username) throws DataAccessException,
			UnauthorisedAccessException, ResourceNotFoundException {
		QueryFilter filterUsername = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "username");
		QueryFilter filterLimit = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_LIMIT, 1);
		filterUsername.addParamValue(username);
		List<User> result = findAll(filterUsername, filterLimit);
		
		if (result.size() == 1)
			return result.get(0);
		else
			throw new ResourceNotFoundException("Could not find user: " + username);
	}

	@Override
	public User findByCredentials(String username, String password)
			throws DataAccessException, UnauthorisedAccessException,
			ResourceNotFoundException {
		AuthorityFilter origAuthFilter = getAuthorityFilter();
		// Temporarily bypass authority filter for credential check
		// Reason why is because at login page user has no authority
		setAuthorityFilter(new LoginAuthorityFilter());
		QueryFilter filterUsernamePassword = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "username", "and", "password");
		QueryFilter filterLimit = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_LIMIT, 1);
		filterUsernamePassword.addParamValue(username);
		filterUsernamePassword.addParamValue(password);
		List<User> result;
		
		try {
			 result = findAll(filterUsernamePassword, filterLimit);
		} finally {
			// Set authority filter back
			setAuthorityFilter(origAuthFilter);
		}
		
		if (result.size() == 1)
			return result.get(0);
		else
			throw new ResourceNotFoundException("Invalid credentials for user: " + username);
	}

	/**
	 * Helper class for mapping Rows to Users.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class UserRowMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new User(
					rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getString("username"),
					rs.getString("password"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getBoolean("enabled"),
					rs.getString("address"),
					rs.getString("authority"));
		}

	}
	
	/**
	 * Helper class used to temporarily switch the AuthorityFilter for logging
	 * in. This is needed because default AuthorityFilter imposes real security
	 * authority filtering. Before login the user has no authority.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class LoginAuthorityFilter implements AuthorityFilter {
		@Override
		public boolean isCarer() { return false; }
		
		@Override
		public boolean isClient() { return false; }
		
		@Override
		public boolean isAdmin() { return true; }
		
		@Override
		public User getUser() { return null; }
		
		@Override
		public Client getClient() { return null; }
	}

}
