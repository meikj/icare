package org.icare.shared.data;

import org.icare.shared.exception.ResourceNotFoundException;
import org.icare.shared.exception.UnauthorisedAccessException;
import org.icare.shared.model.User;
import org.springframework.dao.DataAccessException;

/**
 * Provides an interface for specialised user repository methods.
 * 
 * @author John Meikle
 *
 */
public interface UserRepository {

	/**
	 * Find a user by their username.
	 * 
	 * @param username
	 *            the username.
	 * @return the user model.
	 * @throws DataAccessException
	 *             if a problem occurs when finding the user.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 * @throws ResourceNotFoundException
	 *             if the user is not found by the specified username.
	 */
	public User findByUsername(String username) throws DataAccessException,
			UnauthorisedAccessException, ResourceNotFoundException;

	/**
	 * Find a user by their username and password credentials.
	 * 
	 * @param username
	 *            the username.
	 * @param password
	 *            the password.
	 * @return the user model.
	 * @throws DataAccessException
	 *             if a problem occurs when finding the user.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 * @throws ResourceNotFoundException
	 *             if the user is not found by the specified username.
	 */
	public User findByCredentials(String username, String password)
			throws DataAccessException, UnauthorisedAccessException,
			ResourceNotFoundException;

}
