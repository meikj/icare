package org.icare.shared.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a reminder model.
 * 
 * @author John Meikle
 *
 */
@JsonIgnoreProperties({"startDateTime"})
public class Reminder extends Resource {

	private static final long serialVersionUID = -539106661911136313L;

	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm";

	private String title;
	private String description;
	private String location;
	private String frequencyType;
	private int frequencyValue;

	@DateTimeFormat(pattern = DATE_TIME_PATTERN)
	private Date startDate;

	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			DATE_TIME_PATTERN);

	public Reminder() {
		// for construction via setters
	}

	public Reminder(int id, String title, String description, Date startDate,
			String location, String frequencyType, int frequencyValue,
			int clientId) {
		super(id, clientId);
		this.title = title;
		this.description = description;
		this.startDate = startDate;
		this.location = location;
		this.frequencyType = frequencyType;
		this.frequencyValue = frequencyValue;
	}

	public Reminder(int id, String title, String description,
			Timestamp startTimestamp, String location, String frequencyType,
			int frequencyValue, int clientId) {
		super(id, clientId);
		this.title = title;
		this.description = description;
		this.startDate = new Date(startTimestamp.getTime());
		this.location = location;
		this.frequencyType = frequencyType;
		this.frequencyValue = frequencyValue;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDateTime() {
		return dateFormat.format(startDate.getTime());
	}

	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startDate = new Date(startTimestamp.getTime());
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFrequencyType() {
		return frequencyType;
	}

	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}

	public int getFrequencyValue() {
		return frequencyValue;
	}

	public void setFrequencyValue(int frequencyValue) {
		this.frequencyValue = frequencyValue;
	}

	public static enum FrequencyType {
		HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY;

		public static FrequencyType getFrequencyType(String frequencyType) {
			frequencyType = frequencyType.trim().toLowerCase();
			if (frequencyType.equals("hourly"))
				return HOURLY;
			else if (frequencyType.equals("daily"))
				return DAILY;
			else if (frequencyType.equals("weekly"))
				return WEEKLY;
			else if (frequencyType.equals("monthly"))
				return MONTHLY;
			else if (frequencyType.equals("yearly"))
				return YEARLY;
			else
				return null;
		}
	}

}
