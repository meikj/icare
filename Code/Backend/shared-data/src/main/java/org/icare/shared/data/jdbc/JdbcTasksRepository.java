package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Client;
import org.icare.shared.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for a Task resource repository. This repository
 * relies on an appropriate JdbcOperations object for querying.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcTasksRepository extends JdbcResourceRepository<Task> {

	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "tasks";

	/**
	 * This represents the columns in the table for inserting and updating.
	 */
	public static final String[] TABLE_COLUMNS = new String[]{
		"title",
		"description",
		"startdate",
		"enddate",
		"location",
		"complete",
		"client_id"
	};
	
	private String insertQuery;
	private String updateQuery;
	
	@Autowired
	public JdbcTasksRepository(JdbcOperations jdbcOperations,
			ResourceRepository<Client> clientRepository) {
		super(jdbcOperations, clientRepository, TABLE_NAME, new TaskRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS);
		updateQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS);
	}

	@Override
	public void validate(Task task) throws BadRequestException {
		if (task.getTitle() == null || task.getTitle().isEmpty()) {
			throw new BadRequestException("Task title is missing or invalid.");
		} else if (task.getStartDate() == null) {
			throw new BadRequestException(
					"Task start date is missing or invalid.");
		}
	}

	@Override
	protected void doAdd(Task task, GeneratedKeyHolder keyHolder)
			throws DataAccessException {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery, PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, task.getTitle());
				ps.setString(2, task.getDescription());
				ps.setTimestamp(3, new Timestamp(task.getStartDate().getTime()));
				ps.setTimestamp(4, new Timestamp(task.getEndDate().getTime()));
				ps.setString(5, task.getLocation());
				ps.setBoolean(6, false);
				ps.setInt(7, task.getClientId());
				return ps;
			}

		}, keyHolder);
	}

	@Override
	protected void doUpdate(Task task) throws DataAccessException {
		jdbcOperations.update(updateQuery,
				task.getTitle(),
				task.getDescription(),
				task.getStartDate(),
				task.getEndDate(),
				task.getLocation(),
				task.isComplete(),
				task.getClientId(),
				task.getId());
	}

	/**
	 * Helper class for mapping Rows to Tasks.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class TaskRowMapper implements RowMapper<Task> {

		@Override
		public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Task(
					rs.getInt("id"),
					rs.getString("title"),
					rs.getString("description"),
					rs.getTimestamp("startdate"),
					rs.getTimestamp("enddate"),
					rs.getString("location"),
					rs.getBoolean("complete"),
					rs.getInt("client_id"));
		}

	}

}
