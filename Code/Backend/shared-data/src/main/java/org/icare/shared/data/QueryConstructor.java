package org.icare.shared.data;


/**
 * This is a helper class for constructing queries. It provides a greater
 * degree of flexibility over original methods. The notion of a query filter
 * has been introduced so that WHERE, ORDER BY and LIMIT can be added to a
 * base query easily.
 * 
 * The class has base query constructs for SELECT, INSERT, UPDATE and DELETE.
 * 
 * @author John Meikle
 *
 */
public class QueryConstructor {
	
	public static final String FILTER_TYPE_WHERE = "where";
	public static final String FILTER_TYPE_ORDER_BY = "order by";
	public static final String FILTER_TYPE_LIMIT = "limit";
	
	private static final String SELECT_SKELETON = "select * from %s";
	private static final String INSERT_SKELETON = "insert into %s (%s) values (%s)";
	private static final String UPDATE_SKELETON = "update %s set %s where id = ?";
	private static final String DELETE_SKELETON = "delete from %s";
	
	private String table;
	
	/**
	 * Construct a new QueryConstructor that uses the specified table name.
	 * 
	 * @param table the table name.
	 */
	public QueryConstructor(String table) {
		this.table = table;
	}
	
	/**
	 * Get the table name used for query construction.
	 */
	public String getTable() {
		return table;
	}
	
	/**
	 * Constructs the SELECT base query for this classes table name.
	 * 
	 * @return the fully formatted SELECT base query.
	 */
	public String baseQuerySelect() {
		return String.format(SELECT_SKELETON, table);
	}
	
	/**
	 * Constructs the INSERT base query for this classes table name along with
	 * an array of column name parameters.
	 * 
	 * @param params the array of column names in order.
	 * @return the fully formatted INSERT base query.
	 */
	public String baseQueryInsert(String... params) {
		StringBuilder cols = new StringBuilder();
		StringBuilder vals = new StringBuilder();
		for (int i = 0; i < params.length; i++) {
			cols.append(params[i]);
			vals.append("?");
			if (i < (params.length - 1)) {
				cols.append(", ");
				vals.append(", ");
			}
		}
		
		return String.format(INSERT_SKELETON, table, cols.toString(), vals.toString());
	}
	
	/**
	 * Constructs the UPDATE base query for this classes table name along with
	 * an array of column name parameters.
	 * 
	 * @param params the array of column names in order.
	 * @return the fully formatted INSERT base query.
	 */
	public String baseQueryUpdate(String... params) {
		StringBuilder cols = new StringBuilder();
		for (int i = 0; i < params.length; i++) {
			cols.append(params[i]);
			cols.append(" = ?");
			if (i < (params.length - 1))
				cols.append(", ");
		}
		
		return String.format(UPDATE_SKELETON, table, cols.toString());
	}
	
	/**
	 * Constructs the DELETE base query for this classes table name.
	 * 
	 * @return the full formatted DELETE base query.
	 */
	public String baseQueryDelete() {
		return String.format(DELETE_SKELETON, table);
	}
	
	/**
	 * Constructs a query with one or more filters applied to a base query.
	 * This method only supports filters constructed using FILTER_TYPE_WHERE,
	 * FILTER_TYPE_ORDER BY, and FILTER_TYPE_LIMIT.
	 * 
	 * @param baseQuery the base query.
	 * @param filters the array of filters to apply.
	 * @return the fully constructed query.
	 */
	public static String constructWithFilters(String baseQuery, QueryFilter... filters) {
		baseQuery = baseQuery.toLowerCase().trim();
		boolean hasWhere = baseQuery.contains("where");
		boolean hasOrderBy = baseQuery.contains("order by");
		boolean hasLimit = baseQuery.contains("limit");
		StringBuilder query = new StringBuilder(baseQuery);
		
		for (QueryFilter f : filters) {
			if (f == null) continue;
			
			String lcf = f.toString().toLowerCase().trim();
			
			if (lcf.contains(FILTER_TYPE_WHERE)) {
				if (hasWhere) {
					// Combine into one where
					lcf += " and";
					int startIndex = query.indexOf("where");
					query.replace(startIndex, startIndex + 5, lcf); // +5 for "where" length
				} else if (hasOrderBy) {
					// Insert before order by as where takes precedence
					lcf += " ";
					int startIndex = query.indexOf("order by");
					query.insert(startIndex, lcf);
					hasWhere = true;
				} else if (hasLimit) {
					// Insert before limit as where takes precedence
					lcf += " ";
					int startIndex = query.indexOf("limit");
					query.insert(startIndex, lcf);
					hasWhere = true;
				} else {
					query.append(" ");
					query.append(lcf);
					hasWhere = true;
				}
			} else if (lcf.contains(FILTER_TYPE_ORDER_BY)) {
				if (hasOrderBy) {
					// Combine into one order by
					lcf += ",";
					int startIndex = query.indexOf("order by");
					query.replace(startIndex, startIndex + 8, lcf); // +8 for "order by" length
				} else if (hasLimit) {
					// Insert before limit as order by takes precedence
					lcf += " ";
					int startIndex = query.indexOf("limit");
					query.insert(startIndex, lcf);
					hasOrderBy = true;
				} else {
					query.append(" ");
					query.append(lcf);
					hasOrderBy = true;
				}
			} else if (lcf.contains(FILTER_TYPE_LIMIT)) {
				if (hasLimit) {
					// Replace existing limit filter with this one
					int startIndex = query.indexOf("limit");
					query.delete(startIndex, query.length());
					query.append(lcf);
					hasLimit = true;
				} else {
					query.append(" ");
					query.append(lcf);
					hasLimit = true;
				}
			}
		}
		
		return query.toString();
	}
	
	/**
	 * Create a query filter for the constructWithFilters() method.
	 * 
	 * @param type the filter type (FILTER_TYPE_WHERE, FILTER_TYPE_ORDER_BY,
	 * or FILTER_TYPE_LIMIT).
	 * @param params the column name parameters to filter.
	 * @return the fully formed filter string.
	 */
	public static QueryFilter createFilter(String type, Object... params) {
		StringBuilder filter = new StringBuilder(type + " ");
		
		if (type.equals(FILTER_TYPE_WHERE)) {
			for (int i = 0; i < params.length; i++) {
				filter.append(params[i]);
				if (!((String) params[i]).equals("and") && !((String) params[i]).equals("or"))
					filter.append(" = ?");
				if (i < (params.length - 1))
					filter.append(" ");
			}
		} else if (type.equals(FILTER_TYPE_ORDER_BY)) {
			for (int i = 0; i < params.length; i++) {
				filter.append(params[i]);
				if (i < (params.length - 1))
					filter.append(", ");
			}
		} else if (type.equals(FILTER_TYPE_LIMIT)) {
			filter.append(params[0]);
		} else {
			throw new IllegalArgumentException(
					"Invalid type. Accepted types: FILTER_TYPE_WHERE, FILTER_TYPE_ORDER_BY, or FILTER_TYPE_LIMIT.");
		}
		
		return new QueryFilter(filter.toString());
	}

}
