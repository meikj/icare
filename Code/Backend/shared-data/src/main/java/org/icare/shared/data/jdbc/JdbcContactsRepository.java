package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Client;
import org.icare.shared.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for a Contact resource repository. This
 * repository relies on an appropriate JdbcOperations object for querying.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcContactsRepository extends JdbcResourceRepository<Contact> {

	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "contacts";
	
	/**
	 * This represents the columns in the table for inserting and updating.
	 */
	public static final String[] TABLE_COLUMNS = new String[]{
		"first_name",
		"last_name",
		"address",
		"email",
		"phone",
		"description",
		"client_id"
	};
	
	private String insertQuery;
	private String updateQuery;
	
	/**
	 * Construct a new ContactsRepository using a JdbcOperations object for
	 * querying.
	 * 
	 * @param jdbcOperations
	 *            the JdbcOperations object.
	 * @param clientRepository
	 *            the ClientRepository to use.
	 */
	@Autowired
	public JdbcContactsRepository(JdbcOperations jdbcOperations,
			ResourceRepository<Client> clientRepository) {
		super(jdbcOperations, clientRepository, TABLE_NAME, new ContactRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS);
		updateQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS);
	}

	@Override
	public void validate(Contact contact) throws BadRequestException {
		if (contact.getFirstName() == null || contact.getFirstName().isEmpty()) {
			throw new BadRequestException(
					"Contact first name missing or invalid.");
		} else if (contact.getLastName() == null
				|| contact.getLastName().isEmpty()) {
			throw new BadRequestException(
					"Contact last name missing or invalid.");
		} else if (contact.getPhone() == null || contact.getPhone().isEmpty()) {
			throw new BadRequestException(
					"Contact phone number missing or invalid.");
		}
	}

	@Override
	protected void doAdd(Contact contact, GeneratedKeyHolder keyHolder) {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, contact.getFirstName());
				ps.setString(2, contact.getLastName());
				ps.setString(3, contact.getAddress());
				ps.setString(4, contact.getEmail());
				ps.setString(5, contact.getPhone());
				ps.setString(6, contact.getDescription());
				ps.setInt(7, contact.getClientId());
				return ps;
			}

		}, keyHolder);
	}

	@Override
	public void doUpdate(Contact contact) throws DataAccessException {
		jdbcOperations.update(updateQuery,
				contact.getFirstName(),
				contact.getLastName(),
				contact.getAddress(),
				contact.getEmail(),
				contact.getPhone(),
				contact.getDescription(),
				contact.getClientId(),
				contact.getId());
	}

	/**
	 * Helper class for mapping Rows to Contacts.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class ContactRowMapper implements RowMapper<Contact> {

		@Override
		public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Contact(rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getString("address"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getString("description"),
					rs.getInt("client_id"));
		}

	}

}
