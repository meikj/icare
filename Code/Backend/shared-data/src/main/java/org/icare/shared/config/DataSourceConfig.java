package org.icare.shared.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * The data source configuration for the web application.
 * 
 * @author John Meikle
 *
 */
@Configuration
public class DataSourceConfig {
	
	@Autowired
	private DataSource dataSource;
	
	// NOTE: HikariCP is used as the DataSource for connection pooling

	/**
	 * Constructs a DataSource bean. Uses DriverManagerDataSource to connect to
	 * database server. This creates a new connection handler each time.
	 */
	// @Bean
	// public DataSource dataSource() {
	// DriverManagerDataSource dataSource = new DriverManagerDataSource();
	// dataSource.setDriverClassName(DRIVER_CLASS);
	// dataSource.setUrl(URL);
	// dataSource.setUsername(USERNAME);
	// dataSource.setPassword(PASSWORD);
	// return dataSource;
	// }

	/**
	 * Constructs a DataSource bean. Uses BoneCPDataSource to connect to
	 * database server. This uses pooling for maximum performance. See
	 * http://jolbox.com/
	 */
//	@Bean
//	public DataSource dataSource() {
//		BoneCPDataSource dataSource = new BoneCPDataSource();
//		dataSource.setDriverClass(DRIVER_CLASS);
//		dataSource.setJdbcUrl(URL);
//		dataSource.setUsername(USERNAME);
//		dataSource.setPassword(PASSWORD);
//		return dataSource;
//	}

	/**
	 * Constructs a JdbcTemplate bean. This uses the DataSource bean in a JDBC
	 * context.
	 */
	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}

}
