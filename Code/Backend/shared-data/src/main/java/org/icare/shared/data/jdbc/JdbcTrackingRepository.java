package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Client;
import org.icare.shared.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for a Track resource repository. This
 * repository relies on an appropriate JdbcOperations object for querying.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcTrackingRepository extends JdbcResourceRepository<Track> {

	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "tracking";

	/**
	 * This represents the columns in the table for inserting.
	 */
	public static final String[] TABLE_COLUMNS = new String[] {
		"coords",
		"datetime",
		"client_id"
	};
	
	private String insertQuery;
	
	@Autowired
	public JdbcTrackingRepository(JdbcOperations jdbcOperations,
			ResourceRepository<Client> clientRepository) {
		super(jdbcOperations, clientRepository, TABLE_NAME, new TrackRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS);
	}

	@Override
	public void validate(Track track) throws BadRequestException {
		if (track.getCoords() == null || track.getCoords().isEmpty()) {
			throw new BadRequestException("Track coords is missing or empty.");
		} else if (track.getDateTime() == null) {
			throw new BadRequestException(
					"Track date time is missing or empty.");
		}
	}

	@Override
	protected void doAdd(Track track, GeneratedKeyHolder keyHolder)
			throws DataAccessException {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, track.getCoords());
				ps.setTimestamp(2, new Timestamp(track.getDateTime().getTime()));
				ps.setInt(3, track.getClientId());
				return ps;
			}

		}, keyHolder);
	}

	@Override
	protected void doUpdate(Track model) throws DataAccessException {
		// Pass... no update on tracking resources
	}

	/**
	 * Helper class for mapping Rows to Tracks.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class TrackRowMapper implements RowMapper<Track> {

		@Override
		public Track mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Track(
					rs.getInt("id"),
					rs.getString("coords"),
					rs.getTimestamp("datetime"),
					rs.getInt("client_id"));
		}

	}

}
