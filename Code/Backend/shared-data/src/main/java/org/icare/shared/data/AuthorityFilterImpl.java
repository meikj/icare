package org.icare.shared.data;

import org.icare.shared.model.Client;
import org.icare.shared.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This is an abstract implementation of AuthorityFilter. This accesses the
 * SecurityContext for the currently authorised users Authentication model,
 * which gives access to their authorities and user/client model.
 * 
 * @author John Meikle
 *
 */
public class AuthorityFilterImpl implements AuthorityFilter {

	@Override
	public boolean isCarer() {
		for (GrantedAuthority ga : SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities()) {
			if (ga.toString().equals("ROLE_CARER"))
				return true;
		}
		return false;
	}

	@Override
	public boolean isClient() {
		for (GrantedAuthority ga : SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities()) {
			if (ga.toString().equals("ROLE_CLIENT"))
				return true;
		}
		return false;
	}

	@Override
	public boolean isAdmin() {
		for (GrantedAuthority ga : SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities()) {
			if (ga.toString().equals("ROLE_ADMIN"))
				return true;
		}
		return false;
	}

	@Override
	public User getUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication()
				.getDetails();
	}

	@Override
	public Client getClient() {
		return (Client) SecurityContextHolder.getContext().getAuthentication()
				.getDetails();
	}

}
