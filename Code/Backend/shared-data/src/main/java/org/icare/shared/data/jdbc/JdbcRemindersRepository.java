package org.icare.shared.data.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.icare.shared.data.ResourceRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Client;
import org.icare.shared.model.Reminder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * This is a JDBC implementation for a Reminder resource repository. This
 * repository relies on an appropriate JdbcOperations object for querying.
 * 
 * @author John Meikle
 *
 */
@Repository
public class JdbcRemindersRepository extends JdbcResourceRepository<Reminder> {

	/**
	 * This represents the table name.
	 */
	public static final String TABLE_NAME = "reminders";

	/**
	 * This represents the columns in the table for inserting and updating.
	 */
	public static final String[] TABLE_COLUMNS = new String[]{
		"title",
		"description",
		"startdate",
		"location",
		"frequency_type",
		"frequency_value",
		"client_id"
	};
	
	private String insertQuery;
	private String updateQuery;
	
	@Autowired
	public JdbcRemindersRepository(JdbcOperations jdbcOperations,
			ResourceRepository<Client> clientRepository) {
		super(jdbcOperations, clientRepository, TABLE_NAME, new ReminderRowMapper());
		initQueries();
	}
	
	/**
	 * Initialise the INSERT and UPDATE queries using the QueryConstructor.
	 */
	private void initQueries() {
		insertQuery = queryConstructor.baseQueryInsert(TABLE_COLUMNS);
		updateQuery = queryConstructor.baseQueryUpdate(TABLE_COLUMNS);
	}

	@Override
	public void validate(Reminder reminder) throws BadRequestException {
		if (reminder.getTitle() == null || reminder.getTitle().isEmpty()) {
			throw new BadRequestException("Reminder title missing or invalid.");
		} else if (reminder.getStartDate() == null) {
			throw new BadRequestException(
					"Reminder start date missing or invalid.");
		} else if (reminder.getFrequencyType() == null
				|| Reminder.FrequencyType.getFrequencyType(reminder
						.getFrequencyType()) == null) {
			throw new BadRequestException(
					"Reminder frequency type missing or invalid.");
		} else if (reminder.getFrequencyValue() <= 0) {
			throw new BadRequestException(
					"Reminder frequency value missing or invalid.");
		}
	}

	@Override
	protected void doAdd(Reminder reminder, GeneratedKeyHolder keyHolder) throws DataAccessException {
		jdbcOperations.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(insertQuery,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, reminder.getTitle());
				ps.setString(2, reminder.getDescription());
				ps.setTimestamp(3, new Timestamp(reminder.getStartDate()
						.getTime()));
				ps.setString(4, reminder.getLocation());
				ps.setString(5, reminder.getFrequencyType());
				ps.setInt(6, reminder.getFrequencyValue());
				ps.setInt(7, reminder.getClientId());
				return ps;
			}

		}, keyHolder);
	}

	@Override
	protected void doUpdate(Reminder reminder) throws DataAccessException {
		jdbcOperations.update(updateQuery,
				reminder.getTitle(),
				reminder.getDescription(),
				reminder.getStartDate(),
				reminder.getLocation(),
				reminder.getFrequencyType(),
				reminder.getFrequencyValue(),
				reminder.getClientId(),
				reminder.getId());
	}

	/**
	 * Helper class for mapping Rows to Reminders.
	 * 
	 * @author John Meikle
	 *
	 */
	private static final class ReminderRowMapper implements RowMapper<Reminder> {

		@Override
		public Reminder mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Reminder(
					rs.getInt("id"),
					rs.getString("title"),
					rs.getString("description"),
					rs.getTimestamp("startdate"),
					rs.getString("location"),
					rs.getString("frequency_type"),
					rs.getInt("frequency_value"),
					rs.getInt("client_id"));
		}

	}

}
