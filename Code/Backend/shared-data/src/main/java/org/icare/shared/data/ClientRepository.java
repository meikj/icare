package org.icare.shared.data;

import org.icare.shared.exception.ResourceNotFoundException;
import org.icare.shared.model.Client;
import org.springframework.dao.DataAccessException;

/**
 * Provides an interface for specialised client repository methods.
 * 
 * @author John Meikle
 *
 */
public interface ClientRepository {

	/**
	 * Find a client by auth token.
	 * 
	 * @param authToken
	 *            the auth token.
	 * @return the client model.
	 * @throws DataAccessException
	 *             if a problem occurs when finding the client.
	 * @throws ResourceNotFoundException
	 *             if the client is not found by the specified auth token.
	 */
	public Client findByAuthToken(String authToken) throws DataAccessException,
			ResourceNotFoundException;

}
