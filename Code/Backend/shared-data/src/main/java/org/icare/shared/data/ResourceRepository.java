package org.icare.shared.data;

import java.util.List;

import org.icare.shared.exception.BadRequestException;
import org.icare.shared.exception.ResourceNotFoundException;
import org.icare.shared.exception.UnauthorisedAccessException;
import org.icare.shared.model.Resource;
import org.springframework.dao.DataAccessException;

/**
 * Provides a general interface for repositories accessing backend resources.
 * 
 * @author John Meikle
 *
 * @param <T>
 *            the type of Resource.
 */
public interface ResourceRepository<T extends Resource> {

	/**
	 * Add a new resource of type T.
	 * 
	 * @param model
	 *            the resource model.
	 * @return The resulting resource model, complete with ID, etc.
	 * @throws DataAccessException
	 *             if a problem occurs when adding the data.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 * @throws BadRequestException
	 *             if the model is invalid.
	 */
	public T add(T model) throws UnauthorisedAccessException,
			DataAccessException, BadRequestException;

	/**
	 * Find the list of all resources.
	 * 
	 * @return the list of all resources.
	 * @throws DataAccessException
	 *             if a problem occurs when finding the data.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 */
	public List<T> findAll() throws UnauthorisedAccessException,
			DataAccessException;
	
	/**
	 * Find the list of all resources.
	 * 
	 * @param filters the array of filters to apply to the query.
	 * @return the list of all resources.
	 * @throws DataAccessException
	 *             if a problem occurs when finding the data.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 */
	public List<T> findAll(QueryFilter... filters) throws UnauthorisedAccessException,
			DataAccessException;

	/**
	 * Find a resource by ID.
	 * 
	 * @param id
	 *            the id.
	 * @return the resource model.
	 * @throws DataAccessException
	 *             if a problem occurs when finding the data.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 * @throws ResourceNotFoundException
	 *             if the resource by the specified ID is not found.
	 */
	public T findById(int id) throws UnauthorisedAccessException,
			DataAccessException, ResourceNotFoundException;

	/**
	 * Remove a resource by its ID.
	 * 
	 * @param id
	 *            the id.
	 * @throws DataAccessException
	 *             if a problem occurs when removing the data.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 * @throws ResourceNotFoundException
	 *             if the resource by the specified ID is not found.
	 */
	public void remove(int id) throws UnauthorisedAccessException,
			DataAccessException, ResourceNotFoundException;

	/**
	 * Update a resource. This uses the ID specified in the model.
	 * 
	 * @param model
	 *            the resource model.
	 * @return the (possibly newly updated) resource model.
	 * @throws DataAccessException
	 *             if a problem occurs when updating the data.
	 * @throws UnauthorisedAccessException
	 *             if the current user is unauthorised.
	 * @throws BadRequestException
	 *             if the model is invalid.
	 */
	public T update(T model) throws UnauthorisedAccessException,
			DataAccessException, BadRequestException;

}
