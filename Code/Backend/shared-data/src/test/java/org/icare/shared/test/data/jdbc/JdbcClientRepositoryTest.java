package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.icare.shared.data.ClientRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcClientRepositoryTest extends JdbcResourceRepositoryTest {
	
	private Client c1;
	private Client c2;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(Client.DATE_TIME_PATTERN);
	
	@Before
	public void initClients() {
		String firstName = "Test First";
		String lastName = "Test Last";
		String email = "test@test.com";
		String phone = "014155544332";
		String address = "110 Test Street";
		Date dob = new Date();
		String authToken = "56789";
		int userId = 1;
		
		c1 = new Client();
		c1.setFirstName(firstName);
		c1.setLastName(lastName);
		c1.setEmail(email);
		c1.setPhone(phone);
		c1.setAddress(address);
		c1.setDob(dob);
		c1.setAuthToken(authToken);
		c1.setUserId(userId);
		
		firstName = "Test Again";
		lastName = "Testy Test";
		email = "test2@test2.com";
		phone = "014153444332";
		address = "110 Test Avenue";
		dob = new GregorianCalendar(1993, 01, 02).getTime();
		authToken = "2345";
		
		c2 = new Client();
		c2.setFirstName(firstName);
		c2.setLastName(lastName);
		c2.setEmail(email);
		c2.setPhone(phone);
		c2.setAddress(address);
		c2.setDob(dob);
		c2.setAuthToken(authToken);
		c2.setUserId(userId);
	}
	
	@Test
	public void testValidate() {
		getClientRepository().validate(c1);
	}
	
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		getClientRepository().validate(new Client());
	}
	
	@Test
	public void testInsert() {
		Client result = getClientRepository().add(c1);
		assertEquals(c1.getFirstName(), result.getFirstName());
		assertEquals(c1.getLastName(), result.getLastName());
		assertEquals(c1.getEmail(), result.getEmail());
		assertEquals(c1.getPhone(), result.getPhone());
		assertEquals(c1.getAddress(), result.getAddress());
		assertEquals(dateFormat.format(c1.getDob()), dateFormat.format(result.getDob()));
		assertEquals(c1.getAuthToken(), result.getAuthToken());
		assertEquals(c1.getUserId(), result.getUserId());
		
		result = getClientRepository().add(c2);
		assertEquals(c2.getFirstName(), result.getFirstName());
		assertEquals(c2.getLastName(), result.getLastName());
		assertEquals(c2.getEmail(), result.getEmail());
		assertEquals(c2.getPhone(), result.getPhone());
		assertEquals(c2.getAddress(), result.getAddress());
		assertEquals(dateFormat.format(c2.getDob()), dateFormat.format(result.getDob()));
		assertEquals(c2.getAuthToken(), result.getAuthToken());
		assertEquals(c2.getUserId(), result.getUserId());
	}
	
	@Test
	public void testUpdate() {
		Client result = getClientRepository().add(c2);
		
		String firstName = "Bob";
		String lastName = "Jones";
		String email = "bj@bj.com";
		String phone = "0141533334332";
		String address = "110 Bobby Avenue";
		Date dob = new GregorianCalendar(1990, 02, 01).getTime();
		String authToken = "100000";
		int userId = 2;
		
		result.setFirstName(firstName);
		result.setLastName(lastName);
		result.setEmail(email);
		result.setPhone(phone);
		result.setAddress(address);
		result.setDob(dob);
		result.setAuthToken(authToken);
		result.setUserId(userId);
		
		result = getClientRepository().update(result);
		assertEquals(firstName, result.getFirstName());
		assertEquals(lastName, result.getLastName());
		assertEquals(email, result.getEmail());
		assertEquals(phone, result.getPhone());
		assertEquals(address, result.getAddress());
		assertEquals(dateFormat.format(dob), dateFormat.format(result.getDob()));
		assertEquals(authToken, result.getAuthToken());
		assertEquals(userId, result.getUserId());
	}
	
	@Test
	public void testFindByAuthToken() {
		getClientRepository().add(c1);
		getClientRepository().add(c2);
		ClientRepository clientRepo = (ClientRepository) getClientRepository();
		Client result1 = clientRepo.findByAuthToken("56789");
		Client result2 = clientRepo.findByAuthToken("2345");
		
		assertEquals(c1.getFirstName(), result1.getFirstName());
		assertEquals(c1.getLastName(), result1.getLastName());
		assertEquals(c1.getEmail(), result1.getEmail());
		assertEquals(c1.getPhone(), result1.getPhone());
		assertEquals(c1.getAddress(), result1.getAddress());
		assertEquals(dateFormat.format(c1.getDob()), dateFormat.format(result1.getDob()));
		assertEquals(c1.getAuthToken(), result1.getAuthToken());
		assertEquals(c1.getUserId(), result1.getUserId());
		
		assertEquals(c2.getFirstName(), result2.getFirstName());
		assertEquals(c2.getLastName(), result2.getLastName());
		assertEquals(c2.getEmail(), result2.getEmail());
		assertEquals(c2.getPhone(), result2.getPhone());
		assertEquals(c2.getAddress(), result2.getAddress());
		assertEquals(dateFormat.format(c2.getDob()), dateFormat.format(result2.getDob()));
		assertEquals(c2.getAuthToken(), result2.getAuthToken());
		assertEquals(c2.getUserId(), result2.getUserId());
	}

}
