package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import org.icare.shared.data.UserRepository;
import org.icare.shared.data.jdbc.JdbcUserRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcUserRepositoryTest extends JdbcResourceRepositoryTest {
	
	private JdbcUserRepository repo;
	
	private User u1;
	private User u2;
	
	@Before
	public void setupUserRepo() {
		repo = new JdbcUserRepository(getJdbcOperations());
		repo.setAuthorityFilter(getClientRepository().getAuthorityFilter());
	}
	
	@Before
	public void initUsers() {
		String firstName = "Test First";
		String lastName = "Test Last";
		String username = "testuser1";
		String password = "test";
		String email = "test@test.com";
		String phone = "014155544332";
		boolean enabled = true;
		String address = "110 Test Street";
		String authority = "ROLE_CARER";
		
		u1 = new User();
		u1.setFirstName(firstName);
		u1.setLastName(lastName);
		u1.setUsername(username);
		u1.setPassword(password);
		u1.setEmail(email);
		u1.setPhone(phone);
		u1.setEnabled(enabled);
		u1.setAddress(address);
		u1.setAuthority(authority);
		
		firstName = "Another Test First";
		lastName = "Another Test Last";
		username = "testuser2";
		password = "test";
		email = "test2@test2.com";
		phone = "01413332212";
		enabled = true;
		address = "111 Test Avenue";
		authority = "ROLE_CARER";
		
		u2 = new User();
		u2.setFirstName(firstName);
		u2.setLastName(lastName);
		u2.setUsername(username);
		u2.setPassword(password);
		u2.setEmail(email);
		u2.setPhone(phone);
		u2.setEnabled(enabled);
		u2.setAddress(address);
		u2.setAuthority(authority);
	}
	
	@Test
	public void testValidate() {
		repo.validate(u1);
	}
	
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		repo.validate(new User());
	}
	
	@Test
	public void testInsert() {
		User result = repo.add(u1);
		assertEquals(u1.getFirstName(), result.getFirstName());
		assertEquals(u1.getLastName(), result.getLastName());
		assertEquals(u1.getUsername(), result.getUsername());
		assertEquals(u1.getPassword(), result.getPassword());
		assertEquals(u1.getEmail(), result.getEmail());
		assertEquals(u1.getPhone(), result.getPhone());
		assertEquals(u1.isEnabled(), result.isEnabled());
		assertEquals(u1.getAddress(), result.getAddress());
		assertEquals(u1.getAuthority(), result.getAuthority());
		
		result = repo.add(u2);
		assertEquals(u2.getFirstName(), result.getFirstName());
		assertEquals(u2.getLastName(), result.getLastName());
		assertEquals(u2.getUsername(), result.getUsername());
		assertEquals(u2.getPassword(), result.getPassword());
		assertEquals(u2.getEmail(), result.getEmail());
		assertEquals(u2.getPhone(), result.getPhone());
		assertEquals(u2.isEnabled(), result.isEnabled());
		assertEquals(u2.getAddress(), result.getAddress());
		assertEquals(u2.getAuthority(), result.getAuthority());
	}
	
	@Test
	public void testUpdate() {
		User result = repo.add(u2);
		
		int id = result.getId();
		String firstName = "First Namet";
		String lastName = "Last Name";
		String username = "testuser3";
		String password = "test";
		String email = "test3@test3.com";
		String phone = "0141555445782";
		boolean enabled = false;
		String address = "110 Test Avenue";
		String authority = "ROLE_ADMIN";
		
		result.setFirstName(firstName);
		result.setLastName(lastName);
		result.setUsername(username);
		result.setPassword(password);
		result.setEmail(email);
		result.setPhone(phone);
		result.setEnabled(enabled);
		result.setAddress(address);
		result.setAuthority(authority);
		
		result = repo.update(result);
		assertEquals(id, result.getId());
		assertEquals(firstName, result.getFirstName());
		assertEquals(lastName, result.getLastName());
		assertEquals(username, result.getUsername());
		assertEquals(password, result.getPassword());
		assertEquals(email, result.getEmail());
		assertEquals(phone, result.getPhone());
		assertEquals(enabled, result.isEnabled());
		assertEquals(address, result.getAddress());
		assertEquals(authority, result.getAuthority());
		
		result.setEnabled(false);
		result = repo.update(result);
		assertEquals(false, result.isEnabled());
	}
	
	@Test
	public void testFindByUsername() {
		repo.add(u1);
		repo.add(u2);
		UserRepository userRepo = (UserRepository) repo;
		User result1 = userRepo.findByUsername("testuser1");
		User result2 = userRepo.findByUsername("testuser2");
		
		assertEquals(u1.getFirstName(), result1.getFirstName());
		assertEquals(u1.getLastName(), result1.getLastName());
		assertEquals(u1.getUsername(), result1.getUsername());
		assertEquals(u1.getPassword(), result1.getPassword());
		assertEquals(u1.getEmail(), result1.getEmail());
		assertEquals(u1.getPhone(), result1.getPhone());
		assertEquals(u1.isEnabled(), result1.isEnabled());
		assertEquals(u1.getAddress(), result1.getAddress());
		assertEquals(u1.getAuthority(), result1.getAuthority());
		
		assertEquals(u2.getFirstName(), result2.getFirstName());
		assertEquals(u2.getLastName(), result2.getLastName());
		assertEquals(u2.getUsername(), result2.getUsername());
		assertEquals(u2.getPassword(), result2.getPassword());
		assertEquals(u2.getEmail(), result2.getEmail());
		assertEquals(u2.getPhone(), result2.getPhone());
		assertEquals(u2.isEnabled(), result2.isEnabled());
		assertEquals(u2.getAddress(), result2.getAddress());
		assertEquals(u2.getAuthority(), result2.getAuthority());
	}
	
	@Test
	public void testFindByCredentials() {
		User addResult1 = repo.add(u1);
		User addResult2 = repo.add(u2);
		
		UserRepository userRepo = (UserRepository) repo;
		User result1 = userRepo.findByCredentials("testuser1", "test");
		User result2 = userRepo.findByCredentials("testuser2", "test");
		
		assertEquals(addResult1.getId(), result1.getId());
		assertEquals(u1.getFirstName(), result1.getFirstName());
		assertEquals(u1.getLastName(), result1.getLastName());
		assertEquals(u1.getUsername(), result1.getUsername());
		assertEquals(u1.getPassword(), result1.getPassword());
		assertEquals(u1.getEmail(), result1.getEmail());
		assertEquals(u1.getPhone(), result1.getPhone());
		assertEquals(u1.isEnabled(), result1.isEnabled());
		assertEquals(u1.getAddress(), result1.getAddress());
		assertEquals(u1.getAuthority(), result1.getAuthority());
		
		assertEquals(addResult2.getId(), result2.getId());
		assertEquals(u2.getFirstName(), result2.getFirstName());
		assertEquals(u2.getLastName(), result2.getLastName());
		assertEquals(u2.getUsername(), result2.getUsername());
		assertEquals(u2.getPassword(), result2.getPassword());
		assertEquals(u2.getEmail(), result2.getEmail());
		assertEquals(u2.getPhone(), result2.getPhone());
		assertEquals(u2.isEnabled(), result2.isEnabled());
		assertEquals(u2.getAddress(), result2.getAddress());
		assertEquals(u2.getAuthority(), result2.getAuthority());
	}

}
