package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.icare.shared.data.jdbc.JdbcTrackingRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Track;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcTrackingRepositoryTest extends JdbcResourceRepositoryTest {
	
	private JdbcTrackingRepository repo;
	
	private Track t1;
	private Track t2;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(Track.DATE_TIME_PATTERN);
	
	@Before
	public void setupAlertsRepo() {
		repo = new JdbcTrackingRepository(getJdbcOperations(), getClientRepository());
		repo.setAuthorityFilter(getClientRepository().getAuthorityFilter());
	}
	
	@Before
	public void initTracks() {
		String coords = "50.321,-4.321";
		Date dateTime = new Date();
		
		t1 = new Track();
		t1.setCoords(coords);
		t1.setDateTime(dateTime);
		
		coords = "30.012,-5.874";
		dateTime = new GregorianCalendar(2020, 02, 03).getTime();
		
		t2 = new Track();
		t2.setCoords(coords);
		t2.setDateTime(dateTime);
	}
	
	@Test
	public void testValidate() {
		repo.validate(t1);
	}
	
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		Track t = new Track();
		repo.validate(t);
	}
	
	@Test
	public void testInsert() {
		Track result = repo.add(t1);
		assertEquals(1, result.getId());
		assertEquals(t1.getCoords(), result.getCoords());
		assertEquals(dateFormat.format(t1.getDateTime()), dateFormat.format(result.getDateTime()));
		assertEquals(getUser().getSelectedClientId(), result.getClientId());
		
		result = repo.add(t2);
		assertEquals(2, result.getId());
		assertEquals(t2.getCoords(), result.getCoords());
		assertEquals(dateFormat.format(t2.getDateTime()), dateFormat.format(result.getDateTime()));
		assertEquals(getUser().getSelectedClientId(), result.getClientId());
	}
	
	@Test
	public void testUpdate() {
		Track result = repo.add(t1);
		int id = result.getId();
		int clientId = result.getClientId();
		result.setCoords(t1.getCoords());
		result.setDateTime(t1.getDateTime());
		result = repo.update(result);
		assertEquals(id, result.getId());
		assertEquals(t1.getCoords(), result.getCoords());
		assertEquals(dateFormat.format(t1.getDateTime()), dateFormat.format(result.getDateTime()));
		assertEquals(clientId, result.getClientId());
	}

}
