package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.icare.shared.data.jdbc.JdbcRemindersRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Reminder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcRemindersRepositoryTest extends JdbcResourceRepositoryTest {
	
	private JdbcRemindersRepository  repo;
	
	private Reminder r1;
	private Reminder r2;
	
	@Before
	public void setupTasksRepo() {
		repo = new JdbcRemindersRepository(getJdbcOperations(), getClientRepository());
		repo.setAuthorityFilter(getClientRepository().getAuthorityFilter());
	}
	
	@Before
	public void initReminders() {
		String title = "My Test Title";
		String description = "My test description!";
		Date startDate = new Date();
		String frequencyType = "weekly";
		int frequencyValue = 2;
		String location = "123 Test Road";
		
		r1 = new Reminder();
		r1.setTitle(title);
		r1.setDescription(description);
		r1.setStartDate(startDate);
		r1.setFrequencyType(frequencyType);
		r1.setFrequencyValue(frequencyValue);
		r1.setLocation(location);
		
		title = "Testing!";
		description = "Another test description!";
		startDate = new Date();
		frequencyType = "daily";
		frequencyValue = 1;
		location = "110 Test Street";
		
		r2 = new Reminder();
		r2.setTitle(title);
		r2.setDescription(description);
		r2.setStartDate(startDate);
		r2.setFrequencyType(frequencyType);
		r2.setFrequencyValue(frequencyValue);
		r2.setLocation(location);
	}
	
	/**
	 * Test case to ensure a minimal task is validated.
	 */
	@Test
	public void testValidate() {
		repo.validate(r1);
	}

	/**
	 * Test case to ensure BadRequestException is thrown when required fields
	 * are null/missing.
	 */
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		repo.validate(new Reminder());
	}

	/**
	 * Test case to ensure a task INSERT correctly inserts all values.
	 */
	@Test
	public void testInsert() {
		Reminder result = repo.add(r1);
		assertEquals(result.getId(), 1);
		assertEquals(result.getTitle(), r1.getTitle());
		assertEquals(result.getDescription(), r1.getDescription());
		assertEquals(result.getStartDateTime(), r1.getStartDateTime());
		assertEquals(result.getFrequencyType(), r1.getFrequencyType());
		assertEquals(result.getFrequencyValue(), r1.getFrequencyValue());
		assertEquals(result.getLocation(), r1.getLocation());
		assertEquals(result.getClientId(), getUser().getSelectedClientId());
		
		result = repo.add(r2);
		assertEquals(result.getId(), 2);
		assertEquals(result.getTitle(), r2.getTitle());
		assertEquals(result.getDescription(), r2.getDescription());
		assertEquals(result.getStartDate(), r2.getStartDate());
		assertEquals(result.getFrequencyType(), r2.getFrequencyType());
		assertEquals(result.getFrequencyValue(), r2.getFrequencyValue());
		assertEquals(result.getLocation(), r2.getLocation());
		assertEquals(result.getClientId(), getUser().getSelectedClientId());
	}

	/**
	 * Test case to ensure a task UPDATE correctly updates all values.
	 */
	@Test
	public void testUpdate() {
		Reminder result2 = repo.add(r2);
		
		int id = result2.getId();
		String title = "My Test Title";
		String description = "My test description!";
		Date startDate = new Date();
		String frequencyType = "hourly";
		int frequencyValue = 6;
		String location = "123 Test Road";
		int clientId = result2.getClientId();
		
		result2.setTitle(title);
		result2.setDescription(description);
		result2.setStartDate(startDate);
		result2.setFrequencyType(frequencyType);
		result2.setFrequencyValue(frequencyValue);
		result2.setLocation(location);
		
		result2 = repo.update(result2);
		assertEquals(id, result2.getId());
		assertEquals(title, result2.getTitle());
		assertEquals(description, result2.getDescription());
		assertEquals(startDate, result2.getStartDate());
		assertEquals(frequencyType, result2.getFrequencyType());
		assertEquals(frequencyValue, result2.getFrequencyValue());
		assertEquals(location, result2.getLocation());
		assertEquals(clientId, result2.getClientId());
	}

}
