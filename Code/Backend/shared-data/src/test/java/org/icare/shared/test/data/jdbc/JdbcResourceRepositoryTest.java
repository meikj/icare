package org.icare.shared.test.data.jdbc;

import org.icare.shared.data.AuthorityFilter;
import org.icare.shared.data.jdbc.JdbcClientRepository;
import org.icare.shared.model.Client;
import org.icare.shared.model.User;
import org.icare.shared.test.config.DataSourceConfig;
import org.junit.After;
import org.junit.Before;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = DataSourceConfig.class)
public abstract class JdbcResourceRepositoryTest {
	
	private EmbeddedDatabase db;
	private JdbcOperations jdbcOperations;
	private JdbcClientRepository clientRepo;
	private int selectedClientId = 1; // "Robert Paulson"
	private User user;
	private AuthorityFilter authorityFilter;
	
	@Before
	public void setupClientRepo() {
		// just so there's a fresh slate each test
		db = new EmbeddedDatabaseBuilder().addScripts("test_schema.sql", "test_data.sql").build();
		jdbcOperations = new JdbcTemplate(db);
		
		// Values taken from 'data.sql'
		user = new User(1, "John", "Meikle", "john", "", "john@meikj.com", "01415554433", true, "110 Test Road", "ROLE_ADMIN");
		
		// Create a 'mock' AuthorityFilter with admin enabled
		authorityFilter = new TestAuthorityFilter();
		
		clientRepo = new JdbcClientRepository(jdbcOperations);
		clientRepo.setAuthorityFilter(authorityFilter);
	}
	
	@After
	public void shutdown() {
		if (db != null)
			db.shutdown();
	}
	
	/**
	 * Get a reference to this tests JdbcOperations (uses JdbcTemplate
	 * implementation).
	 */
	protected JdbcOperations getJdbcOperations() {
		return jdbcOperations;
	}
	
	/**
	 * Get a reference to this tests JdbcClientRepository.
	 */
	protected JdbcClientRepository getClientRepository() {
		return clientRepo;
	}
	
	/**
	 * Get a reference to this tests User.
	 */
	protected User getUser() {
		return user;
	}
	
	/**
	 * Get a reference to this tests AuthorityFilter.
	 */
	protected AuthorityFilter getAuthorityFilter() {
		return authorityFilter;
	}
	
	/**
	 * Create a 'mock' AuthorityFilter with admin enabled for testing purposes.
	 * 
	 * @author John Meikle
	 *
	 */
	protected final class TestAuthorityFilter implements AuthorityFilter {
		@Override
		public boolean isCarer() { return false; }
		@Override
		public boolean isClient() { return false; }
		@Override
		public Client getClient() { return null; }

		@Override
		public boolean isAdmin() {
			return true;
		}
		@Override
		public User getUser() {
			user.setSelectedClientId(selectedClientId);
			return user;
		}
	}

}
