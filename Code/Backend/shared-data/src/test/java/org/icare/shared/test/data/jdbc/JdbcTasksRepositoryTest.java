package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.icare.shared.data.jdbc.JdbcTasksRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcTasksRepositoryTest extends JdbcResourceRepositoryTest {
	
	private JdbcTasksRepository repo;
	
	private Task t1;
	private Task t2;
	
	@Before
	public void setupTasksRepo() {
		repo = new JdbcTasksRepository(getJdbcOperations(), getClientRepository());
		repo.setAuthorityFilter(getClientRepository().getAuthorityFilter());
	}
	
	@Before
	public void initTasks() {
		String title = "My Test Title";
		String description = "My test description!";
		Date startDate = new Date();
		Calendar endDate = new GregorianCalendar(2020, 1, 1);
		String location = "123 Test Road";
		
		t1 = new Task();
		t1.setTitle(title);
		t1.setDescription(description);
		t1.setStartDate(startDate);
		t1.setEndDate(endDate.getTime());
		t1.setLocation(location);
		
		title = "Another Test Title";
		description = "Another description!";
		startDate = new Date();
		endDate = new GregorianCalendar(2025, 5, 5);
		location = "110 Test Avenue";
		
		t2 = new Task();
		t2.setTitle(title);
		t2.setDescription(description);
		t2.setStartDate(startDate);
		t2.setEndDate(endDate.getTime());
		t2.setLocation(location);
	}

	/**
	 * Test case to ensure a minimal task is validated.
	 */
	@Test
	public void testValidate() {
		Task t = new Task();
		t.setTitle("Test Title");
		t.setStartDate(new Date());
		repo.validate(t);
	}

	/**
	 * Test case to ensure BadRequestException is thrown when required fields
	 * are null/missing.
	 */
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		Task t = new Task();
		repo.validate(t);
	}
	
	/**
	 * Test case to ensure BadRequestException is thrown when Task title is
	 * empty.
	 */
	@Test(expected = BadRequestException.class)
	public void testValidateException2() {
		Task t = new Task();
		t.setTitle("");
		t.setStartDate(new Date());
		repo.validate(t);
	}

	/**
	 * Test case to ensure a task INSERT correctly inserts all values.
	 */
	@Test
	public void testInsert() {
		Task result = repo.add(t1);
		assertEquals(result.getId(), 1);
		assertEquals(result.getTitle(), t1.getTitle());
		assertEquals(result.getDescription(), t1.getDescription());
		assertEquals(result.getStartDateTime(), t1.getStartDateTime());
		assertEquals(result.getEndDate(), t1.getEndDate());
		assertEquals(result.getLocation(), t1.getLocation());
		assertEquals(result.getClientId(), getUser().getSelectedClientId());
		
		result = repo.add(t2);
		assertEquals(result.getId(), 2);
		assertEquals(result.getTitle(), t2.getTitle());
		assertEquals(result.getDescription(), t2.getDescription());
		assertEquals(result.getStartDate(), t2.getStartDate());
		assertEquals(result.getEndDate(), t2.getEndDate());
		assertEquals(result.getLocation(), t2.getLocation());
		assertEquals(result.getClientId(), getUser().getSelectedClientId());
	}

	/**
	 * Test case to ensure a task UPDATE correctly updates all values.
	 */
	@Test
	public void testUpdate() {
		Task result2 = repo.add(t2);
		
		int id = result2.getId();
		String title = "My Test Title";
		String description = "My test description!";
		Date startDate = new Date();
		Calendar endDate = new GregorianCalendar(2020, 1, 1);
		String location = "123 Test Road";
		int clientId = result2.getClientId();
		
		result2.setTitle(title);
		result2.setDescription(description);
		result2.setStartDate(startDate);
		result2.setEndDate(endDate.getTime());
		result2.setLocation(location);
		
		result2 = repo.update(result2);
		assertEquals(id, result2.getId());
		assertEquals(title, result2.getTitle());
		assertEquals(description, result2.getDescription());
		assertEquals(startDate, result2.getStartDate());
		assertEquals(endDate.getTime(), result2.getEndDate());
		assertEquals(location, result2.getLocation());
		assertEquals(clientId, result2.getClientId());
	}

}
