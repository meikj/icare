package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import org.icare.shared.data.jdbc.JdbcAlertsRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Alert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcAlertsRepositoryTest extends JdbcResourceRepositoryTest {
	
	private JdbcAlertsRepository repo;
	
	private Alert a1;
	private Alert a2;
	
	@Before
	public void setupAlertsRepo() {
		repo = new JdbcAlertsRepository(getJdbcOperations(), getClientRepository());
		repo.setAuthorityFilter(getClientRepository().getAuthorityFilter());
	}
	
	@Before
	public void initAlerts() {
		String type = "emergency";
		String coords = "50.4234,-4.5874";
		boolean ack = false;
		
		a1 = new Alert();
		a1.setType(type);
		a1.setCoords(coords);
		a1.setAck(ack);
		
		type = "other";
		coords = "30.3212,-8.8473";
		ack = false;
		a2 = new Alert();
		a2.setType(type);
		a2.setCoords(coords);
		a2.setAck(ack);
	}
	
	@Test
	public void testValidate() {
		Alert a = new Alert();
		a.setType("emergency");
		a.setCoords("10.234,-3.212");
		repo.validate(a);
	}
	
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		Alert a = new Alert();
		repo.validate(a);
	}
	
	@Test
	public void testInsert() {
		Alert result = repo.add(a1);
		assertEquals(1, result.getId());
		assertEquals(a1.getType(), result.getType());
		assertEquals(a1.getCoords(), result.getCoords());
		assertEquals(a1.isAck(), result.isAck());
		assertEquals(getUser().getSelectedClientId(), result.getClientId());
		
		result = repo.add(a2);
		assertEquals(2, result.getId());
		assertEquals(a2.getType(), result.getType());
		assertEquals(a2.getCoords(), result.getCoords());
		assertEquals(a2.isAck(), result.isAck());
		assertEquals(getUser().getSelectedClientId(), result.getClientId());
	}
	
	@Test
	public void testUpdate() {
		Alert result = repo.add(a2);
		int id = result.getId();
		int clientId = result.getClientId();
		result.setAck(true);
		result = repo.update(result);
		assertEquals(true, result.isAck());
		assertEquals(id, result.getId());
		assertEquals(clientId, result.getClientId());
	}

}
