package org.icare.shared.test.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

@Configuration
@Profile("test")
public class DataSourceConfig {
	
//	@Bean
//	public DataSource dataSource() {
//		return new EmbeddedDatabaseBuilder().addScripts("test_schema.sql", "test_data.sql").build();
//	}

}
