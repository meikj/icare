package org.icare.shared.test.data.jdbc;

import static org.junit.Assert.assertEquals;

import org.icare.shared.data.jdbc.JdbcContactsRepository;
import org.icare.shared.exception.BadRequestException;
import org.icare.shared.model.Contact;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
public class JdbcContactsRepositoryTest extends JdbcResourceRepositoryTest {
	
	private JdbcContactsRepository repo;
	
	private Contact c1;
	private Contact c2;
	
	@Before
	public void setupContactsRepo() {
		repo = new JdbcContactsRepository(getJdbcOperations(), getClientRepository());
		repo.setAuthorityFilter(getClientRepository().getAuthorityFilter());
	}
	
	@Before
	public void initContacts() {
		String firstName = "Test First";
		String lastName = "Test Last";
		String email = "test@test.com";
		String phone = "014155544332";
		String address = "110 Test Street";
		String description = "Test Description";
		
		c1 = new Contact();
		c1.setFirstName(firstName);
		c1.setLastName(lastName);
		c1.setEmail(email);
		c1.setPhone(phone);
		c1.setAddress(address);
		c1.setDescription(description);
		
		firstName = "Another Test First";
		lastName = "Another Test Last";
		email = "test2@test2.com";
		phone = "01413332212";
		address = "111 Test Avenue";
		description = "Another Description";
		
		c2 = new Contact();
		c2.setFirstName(firstName);
		c2.setLastName(lastName);
		c2.setEmail(email);
		c2.setPhone(phone);
		c2.setAddress(address);
		c2.setDescription(description);
	}
	
	@Test
	public void testValidate() {
		repo.validate(c1);
	}
	
	@Test(expected = BadRequestException.class)
	public void testValidateException() {
		repo.validate(new Contact());
	}
	
	@Test
	public void testInsert() {
		Contact result = repo.add(c1);
		assertEquals(c1.getFirstName(), result.getFirstName());
		assertEquals(c1.getLastName(), result.getLastName());
		assertEquals(c1.getEmail(), result.getEmail());
		assertEquals(c1.getPhone(), result.getPhone());
		assertEquals(c1.getAddress(), result.getAddress());
		assertEquals(c1.getDescription(), result.getDescription());
		
		result = repo.add(c2);
		assertEquals(c2.getFirstName(), result.getFirstName());
		assertEquals(c2.getLastName(), result.getLastName());
		assertEquals(c2.getEmail(), result.getEmail());
		assertEquals(c2.getPhone(), result.getPhone());
		assertEquals(c2.getAddress(), result.getAddress());
		assertEquals(c2.getDescription(), result.getDescription());
	}
	
	@Test
	public void testUpdate() {
		Contact result = repo.add(c2);
		
		int id = result.getId();
		String firstName = "First Namet";
		String lastName = "Last Name";
		String email = "test3@test3.com";
		String phone = "0141555445782";
		String address = "110 Test Avenue";
		String description = "Test Description 2";
		
		result.setFirstName(firstName);
		result.setLastName(lastName);
		result.setEmail(email);
		result.setPhone(phone);
		result.setAddress(address);
		result.setDescription(description);
		
		result = repo.update(result);
		assertEquals(id, result.getId());
		assertEquals(firstName, result.getFirstName());
		assertEquals(lastName, result.getLastName());
		assertEquals(email, result.getEmail());
		assertEquals(phone, result.getPhone());
		assertEquals(address, result.getAddress());
		assertEquals(description, result.getDescription());
	}

}
