package org.icare.shared.test.data;

import static org.junit.Assert.assertEquals;

import org.icare.shared.data.QueryConstructor;
import org.icare.shared.data.QueryFilter;
import org.junit.Before;
import org.junit.Test;

public class QueryConstructorTest {
	
	public static final String TABLE_NAME = "mytesttable";
	
	private QueryConstructor qc;
	private QueryFilter filterWhere;
	private QueryFilter filterOrderBy;
	
	@Before
	public void before() {
		qc = new QueryConstructor(TABLE_NAME);
		filterWhere = new QueryFilter(" where test1 = ? and test2 = ?");
		filterOrderBy = new QueryFilter(" order by test1, test2");	
	}
	
	@Test
	public void testTableName() {
		assertEquals(qc.getTable(), TABLE_NAME);
	}
	
	@Test
	public void testBaseQuerySelect() {
		String result = qc.baseQuerySelect();
		String expectedResult = "select * from " + TABLE_NAME;
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testBaseQueryInsert() {
		String[] cols = {"col1", "col2", "col3", "col4"};
		String result = qc.baseQueryInsert(cols);
		String expectedResult = String.format(
				"insert into %s (col1, col2, col3, col4) values (?, ?, ?, ?)", TABLE_NAME);
		assertEquals(result, expectedResult);
	}
	
	@Test
	public void testBaseQueryUpdate() {
		String[] cols = {"col1", "col2", "col3", "col4"};
		String result = qc.baseQueryUpdate(cols);
		String expectedResult = String.format(
				"update %s set col1 = ?, col2 = ?, col3 = ?, col4 = ? where id = ?", TABLE_NAME);
		assertEquals(result, expectedResult);
	}
	
	@Test
	public void testBaseQueryDelete() {
		String result = qc.baseQueryDelete();
		String expectedResult = "delete from " + TABLE_NAME;
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testConstructWithFiltersNoBaseQueryClause() {
		String baseQuery = "select * from test";
		
		String whereResult = QueryConstructor.constructWithFilters(baseQuery, filterWhere);
		String expectedWhereResult = baseQuery + filterWhere;
		assertEquals(whereResult, expectedWhereResult);
		
		String orderByResult = QueryConstructor.constructWithFilters(baseQuery, filterOrderBy);
		String expectedOrderByResult = baseQuery + filterOrderBy;
		assertEquals(orderByResult, expectedOrderByResult);
	}
	
	@Test
	public void testConstructWithFiltersWhereBaseQueryClause() {
		String baseQuery = "select * from test where id = ?";
		
		String whereResult = QueryConstructor.constructWithFilters(baseQuery, filterWhere);
		String expectedWhereResult = "select * from test where test1 = ? and test2 = ? and id = ?";
		assertEquals(whereResult, expectedWhereResult);
		
		String orderByResult = QueryConstructor.constructWithFilters(baseQuery, filterOrderBy);
		String expectedOrderByResult = baseQuery + filterOrderBy;
		assertEquals(orderByResult, expectedOrderByResult);
	}
	
	@Test
	public void testConstructWithFiltersOrderByBaseQueryClause() {
		String baseQuery = "select * from test order by id";
		
		String whereResult = QueryConstructor.constructWithFilters(baseQuery, filterWhere);
		String expectedWhereResult = "select * from test where test1 = ? and test2 = ? order by id";
		assertEquals(whereResult, expectedWhereResult);
		
		String orderByResult = QueryConstructor.constructWithFilters(baseQuery, filterOrderBy);
		String expectedOrderByResult = "select * from test order by test1, test2, id";
		assertEquals(orderByResult, expectedOrderByResult);
	}
	
	@Test
	public void testCreateFilterWhere() {
		Object[] cols = new String[] { "col1", "and", "col2" };
		QueryFilter result = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, cols);
		String expectedResult = "where col1 = ? and col2 = ?";
		assertEquals(expectedResult, result.toString());
		
		result = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_WHERE, "only_col");
		expectedResult = "where only_col = ?";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test
	public void testCreateFilterOrderBy() {
		Object[] cols = new String[] { "col1", "col2", "col3" };
		QueryFilter result = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_ORDER_BY, cols);
		String expectedResult = "order by col1, col2, col3";
		assertEquals(expectedResult, result.toString());
		
		result = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_ORDER_BY, "only_col");
		expectedResult = "order by only_col";
		assertEquals(expectedResult, result.toString());
		
		cols = new String[] { "col1", "col2 desc", "col3" };
		result = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_ORDER_BY, cols);
		expectedResult = "order by col1, col2 desc, col3";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test
	public void testCreateFilterLimit() {
		QueryFilter result = QueryConstructor.createFilter(QueryConstructor.FILTER_TYPE_LIMIT, 2);
		String expectedResult = "limit 2";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCreateFilterException() {
		QueryConstructor.createFilter("ERROR", "test");
	}

}
