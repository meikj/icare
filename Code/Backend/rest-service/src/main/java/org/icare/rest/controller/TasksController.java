package org.icare.rest.controller;

import org.icare.shared.model.Task;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This implements a Task resource controller for the tasks section of the REST
 * service.
 * 
 * @author John Meikle
 *
 */
@RestController
@RequestMapping("/tasks")
public class TasksController extends ResourceController<Task> {

	@RequestMapping(value = "/{id}/complete", method = RequestMethod.GET)
	public @ResponseBody Task complete(@PathVariable int id) {
		Task task = (Task) resourceRepository.findById(id);
		task.setComplete(true);
		resourceRepository.update(task);
		return task;
	}

	@RequestMapping(value = "/{id}/incomplete", method = RequestMethod.GET)
	public @ResponseBody Task incomplete(@PathVariable int id) {
		Task task = (Task) resourceRepository.findById(id);
		task.setComplete(false);
		resourceRepository.update(task);
		return task;
	}

}
