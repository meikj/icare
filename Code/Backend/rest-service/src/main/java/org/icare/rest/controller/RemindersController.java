package org.icare.rest.controller;

import org.icare.shared.model.Reminder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This implements a Reminder resource controller for the reminders section of
 * the REST service.
 * 
 * @author John Meikle
 *
 */
@RestController
@RequestMapping("/reminders")
public class RemindersController extends ResourceController<Reminder> {

}
