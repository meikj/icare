package org.icare.rest.controller;

import org.icare.shared.model.Client;
import org.icare.shared.security.AuthTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This implements a Client resource controller for the client section of the
 * REST service.
 * 
 * @author John Meikle
 *
 */
@RestController
@RequestMapping("/client")
public class ClientController extends ResourceController<Client> {

	@Autowired
	private AuthTokenProvider authTokenProvider;

	@RequestMapping(value = "/auth_regen", method = RequestMethod.GET)
	public @ResponseBody Client regenAuthToken() {
		Client client = resourceRepository.findById(getClientId());
		client.setAuthToken(authTokenProvider.generateToken());
		resourceRepository.update(client);
		return client;
	}

}
