package org.icare.rest.security;

import org.icare.shared.data.ClientRepository;
import org.icare.shared.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;

/**
 * This class represents an AuthenticationProvider that can authenticate an auth
 * token against a client repository. This only supports AuthenticationToken's.
 * 
 * @author John Meikle
 *
 */
public class JdbcAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public Authentication authenticate(Authentication authentication) {
		String authString = (String) authentication.getCredentials();

		try {
			Client client = clientRepository.findByAuthToken(authString);
			return new AuthenticationToken(client);
		} catch (Exception e) {
			authentication.setAuthenticated(false);
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(AuthenticationToken.class);
	}

}
