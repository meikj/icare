package org.icare.rest.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.icare.rest.security.CacheSecurityContextRepository;
import org.icare.rest.security.JdbcAuthenticationProvider;
import org.icare.rest.security.AuthenticationTokenFilter;
import org.icare.shared.security.AuthTokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextRepository;

/**
 * The web security configuration of the REST application.
 * 
 * @author John Meikle
 *
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	/**
	 * This bean provides access to auth token generation.
	 */
	@Bean
	public AuthTokenProvider authTokenProvider() {
		return new AuthTokenProvider();
	}

	/**
	 * This bean ensures that JdbcAuthenticationProvider is used as Spring
	 * Security AuthenticationProvider.
	 */
	@Bean
	public AuthenticationProvider authProvider() {
		return new JdbcAuthenticationProvider();
	}

	/**
	 * This bean defines the AuthenticationEntryPoint. This is what will happen
	 * if the client is unauthorised.
	 */
	@Bean
	public AuthenticationEntryPoint authEntryPoint() {
		return new AuthenticationEntryPoint() {

			@Override
			public void commence(HttpServletRequest request,
					HttpServletResponse response,
					AuthenticationException authException) throws IOException,
					ServletException {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
						"Unauthorised: Please supply a valid auth token.");
			}

		};
	}

	/**
	 * This bean ensures that CacheSecurityContextRepository is used as Spring
	 * Security SecurityContextRepository.
	 */
	@Bean
	public SecurityContextRepository contextRepository() {
		return new CacheSecurityContextRepository();
	}

	/**
	 * Configures the REST service to ensure all requests are authorised, use
	 * our custom beans, provide exception handling, ensure statelessness,
	 * disable CSRF, and add our custom token authentication filter before the
	 * first authentication filter in the chain.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.anyRequest().authenticated()
			.and()
		.securityContext()
			.securityContextRepository(contextRepository())
			.and()
		.exceptionHandling()
			.authenticationEntryPoint(authEntryPoint())
			.and()
		.sessionManagement()
			// No need for sessions in REST
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
		// No need for CSRF protection in REST
		.csrf().disable().anonymous().disable()
		// Ensure token authentication occurs before any other authentication
		.addFilterBefore(new AuthenticationTokenFilter(authProvider(), contextRepository()), BasicAuthenticationFilter.class);
	}

}
