package org.icare.rest.security;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

/**
 * This implements the Spring SecurityContextRepository providing a cached
 * implementation using a HashMap where the key represents the auth token and
 * the value represents the SecurityContext. This allows SecurityContext to be
 * saved between HTTP requests, relieving the need to constantly hit the backend
 * data source with auth token lookups.
 * 
 * @author John Meikle
 *
 */
public class CacheSecurityContextRepository implements
		SecurityContextRepository {

	private Map<String, SecurityContext> tokenContextCache;

	/**
	 * Construct a new CacheSecurityContextRepository.
	 */
	public CacheSecurityContextRepository() {
		tokenContextCache = new HashMap<>();
	}

	@Override
	public SecurityContext loadContext(
			HttpRequestResponseHolder requestResponseHolder) {
		if (containsContext(requestResponseHolder.getRequest())) {
			return tokenContextCache
					.get(getTokenFromRequest(requestResponseHolder.getRequest()));
		} else {
			return SecurityContextHolder.createEmptyContext();
		}
	}

	@Override
	public void saveContext(SecurityContext context,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = context.getAuthentication();

		if (auth != null && auth instanceof AuthenticationToken) {
			String authToken = (String) auth.getCredentials();

			if (authToken != null) {
				tokenContextCache.put(authToken, context);
			}
		}
	}

	@Override
	public boolean containsContext(HttpServletRequest request) {
		return tokenContextCache.containsKey(getTokenFromRequest(request));
	}

	/**
	 * Retrieve the auth token from a HTTP request. This uses the Authorization
	 * header and expects it in the following format:
	 * 
	 * Authorization: ICARE-REST token=[AUTH_TOKEN]
	 * 
	 * @param request
	 *            the HTTP request.
	 * @return the auth token, or null if Authorization header doesn't exist or
	 *         doesn't follow format.
	 */
	public static String getTokenFromRequest(HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization");

		if (authHeader != null) {
			String[] authHeaderSlices = authHeader.split("=");

			if (authHeaderSlices.length >= 2) {
				// we expect AUTH_TOKEN to be second slice as per format
				return authHeaderSlices[1];
			}
		}

		return null;
	}

}
