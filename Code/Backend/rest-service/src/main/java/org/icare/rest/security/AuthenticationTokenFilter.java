package org.icare.rest.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.icare.shared.model.Client;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.web.filter.GenericFilterBean;

/**
 * This filter handles token authentication by checking if the auth token
 * already exists within a SecurityContextRepository. If not, an authentication
 * procedure is executed by consulting an AuthenticationProvider. If
 * authentication is successful, the resulting auth token is added to the
 * SecurityContextRepository alongside its respective SecurityContext.
 * 
 * @author John Meikle
 *
 */
public class AuthenticationTokenFilter extends GenericFilterBean {

	private AuthenticationProvider authProvider;
	private SecurityContextRepository contextRepository;

	/**
	 * Construct a new TokenAuthenticationFilter with an AuthenticationProvider
	 * for generating an Authentication object using auth token credentials and
	 * a SecurityContextRepository for saving SecurityContext upon successful
	 * authentication and checking if auth token is already authorised.
	 * 
	 * @param authProvider
	 *            the AuthenticationProvider.
	 * @param contextRepository
	 *            the SecurityContextRepository.
	 */
	public AuthenticationTokenFilter(AuthenticationProvider authProvider,
			SecurityContextRepository contextRepository) {
		this.authProvider = authProvider;
		this.contextRepository = contextRepository;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		// If auth token already exists in context repo, skip auth process
		if (!contextRepository.containsContext(httpRequest)) {
			String authToken = CacheSecurityContextRepository
					.getTokenFromRequest(httpRequest);

			if (authToken != null) {
				// Generate an auth request to the provider for a response
				Client clientRequest = new Client();
				clientRequest.setAuthToken(authToken);
				AuthenticationToken authRequest = new AuthenticationToken(
						clientRequest);
				AuthenticationToken authResponse = (AuthenticationToken) authProvider
						.authenticate(authRequest);

				if (authResponse != null
						&& authResponse instanceof AuthenticationToken
						&& authResponse.isAuthenticated()) {
					// Authentication was successful
					SecurityContextHolder.getContext().setAuthentication(
							authResponse);
				}
			}
		}

		// Continue down the filter chain (this will handle load/save context)
		chain.doFilter(httpRequest, httpResponse);
	}

}
