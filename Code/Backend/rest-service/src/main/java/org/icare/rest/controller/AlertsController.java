package org.icare.rest.controller;

import java.util.Date;

import org.icare.shared.model.Alert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This implements an Alert resource controller for the alerts section of the
 * REST service.
 * 
 * @author John Meikle
 *
 */
@RestController
@RequestMapping("/alerts")
public class AlertsController extends ResourceController<Alert> {
	
	@RequestMapping(value = "/{id}/ack", method = RequestMethod.GET)
	public @ResponseBody Alert ack(@PathVariable int id) {
		Alert alert = (Alert) resourceRepository.findById(id);
		alert.setAck(true);
		resourceRepository.update(alert);
		return alert;
	}

	@RequestMapping(value = "/{id}/unack", method = RequestMethod.GET)
	public @ResponseBody Alert unack(@PathVariable int id) {
		Alert alert = (Alert) resourceRepository.findById(id);
		alert.setAck(false);
		resourceRepository.update(alert);
		return alert;
	}
	
	@Override
	public @ResponseBody Alert add(@RequestBody Alert model) {
		model.setDateTime(new Date()); // ensure date and time is NOW
		return super.add(model);
	}

}
