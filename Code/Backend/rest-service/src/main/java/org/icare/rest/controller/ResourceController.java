package org.icare.rest.controller;

import java.util.List;

import org.icare.rest.security.AuthenticationToken;
import org.icare.shared.data.ResourceRepository;
import org.icare.shared.model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This is a basic abstract implementation for all resource controllers. This
 * works with any Resource model providing basic supporting for getting, adding,
 * updating and deleting resources.
 * 
 * @author John Meikle
 *
 * @param <T>
 *            the type of Resource.
 */
public abstract class ResourceController<T extends Resource> {

	@Autowired
	protected ResourceRepository<T> resourceRepository;

	/**
	 * Executed upon a GET request to this resource controller. This finds and
	 * returns all resources.
	 * 
	 * @return the list of all resources of type T.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<T> getAll() {
		return resourceRepository.findAll();
	}

	/**
	 * Executed upon a GET request to a resource by its ID to this controller.
	 * This finds and returns a resource by its ID.
	 * 
	 * @param id
	 *            the resource ID.
	 * @return the resource of type T.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody T getId(@PathVariable int id) {
		return resourceRepository.findById(id);
	}

	/**
	 * Executed upon a POST request to this resource controller. This adds a
	 * resource using a model.
	 * 
	 * @param model
	 *            the resource model.
	 * @return the resulting resource, complete with ID.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody T add(@RequestBody T model) {
		model.setClientId(getClientId());
		return resourceRepository.add(model);
	}

	/**
	 * Executed upon a PUT request to a resource by its ID to this controller.
	 * This updates and returns a resource by its ID and model.
	 * 
	 * @param id
	 *            the resource ID.
	 * @param model
	 *            the resource model.
	 * @return the resulting resource, complete with changes.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody T update(@PathVariable int id, @RequestBody T model) {
		model.setId(id);
		model.setClientId(getClientId());
		return resourceRepository.update(model);
	}

	/**
	 * Executed upon a DELETE request to a resource by its ID to this
	 * controller. This deletes a resource by its ID, returning a success
	 * string.
	 * 
	 * @param id
	 *            the resource ID.
	 * @return the success string.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody String delete(@PathVariable int id) {
		resourceRepository.remove(id);
		return "true";
	}
	
	/**
	 * Get the currently authorised clients ID.
	 * 
	 * @return the client ID.
	 */
	protected int getClientId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth instanceof AuthenticationToken) {
			return (int) auth.getPrincipal();
		}
		return 0;
	}

}
