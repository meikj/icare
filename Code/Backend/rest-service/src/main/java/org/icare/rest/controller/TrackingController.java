package org.icare.rest.controller;

import org.icare.shared.model.Track;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This implements a Track resource controller for the tracking section of the
 * REST service.
 * 
 * @author John Meikle
 *
 */
@RestController
@RequestMapping("/tracks")
public class TrackingController extends ResourceController<Track> {

}
