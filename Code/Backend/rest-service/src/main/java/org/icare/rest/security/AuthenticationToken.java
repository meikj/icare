package org.icare.rest.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.icare.shared.model.Client;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * This class represents an authentication token model. It implements the
 * Authentication interface to ensure support within an AuthenticationProvider.
 * This model exposes the clients ID, name and auth token.
 * 
 * It is important to note that by default isAuthenticated() is set to true.
 * This is because setAuthenticated() cannot be set to true, instead, if
 * authentication fails, setAuthenticated should be set to false. This is just
 * so it cannot be set to true after being set to false.
 * 
 * @author John Meikle
 *
 */
public class AuthenticationToken implements Authentication {

	private static final long serialVersionUID = 900401711320048102L;

	private boolean authenticated;
	private Client client;
	private List<GrantedAuthority> authorities;

	/**
	 * Construct a new AuthenticationToken using a client model.
	 * 
	 * @param client the client model.
	 */
	public AuthenticationToken(Client client) {
		this.client = client;
		authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));
		authenticated = true;
	}

	@Override
	public String getName() {
		return String.format("%s %s", client.getFirstName(), client.getLastName());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/**
	 * The auth token.
	 */
	@Override
	public Object getCredentials() {
		return client.getAuthToken();
	}

	/**
	 * The client model.
	 */
	@Override
	public Object getDetails() {
		return client;
	}

	/**
	 * The ID.
	 */
	@Override
	public Object getPrincipal() {
		return client.getId();
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	/**
	 * Cannot be set to true (but is true by default), only false. Set to false
	 * if authentication fails.
	 */
	@Override
	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		if (isAuthenticated) {
			throw new IllegalArgumentException(
					"setAuthenticated(true) disallowed");
		} else {
			authenticated = false;
		}
	}

}
