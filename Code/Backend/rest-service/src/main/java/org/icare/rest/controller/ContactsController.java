package org.icare.rest.controller;

import org.icare.shared.model.Contact;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This implements a Contact resource controller for the contacts section of the
 * REST service.
 * 
 * @author John Meikle
 *
 */
@RestController
@RequestMapping("/contacts")
public class ContactsController extends ResourceController<Contact> {
	
	// No further functionality needed for now...

}
