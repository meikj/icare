package org.icare.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller provides a way for clients to perform a simple auth check.
 * 
 * @author John Meikle
 *
 */
@RestController
public class AuthController {

	@RequestMapping(value = "/auth_check", method = RequestMethod.GET)
	public @ResponseBody String authCheck() {
		return "true";
	}

}
